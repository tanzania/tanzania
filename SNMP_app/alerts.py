from SNMP_app.core import snmp_sender
from SNMP_app.models import alerts_db

from configuration import LogInput, GetRequest



import traceback

## All possible alert/clear ids
    #-------------------------------
    # npg-db-alert                1
    # bscs-connectivity-alert     2
    # payment-service-alert       3
    # stp1-connectivity-alert     4
    # stp2-connectivity-alert     5
    # email-connectivity-alert    6
    # ras-connectivity-alert      7
    # portout-timeout-alert       8       
    # queue-process-alert         9

    # npg-db-clear                10
    # bscs-connectivity-clear     11
    # payment-service-clear       12
    # stp1-connectivity-clear     13
    # stp2-connectivity-clear     14
    # email-connectivity-clear    15    
    # ras-connectivity-clear      16
    # portout-timeout-clear       17
    # queue-process-clear         18
    #-------------------------------  

alerts_ref = {
	1:"npg-db-alert",
	2:"bscs-connectivity-alert",
	3:"payment-service-alert",
	4:"stp1-connectivity-alert",
	5:"stp2-connectivity-alert",
	6:"email-connectivity-alert",
	7:"ras-connectivity-alert",
	8:"portout-timeout-alert",
	9:"queue-process-alert",
}  

severity_ref = {
	1:"NOTICE",
	2:"WARNING",
	3:"ERROR",
	4:"CRITICAL",
	5:"EMERGENCY",
}   

# OIDs from 1 to 9 used with "send_alert" .. and, from 10 to 18 used with "send_clear"
def send_alert( oid , severe , ID ):

	try:
		LogInput("Preparing to send an SNMP alert Trap .. ","SNMP","SNMP")
		snmp_sender( object_number=oid, notification_type=1, serverity=severe , alert_id=ID )
		LogInput("Alert trap with OID %s has been sent with no problems .. " % str(oid) ,"SNMP","SNMP")
	except Exception as exc:
		print "Sending Alert failed, with this exception traceback:\n "
		print str( traceback.format_exc() )
		LogInput("Sending Alert failed, with this exception .. " + str(exc) ,"SNMP","SNMP")	
		LogInput( str( traceback.format_exc() ) )

	try:	
		## Saving the alert to DB
		LogInput("Saving Objects to DB ..","SNMP","SNMP")
		alerts = alerts_db()
		alerts.oid 			 = oid
		alerts.oid_name 	 = alerts_ref[oid]
		alerts.severity 	 = severe	
		alerts.severity_name = severity_ref[severe]
		alerts.alert_id		 = ID
		alerts.save()
                LogInput("Saving to DB is Done.","SNMP","SNMP")
		
	except Exception:
		print 'Saving Alert to DB failed, probably DB is Down ...'
		print str( traceback.format_exc() )
		LogInput("Saving Alert to DB failed, probably DB is Down.\n","SNMP","SNMP")
		LogInput( str(traceback.format_exc()) )



def send_clear(oid, ID):

	try:
		LogInput("Inside sending clear method.","SNMP","SNMP")
	
		#alerts = alerts_db()
		result = alerts_db.objects.filter(oid=oid , alert_id=ID)

		if result:
			try:
				alerts_db.objects.filter(oid=oid , alert_id=ID).delete()		
				snmp_sender( object_number=int(oid)+9 , notification_type=2, serverity=1 , alert_id=ID )	
			except Exception, e:
				LogInput("Problem with Clear Trap -- %s" % str(e),"SNMP","SNMP")
		else:

			LogInput("Nothing to clear","SNMP","SNMP")

			# This is the only case to send a clear without checking the corresponding alert in DB
			# because the sent alert in case of DB failure will not be stored in DB due to its failure
			if oid == 1:
				 LogInput("OID =1, it is a DB failure clear request .. sending clear trap","SNMP","SNMP")
				 snmp_sender( object_number=int(oid)+9 , notification_type=2, serverity=1 , alert_id=ID )
				 LogInput("DB clear trap has been sent successfully","SNMP","SNMP")


	except Exception:
		print "Sending Clear failed, with this exception traceback:\n "
                print str( traceback.format_exc() )
		LogInput("Sending clear failed, probably becuase DB went down or still down.","SNMP","SNMP")
		LogInput( str(traceback.format_exc())  )

if __name__ == '__main__':

	# Only send one at a time for unit testing agains testing trap manager server/application

	# One:
	send_alert(1,2,100)
	
	# Two:
	#send_clear(1,100)

from pysnmp.entity import engine, config
from pysnmp.carrier.asynsock.dgram import udp
from pysnmp.entity.rfc3413 import ntforg, context
from pysnmp.proto.api import v2c

# Create SNMP engine instance
snmpEngine = engine.SnmpEngine()

## CLEAR is Default, and NOTICE is the main severiy level, so you only need to set the alert
def snmp_sender( object_number=1, notification_type=2, serverity=1 , alert_id=100 ):
	
    # Starting SNMP SERVER logs
    print 'Starting core snmp_sender'
    
    config.addV1System(snmpEngine, 'my-area', 'public')

    # Specify security settings per SecurityName (SNMPv2c -> 1)
    config.addTargetParams(snmpEngine, 'my-creds', 'my-area', 'noAuthNoPriv', 1)

    # Setup transport endpoint and bind it with security settings yielding
    # a target name
    try:
	config.addSocketTransport(
        	snmpEngine,
	        udp.domainName,
	        udp.UdpSocketTransport().openClientMode()
		    )
    ############# Adding target IPs of the managers ################

    # config.addTargetAddr(snmpEngine, 'my-nms-1',
    #                     udp.domainName, ('127.0.0.1', 162),
    #                     'my-creds',
    #                     tagList='all-my-managers'
    #                         )

    # First Target
	print 'Initializing first server configuration -- 192,168.18.128'
    	config.addTargetAddr(snmpEngine, 'my-nms-1',
                        udp.domainName, ('192.168.18.128', 162),
                        'my-creds',
                        tagList='all-my-managers'
                            )
    # Second Target
	print 'Initializing second server configuration -- 192,168.18.129'
	config.addTargetAddr(snmpEngine, 'my-nms-2',
                        udp.domainName, ('192.168.18.129', 162),
                        'my-creds',
                        tagList='all-my-managers'
                            )
    # Third Target
	print 'Initializing third server configuration -- 192,168.18.130'
	config.addTargetAddr(snmpEngine, 'my-nms-3',
                        udp.domainName, ('192.168.18.130', 162),
                        'my-creds',
                        tagList='all-my-managers'
                             )
    except Exception as e:
	print str(e)
    ###################### End of managers list ####################

    # Specify what kind of notification should be sent (TRAP or INFORM),
    # to what targets (chosen by tag) and what filter should apply to
    # the set of targets (selected by tag)
    config.addNotificationTarget(
        snmpEngine, 'my-notification', 'my-filter', 'all-my-managers', 'trap' 
    )

    # Allow NOTIFY access to Agent's MIB by this SNMP model (2), securityLevel
    # and SecurityName
    config.addContext(snmpEngine, '')
    config.addVacmUser(snmpEngine, 2, 'my-area', 'noAuthNoPriv', (), (), (1,3,6))

    # *** SNMP engine configuration is complete by this line ***

    snmpContext = context.SnmpContext(snmpEngine)

    # Create Notification Originator App instance. 
    ntfOrg = ntforg.NotificationOriginator(snmpContext)
     
    # Build and submit notification message to dispatcher
    ntfOrg.sendNotification(
        snmpEngine,
        # Notification targets
        'my-notification',
        
        ## All possible alert/clear ids
        #-------------------------------
        # npg-db-alert                1
        # bscs-connectivity-alert     2
        # payment-service-alert       3
        # stp1-connectivity-alert     4
        # stp2-connectivity-alert     5
        # email-connectivity-alert    6
        # ras-connectivity-alert      7
        # portout-timeout-alert       8       
        # queue-process-alert         9

        # npg-db-clear                10
        # bscs-connectivity-clear     11
        # payment-service-clear       12
        # stp1-connectivity-clear     13
        # stp2-connectivity-clear     14
        # email-connectivity-clear    15    
        # ras-connectivity-clear      16
        # portout-timeout-clear       17
        # queue-process-clear         18
        #-------------------------------    


        ( 1,3,6,1,4,1,4222,10,20, int(object_number) ), ## NPG DB alert(1)
        
        ## below, add all the varbinds you want to the above notification
        # ( (oid, value), ... )
        #gtss:4222 , gtss-npg:10 , software-traps-module:20 , notification-scalars:50 , alert-clear(1) , severiy(2), alert-id(3)
        
        ## Alert type
        #---------------
        # ALERT(1),
        # CLEAR(2),
        

        ## Criticalness
        #---------------
        # NOTICE(1),
        # WARNING(2),
        # ERROR(3),
        # CRITICAL(4),
        # EMERGENCY(5)
        #---------------
        



        ( 
          ( (1,3,6,1,4,1,4222,10,20,50,1), v2c.Integer( int(notification_type) ) ), ## value 1 means alert
          ( (1,3,6,1,4,1,4222,10,20,50,2), v2c.Integer( int(serverity) ) ), ## value 5 means EMERGENCY
          ( (1,3,6,1,4,1,4222,10,20,50,3), v2c.Integer( int(alert_id) ) ), ## 9 is a random value to mark that specific notification, incase of many of its type has been sent
        )
    )

    


    print('Notification is scheduled to be sent')

    # Run I/O dispatcher which would send pending message and process response
    snmpEngine.transportDispatcher.runDispatcher()

if __name__ == '__main__':
   
    ## Format: snmp_sender( object_number=1, type=1, serverity=3 , alert_id=100)

    print 'Calling the main sender function ..'
    snmp_sender( object_number=5, notification_type=1, serverity=3 , alert_id=200 )
    print 'Traps sending accomplished to the requried targets.'

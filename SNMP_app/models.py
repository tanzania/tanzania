from django.db import models

class alerts_db(models.Model):
        oid 			= models.SmallIntegerField(max_length=2, blank=False, null=False)
        oid_name        = models.CharField(max_length=50, blank=True, null=True)
        severity        = models.SmallIntegerField(max_length=2, blank=False, null=False)
        severity_name   = models.CharField(max_length=15, blank=True, null=True)
        alert_id 		= models.SmallIntegerField(max_length=4, blank=False, null=False)
        
        class Meta:
                db_table = "snmp_alerts_track"
                                               
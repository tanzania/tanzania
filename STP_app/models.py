from django.db import models
from django.core.validators import RegexValidator
import re


PHONENUMBER = re.compile('^[0-9]{8}$')
PHONENUMBER_message = "The number is incorrect, must be 8 digits."

class STP_db(models.Model):
	time_stamp = models.DateTimeField(auto_now=True)
	server		= models.CharField(max_length=10, blank=True, null=True)
	command		= models.CharField(max_length=100, blank=True, null=True)
	response	= models.CharField(max_length=100, blank=True, null=True)
	number		= models.CharField(max_length=12, blank=False, null=False, validators=[RegexValidator(regex = PHONENUMBER, message = PHONENUMBER_message)])
	RN 				= models.CharField(max_length=7, blank=True, null=True)
	NPOrderID	= models.BigIntegerField(max_length=15, blank=True, null=True)
	status		= models.CharField(max_length=10, blank=True, null=True)
	RecipientName = models.CharField(max_length=20, blank=True, null=True) # Provider ID for recipient
	DonorName = models.CharField(max_length=20, blank=True, null=True) 

	class Meta:
		db_table = "np_integration_stp"	
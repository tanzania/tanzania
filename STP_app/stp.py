# Import LogInput
from configuration import GetConfig, LogInput, GetRequest, GetOperator, LogMessageResutls, GET_STP_SERVERS
# Importing telnet library
import telnetlib
# Import DB models here
from STP_app.models import STP_db
# Importing pyparsing
from pyparsing import LineEnd, oneOf, Word, nums, Literal, Combine, restOfLine, alphanums, Suppress, empty, originalTextFor, OneOrMore, alphas, Group, ZeroOrMore
import traceback
from time import sleep
# Importing SNMP alerts
from SNMP_app.alerts import send_alert, send_clear


import traceback


STPSERVER = ""

class STP():

	def __init__(self, host=None , port=None, uname=None, pswrd=None, nporder_id=None):
		
		self.host = host
		self.port = port
		self.uname = uname
		self.pswrd = pswrd
		self.NPOrder_ID = nporder_id
	
##############################################################################
# Initial STP connection
##############################################################################
	
	def connection_stub(self, telnet_command):

		try:
			# Issue telnet command
			LogInput("Command: " + telnet_command, self.NPOrder_ID,"STP")
			self.tn.write( str(telnet_command) )

			# Read until any random string so it doesn't  find it and returns everything
			try:
				telnet_result = self.tn.read_until('Quark',timeout=0.1)

				# Checking which STP server for the corresponding clear message
				if STPSERVER == 'STP1':
					send_clear(4,200)
                                        send_clear(5,201)
				elif STPSERVER == 'STP2':
					send_clear(5,201)
                                        send_clear(4,200)

			except Exception as e:
				LogInput( "Can't Read STP Data Stream: " + str(e) , self.NPOrder_ID,"STP")

				# Checking which STP server, to send the corresponding alert
				if STPSERVER == 'STP1':
					send_alert(4 , 5 , 200)

				elif STPSERVER == 'STP2':
					send_alert(5 , 5 , 201)

				else:
					send_alert(4 , 5 , 200)
					send_alert(5 , 5 , 201)
			
			# Debuging by printing
			LogInput("Response: " + telnet_result, self.NPOrder_ID,"STP")

			# Parse the result
			connection_result = self.parse(telnet_result)
			
			if connection_result:
				return [True, telnet_result]
			else:
				return [False, telnet_result]

		except Exception as e:
			LogInput("Error communicating with the STP server ..." + str(e), self.NPOrder_ID,"STP")
			return [False, "Place Holding Text"]
		

	def connect(self,): 
		
		self.tn = telnetlib.Telnet( self.host , self.port )

		# Connect command: connect (iid 1, version 1.0)
		telnet_command = "connect (iid 1, version 1.0)" + '\n'
		
		LogInput("STP MML Connect Command ... " + telnet_command, self.NPOrder_ID,"STP")
		## Connecting to the Teklek's STP
		return self.connection_stub(telnet_command)[0] ## return the first arg, True or False


##############################################################################
# HA STP Active Selector
##############################################################################

	def active_stp_selector(self, which_stp=None): 
		
		self.tn = telnetlib.Telnet( self.host , self.port )

		telnet_command = "connect (iid 1, version 1.0)" + '\n'
		
		LogInput("Checking whether STP %s is Active ... "% which_stp + telnet_command, self.NPOrder_ID,"STP")	

		try:
		
			LogInput("Probing STP %s ...: "% which_stp + telnet_command, self.NPOrder_ID,"STP")
			self.tn.write( str(telnet_command) )

			try:			
				telnet_result = self.tn.read_until('Quark',timeout=0.1)			
			except Exception as e:
				LogInput( "Can't Read STP Data Stream: " + str(e) , self.NPOrder_ID,"STP")	


			LogInput("STP %s Responded with: "% which_stp + telnet_result, self.NPOrder_ID,"STP")
			
			print telnet_result

			## Parsing the results
			selected_stp = self.active_stp_parser( telnet_result , which_stp )

			if self.disconnect():
				LogInput( "Disconnected fine." , self.NPOrder_ID,"STP")

			# Closing the Telnet session
			self.tn.close()
			
			return selected_stp


		except Exception as e:
			LogInput("Error communicating with the STP server ..." + str(e), self.NPOrder_ID,"STP")		

	

	def active_stp_parser(self, telnet_result , which_stp):

		active = Literal( 'side active' ) + ')'
		standby = Literal( 'side standby' ) + ')'

		active_standby_finder = active ^ standby ## "^" acts as "or", but we can't use or here

		total = active_standby_finder

		result = total.scanString( telnet_result )

		active_standby = ''

		try:
		    LogInput( "Checking Active or Standby ..." , self.NPOrder_ID,"STP")
		    active_standby = result.next()
		    
		    LogInput( "STP Response: " + str(active_standby), self.NPOrder_ID,"STP")

		    if active_standby[0][0] == 'side active':
		       LogInput( "STP %s is Active."% which_stp , self.NPOrder_ID,"STP")
		       return True
		    elif active_standby[0][0] == 'side standby':
		       LogInput( "STP %s is Standby."% which_stp , self.NPOrder_ID,"STP")
		       return False
		    else:
		        LogInput( "STP Response is Corrupted." , self.NPOrder_ID,"STP")
		    
		except Exception as e:
		    LogInput( "Error, STP Active System Decision can't be made, " + str(e) , self.NPOrder_ID,"STP")


##############################################################################
# Breaking Communication with STP
##############################################################################
	
	def disconnect(self,):	

		# Disconnect command: disconnect (iid 1)
		telnet_command = 'disconnect (iid 1)'  + '\n'

		LogInput("STP MML Disconnect Command ... " + telnet_command , self.NPOrder_ID,"STP")

		## Connecting to the STP
		return self.connection_stub(telnet_command)[0]

##############################################################################
# Enfording Write Permissions with STP
##############################################################################
	def write_enforce(self,):	

		# Write Enforce command: begin_txn(iid 1, type write)
		telnet_command = "begin_txn (iid 1, type write)" + "\n"

		LogInput("STP MML Write-Enforce Command ... " + telnet_command, self.NPOrder_ID,"STP")


		## Connecting to the STP
		return self.connection_stub(telnet_command)[0]

##############################################################################
# Saving added Numbers to STP DB
##############################################################################

	def stp_save(self,):

		LogInput("Saving Number to STP DB ... ", self.NPOrder_ID,"STP")

		# Add command: end_txn (iid 1)
		telnet_command = 'end_txn (iid 1)' + "\n"

		LogInput("STP MML Command, Saving Number to STP DB ... " + telnet_command, self.NPOrder_ID,"STP")

		## Connecting to the STP
		return self.connection_stub(telnet_command)[0]		

##############################################################################
# Parse the STP response
##############################################################################

	def parse(self, telnet_result):

		#LogInput("Parsing ...", self.NPOrder_ID,"STP")

		try:
			# In case of success -> rc 0'
			LogInput("STP Response String:  " + str(telnet_result), self.NPOrder_ID,"STP")
			finder = ' ' + Literal('rc') + ' ' + Word(nums)
			finder.leaveWhitespace()
			finder.setParseAction(lambda t: [t[1],int(t[3])])	## Only return the rc and the number
			res = finder.scanString(telnet_result)

			try:
			    while True:
			        iterator = res.next()
			        LogInput(str(iterator),"STP","STP")
			        if iterator[0][1] == 0:			            
			            return True
			        else:
			            LogInput("Problem with command --->   %s" % telnet_result, self.NPOrder_ID,"STP")
			            return False
			except Exception, e:
			    LogInput("Inner MML Parsing Error .. " + str(e), self.NPOrder_ID,"STP")
			    
		except Exception, e:
			LogInput("Outer MML Parsing Error .. " + str(e), self.NPOrder_ID,"STP")

##############################################################################
# Database Stub
##############################################################################

	def db_stub(self, issued_commands, number, Route, inmessage, status = None):
		
		# Returns in this format [[True/False,Response],Command]
		Command  = issued_commands[1]
		Response = issued_commands[0][1]
		if status == None:
			Status   = 'Added' if issued_commands[0][0] else 'Failed'
		else:
			Status = status	
		
		# Add it to DB
		try:
			# Object initialization
			stp = STP_db()
			global STPSERVER
			stp.number 	  	  = number
			stp.RN 	   	  	  = Route.RoutingNumber
			stp.command   	  = Command
			stp.response  	  = Response
			stp.status    	  = Status
			stp.NPOrderID 	  = inmessage.NPOrderID
			stp.server 		  = STPSERVER
			stp.RecipientName = inmessage.RecipientName
			stp.DonorName 	  = inmessage.DonorName
			stp.save()

		except Exception, e:
			LogInput("Problem Saving to NPG's Database ..." + str(e), self.NPOrder_ID,"STP")

##############################################################################
# Check the incoming NP message type
##############################################################################

	def check_message(self, inmessage):

		if inmessage.MessageTypeName == 'NPExecuteBroadcast':

			## If Porting IN
			if inmessage.RecipientName == GetConfig("COMPANY_NAME"):

				LogInput("Port in number(s) patch ...", self.NPOrder_ID,"STP")
				
				for Route in  inmessage.RoutingInfoList.all():

					List = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 	

					for key,number in enumerate(List):
						
						LogInput('key: '+str(key), self.NPOrder_ID,"STP")
						if key%30 == 0:

							try:
								LogInput('Making an STP Save for each 30 numbers batch', self.NPOrder_ID,"STP")
								self.stp_save()
								LogInput('Restarting Write-Enforce/begin_txn', self.NPOrder_ID,"STP")
								self.write_enforce()
							except Exception as e:
								LogInput( 'Problem in saving the 30 numbers batch ..' + str(e), self.NPOrder_ID,"STP")
						
						number = int(str("965" + str(number)))	

						# Add it to STP
						issued_commands = self.add_portin( number , Route.RoutingNumber )
						
						self.db_stub(issued_commands, number, Route, inmessage)
						LogInput('Stored %s Number in NPG DB ...' % str(number), self.NPOrder_ID,"STP")


			## If Porting OUT	
			elif inmessage.DonorName == GetConfig("COMPANY_NAME"):

				LogInput("Port out number(s) patch ...", self.NPOrder_ID,"STP")
				
				for Route in  inmessage.RoutingInfoList.all():

					List = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 	
				
					for key,number in enumerate(List):

						LogInput('key: '+str(key), self.NPOrder_ID,"STP")
						
						if key%30 == 0:

							try:
								LogInput('Making an STP Save for each 30 numbers batch', self.NPOrder_ID,"STP")
								self.stp_save()
								LogInput('Restarting Write-Enforce/begin_txn', self.NPOrder_ID,"STP")
								self.write_enforce()
							except Exception as e:
								LogInput( 'Problem in saving the 30 numbers batch ..' + str(e), self.NPOrder_ID,"STP")


						number = int(str("965" + str(number)))	

						# Add it to STP
						issued_commands = self.add_portout( number , Route.RoutingNumber )
						
						self.db_stub(issued_commands, number, Route, inmessage)
						LogInput('Stored %s Number in NPG DB ...' % str(number), self.NPOrder_ID,"STP")
			else:
				
				for Route in  inmessage.RoutingInfoList.all():

					List = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 	
				
					for key,number in enumerate(List):		
												
						if key%30 == 0:

							try:
								LogInput('Making an STP Save for each 30 numbers batch', self.NPOrder_ID,"STP")
								self.stp_save()
								LogInput('Restarting Write-Enforce/begin_txn', self.NPOrder_ID,"STP")
								self.write_enforce()
							except Exception as e:
								LogInput( 'Problem in saving the 30 numbers batch ..' + str(e), self.NPOrder_ID,"STP")

						number = int(str("965" + str(number)))	

						# Add it to STP						
						issued_commands = self.add_cross( number , Route.RoutingNumber )
												
						self.db_stub(issued_commands, number, Route, inmessage)
						LogInput('Stored %s Number in NPG DB ...' % str(number), self.NPOrder_ID,"STP")

		

		elif inmessage.MessageTypeName == 'NPReturnBroadcast':

			for Route in  inmessage.RoutingInfoList.all():

				List = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 	
				
				for number in List:	
					
					# Adding the country code ..
					number = int(str("965" + str(number)))	

					LogInput('Deleting %s ...'% number, self.NPOrder_ID,"STP")
					LogInput( 'DonorName: ' + str(inmessage.DonorName), self.NPOrder_ID,"STP")
					LogInput( 'RecipientName: ' + str(inmessage.RecipientName), self.NPOrder_ID,"STP")

					# Delete from STP
					issued_commands = self.delete( number ) 
					
					# # Returns in this format [[True/False,Response],Command]
					# Command  = issued_commands[1]
					# Response = issued_commands[0][1]
					Status   = 'Deleted' if issued_commands[0][0] else 'Failed'

					self.db_stub(issued_commands, number, Route, inmessage, status = Status)
					LogInput('Stored %s Number in NPG DB ...' % str(number), self.NPOrder_ID,"STP")

				
##############################################################################
# Add a port-out Number to STP DB
##############################################################################

	def add_portout(self, number, RN):

		# Add command: ent_sub (iid 1, dn 96589887766, pt 1, rn A1, force yes)
		telnet_command = 'ent_sub (iid 1, dn %s, pt 1, rn %s, force yes)' % ( number , RN ) + '\n'

		LogInput("Adding A Portout Number ... " + telnet_command, self.NPOrder_ID,"STP")

		## Connecting to the STP
		return [self.connection_stub(telnet_command), telnet_command]

##############################################################################
# Add a port-in Number to STP DB
##############################################################################

	def add_portin(self, number, RN):

		# Add command: ent_sub (iid 1, dn 96589887766, sp A3, force yes)
		telnet_command = 'ent_sub (iid 1, dn %s, sp %s, force yes)' % ( number , RN ) + '\n'

		LogInput("Adding A Portin Number ... " + telnet_command, self.NPOrder_ID,"STP")

		## Connecting to the STP
		return [self.connection_stub(telnet_command), telnet_command]	


##############################################################################
# Add a 3rd-party/cross-porting Number to STP DB
##############################################################################

	def add_cross(self, number, RN):

		# Add command: ent_sub (iid 1, dn 96589887766, pt 2, rn A1, force yes)
		telnet_command = 'ent_sub (iid 1, dn %s, pt 2, rn %s, force yes)' % ( number , RN ) + '\n'

		LogInput("Adding A Cross-Porting Number ... " + telnet_command, self.NPOrder_ID,"STP")

		## Connecting to the STP
		return [self.connection_stub(telnet_command), telnet_command]	

##############################################################################
# Delete an existing number
##############################################################################

	def delete(self, number):

		# Delete command: dlt_sub (dn 96566369244)
		telnet_command = 'dlt_sub ( iid 1 , dn %s)' % (number,) + '\n'

		LogInput("Deleteing Number ... " + telnet_command, self.NPOrder_ID,"STP")

		## Connecting to the STP
		return [self.connection_stub(telnet_command), telnet_command]		

##############################################################################
# Initializer ...
##############################################################################

# We should pass inmessage.NPOrderID to Initializer, in forker/tasks.pyx

class Initializer():

	def __init__(self,inmessage):
		try:
			# Extracting the Order ID to use it in creating a separate log file for each NPOrderID
			self.NPOrder_ID = inmessage.NPOrderID
			
	
			LogInput('\n\n',self.NPOrder_ID,"STP")
			LogInput('Testing Connection to STP 1 ...',self.NPOrder_ID,"STP")
			LogInput(str(GET_STP_SERVERS("STP1").get('server')) + ' ----- ' + str(GET_STP_SERVERS("STP1").get('port')),self.NPOrder_ID,"STP")

			STP_obj = STP( host=GET_STP_SERVERS("STP1").get('server') , port=GET_STP_SERVERS("STP1").get('port'), nporder_id=self.NPOrder_ID ) 
			is_working = STP_obj.active_stp_selector("One")


			print GET_STP_SERVERS("STP1").get('server')
                        print GET_STP_SERVERS("STP1").get('port')
                        print is_working


			if is_working:
			
				LogInput('STP1 Connection Test Successful ...',self.NPOrder_ID,"STP")
				LogInput('Initializing Actual STP 1 Connection ...',self.NPOrder_ID,"STP")
				
				global STPSERVER 
				STPSERVER = 'STP1'
				
				self.host = GET_STP_SERVERS("STP1").get('server')
				self.port = GET_STP_SERVERS("STP1").get('port')

				#######################################
				# Clear .. STP 1, it works fine
				send_clear(4 , 200)
				#######################################
			

			else:
				raise Exception("STP 1 is Standby ... Moving to STP 2")




		except Exception:

			#######################################
			# Alarm .. STP 1 failure
			send_alert(4 , 5 , 200)
			#######################################
			LogInput('\n\n',self.NPOrder_ID,"STP")
			LogInput('Testing Connection to STP 2 ...',self.NPOrder_ID,"STP")
			LogInput(str(GET_STP_SERVERS("STP2").get('server')) + ' ----- ' + str(GET_STP_SERVERS("STP2").get('port')),self.NPOrder_ID,"STP")

			STP_obj = STP( host=GET_STP_SERVERS("STP2").get('server') , port=GET_STP_SERVERS("STP2").get('port'), nporder_id=self.NPOrder_ID ) 

			print GET_STP_SERVERS("STP2").get('server')
			print GET_STP_SERVERS("STP2").get('port')
			print is_working

			is_working = STP_obj.active_stp_selector("Two")

			if is_working:

				LogInput('STP2 Connection Test Successful ...',self.NPOrder_ID,"STP")
				LogInput('Initializing Actual STP 2 Connection ...',self.NPOrder_ID,"STP")			

				global STPSERVER
				STPSERVER = 'STP2'	
				
				self.host = GET_STP_SERVERS("STP2").get('server')
				self.port = GET_STP_SERVERS("STP2").get('port')

				#######################################
				# Clear .. STP 2, it works fine
				send_clear(5 , 201)
				#######################################
			
			else:
				raise Exception("STP 2 is NOT Active as well ... Conncetion Failure")				

		except Exception:
			LogInput("Problem communicating with the 2 STP servers ...",self.NPOrder_ID,"STP")

			global STPSERVER
			STPSERVER = None

			#######################################
			# Alarm .. BOTH STPs failure
			send_alert(4 , 5 , 200)
			send_alert(5 , 5 , 201)
			#######################################

			#raise Exception("Both STP Servers Are Not Reacheable in Initializer ...")
			LogInput('Both STP Servers Are Not Reacheable in Initializer ...',self.NPOrder_ID,"STP")



	def start(self, inmessage):
		
		# check if the incoming message is either NPExecuteBroadcast or NPReturn
		# This part could be passed in __init__ of the STP class
		if inmessage.MessageTypeName in ['NPExecuteBroadcast','NPReturnBroadcast']:
			
		
			#raise Exception("We will do it isA")
				

			# Object initialization
			STP_obj = STP( host=self.host , port=self.port, nporder_id=self.NPOrder_ID ) # Initial communication data should go here
			
			# Let the object work on updating STP and updating NPG's DB
			LogInput('Initializing main STP object ...',self.NPOrder_ID,"STP")

			if STP_obj.connect():
				#LogInput("System connected ... ",self.NPOrder_ID,"STP")
				
				if STP_obj.write_enforce():
					#LogInput("Write permission has been enforced ... ",self.NPOrder_ID,"STP")
					
					# Check_message has all the workflow
					STP_obj.check_message(inmessage)

					# Saving To STP DB
					if STP_obj.stp_save():
						LogInput('Saved to STP DB ...',self.NPOrder_ID,"STP")
					else:
						LogInput('ERROR: Failed to save to STP DB.',self.NPOrder_ID,"STP")
					
					# Disconnect from STP system
					if STP_obj.disconnect():
						LogInput("Disconnected.",self.NPOrder_ID,"STP")
					else:
						LogInput("ERROR: Failed to Disconnect ...",self.NPOrder_ID,"STP")
	
				
				else:
					LogInput("Write Enforce Failed ...",self.NPOrder_ID,"STP")
					# Disconnect from STP system
					if STP_obj.disconnect():
						LogInput("Disconnected",self.NPOrder_ID,"STP")
					else:
						LogInput("ERROR: Failed to Disconnect ...",self.NPOrder_ID,"STP")
			else:
				LogInput("Connection Failed ...",self.NPOrder_ID,"STP")
		

		else:
			LogInput('NP Message Should be NPExecuteBroadcast or NPReturnBroadcast, %s has been passed' % inmessage.MessageTypeName , self.NPOrder_ID,"STP")











class Failed_Retry():

	def __init__(self,):
			try:
				LogInput('Connecting to STP 1 ...',"STP","STP")
				LogInput(str(GET_STP_SERVERS("STP1").get('server')) + ' ----- ' + str(GET_STP_SERVERS("STP1").get('port')),"STP","STP")
				
				telnet = telnetlib.Telnet( GET_STP_SERVERS("STP1").get('server') , GET_STP_SERVERS("STP1").get('port') )
				LogInput('Connection worked ...',"STP","STP")
				
				telnet.close()
				LogInput('Closing connection ...',"STP","STP")

				LogInput('Setting ServerName ...',"STP","STP")
				
				global STPSERVER 
				STPSERVER = 'STP1'
				
				LogInput('Setting host IP and port ...',"STP","STP")
				self.host = GET_STP_SERVERS("STP1").get('server')
				self.port = GET_STP_SERVERS("STP1").get('port')

				#######################################
				# Clear .. STP 1, it works fine
				send_clear(4 , 200)
				#######################################

			except Exception:

				#######################################
				# Alarm .. STP 1 failure
				send_alert(4 , 5 , 200)
				#######################################

				telnet = telnetlib.Telnet( GET_STP_SERVERS("STP2").get('server') , GET_STP_SERVERS("STP2").get('port') )
				telnet.close()
				
				global STPSERVER
				STPSERVER = 'STP2'	
				
				self.host = GET_STP_SERVERS("STP2").get('server')
				self.port = GET_STP_SERVERS("STP2").get('port')
				#######################################
				# Clear .. STP 2, it works fine
				send_clear(5 , 201)
				#######################################

			except:

				LogInput("Problem communicating with the 2 STP servers ...","STP","STP")

				#######################################
				# Alarm .. BOTH STPs failure
				send_alert(4 , 5 , 200)
				send_alert(5 , 5 , 201)
				#######################################

				raise Exception("Both STP Servers Are Not Reacheable in Retry Failed ...")



	def failed_retry(self,): 
			
			failed_numbers = STP_db.objects.filter(status="Failed")
			

			if failed_numbers:

				# Object initialization
				STP_obj = STP( self.host , self.port ) # Initial communication data should go here
				
				# Let the object work on updating STP and updating NPG's DB
				LogInput('Initializing main STP object ...',"STP","STP")

				if STP_obj.connect():
					LogInput("System connected ... ","STP","STP")
					
					if STP_obj.write_enforce():
						LogInput("Write permission has been enforced ... ","STP","STP")
						
						for NUM in failed_numbers:
							print NUM.command
							print NUM.number	
							if STP_obj.connection_stub(NUM.command)[0]:
								STP_db.objects.filter( status="Failed" , number=NUM.number ).update(status="Added")

						# Saving To STP DB
						if STP_obj.stp_save():
							LogInput('Saved to STP DB ...',"STP","STP")
						else:
							LogInput('ERROR:    Failed to save to STP DB.',"STP","STP")
						
						# Disconnect from STP system
						if STP_obj.disconnect():
							LogInput("Disconnected","STP","STP")
						else:
							LogInput("Failed to Disconnect ...","STP","STP")
		
					
					else:
						LogInput("Write Enforce Failed ...","STP","STP")
						# Disconnect from STP system
						if STP_obj.disconnect():
							LogInput("Disconnected","STP","STP")
						else:
							LogInput("Failed to Disconnect ...","STP","STP")
				else:
					LogInput("Connection Failed ...","STP","STP")
			

			else:
				LogInput('NP Message Should be NPExecuteBroadcast or NPReturnBroadcast, has been passed',"STP","STP")






if __name__ == '__main__':

	LogInput("Application Starting ... ","STP","STP")

	message = dict(PhoneNumber=96566369244, RoutingNumber = 10, MessageTypeName = 'NPExecuteBroadcast')

	init = Initializer()
	init.start(message)





# [Mandatory Command to Connect to STP to start the session)	connect (iid 1, version 1.0)
# [Response For Connect]rsp (iid 1, rc 0, data (connectId 16, side active))

# [Enforcing Write Permission For the session]	begin_txn (iid 1, type write)
# [Successful Response for Write Request]rsp (iid 1, rc 0)

# [Make Routing Entry For MSISDN 96566369244 with RN 05]	ent_sub (iid 1, dn 96566369244, pt 1, rn 05)
# [Success Response For RN Write]rsp (iid 1, rc 0)

# [Retrieve MSISDN Details]	rtrv_sub (iid 1, dn 96566369244)
# [Response for Retrieve Command]rsp (iid 1, rc 0, data (segment 1, dns (dn (id 96566369244, pt 1, rn 05))))

# [Delete MSISDN 96566369244]	dlt_sub (dn 96566369244)
# [Success Response For Delete]rsp (rc 0)

# [Retrieve MSISDN after deleting]	rtrv_sub (iid 1, dn 96566369244)
# [Retrieve Response for non present subscriber]rsp (iid 1, rc 1013)

# disconnect
# rsp (rc 1004, data (reason "Missing paren", location "disconnect{}"))
# [Terminate Session]	disconnect (iid 1)
# rsp (iid 1, rc 1010)Connection closed by foreign host.

# update exising number
# upd_sub(iid 1, dn 96566369244, pt 1, rn 07)

# add more than one number
# ent_sub(iid 1, dn 96566369401, dn 96566369402, dn 96566369403, pt 1, rn 05)

# Delete more than one number
# dlt_sub(iid 1, bdn 96566369401, edn 96566369403)

# Update block of numbers
## upd_sub(iid 1, bdn 96566369401, edn 96566369403, rn 06)

# Retrieve a block of numbers
# rtrv_sub(iid 1, bdn 96566369401, edn 96566369403)
###########################################################
# New set of issued_commands
###########################################################
# connect (iid 1, version 1.0)
# begin_txn (iid 1, type write)
# 1 - Porting out of wataniya
# 	 ent_sub (iid 1, dn 96589887766, pt 1, rn A1, force yes)
# 	 ent_sub (iid 1, dn 96589887766, pt 1, rn A2, force yes)
# 2-  Porting into Wataniya
# 	 ent_sub (iid 1, dn 96589887766, sp A3, force yes)

# 3-  Cross Porting (viva to zain, zain to viva) only
# 	 ent_sub (iid 1, dn 96589887766, pt 2, rn A1, force yes)
# 	 ent_sub (iid 1, dn 96589887766, pt 2, rn A2, force yes)

# end_txn (iid 1)

# dlt_sub (iid 1, dn XXXXXXXXXXX)

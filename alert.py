from django.core.mail import send_mail
from configuration import LogInput, GET_MAILING_LIST
import traceback

def EmailAlert(AlertType,Message=""):
	if AlertType == "PortOut":
		subject = 'NPG Alert - Port Out Failed - ' + Message["PhoneNumberNoCode"]
		to = ['NPG-Port-OUT@wataniya.com','hazim.samour@4gtss.com']
		
		msg = "Hello,"
		msg += "\n\n"
		msg += "The following number faced some issues deactivating."
		msg += "\n"
		msg += "\n"
		msg += "%s      %s\n" % (Message["NPOrderID"],Message["PhoneNumberNoCode"])
		msg += "\n"	
		msg += "Thank you,"	
		msg += "\n"	
		msg += "NPG Alert System"	
		
		try:
			if GET_MAILING_LIST("Port OUT")["enabled"]:
				if send_mail(subject, msg, GET_MAILING_LIST("Port OUT")["from"], GET_MAILING_LIST("Port OUT")["to"], fail_silently=True):
					LogInput("INFO: email was sent:  " + msg,"alerts","portout")
				else:
					LogInput("ERROR: Email wasn't sent: " + str(traceback.format_exc()),"alerts","portout")

		except:
			LogInput("ERROR: Email wasn't sent: " + str(traceback.format_exc()),"alerts","portout")

	elif AlertType == "NPReturn":
		subject = 'NPG Alert - Returned Number - ' + Message["PhoneNumberNoCode"]
		to = ['NPG-NP-Return@wataniya.com','hazim.samour@4gtss.com']
		
		msg = "Hello,"
		msg += "\n\n"
		msg += "The following number has been returned to Wataniya's, please take action accordingly."
		msg += "\n"
		msg += "\n"
		msg += "%s      %s\n" % (Message["NPOrderID"],Message["PhoneNumberNoCode"])
		msg += "\n"	
		msg += "Thank you,"	
		msg += "\n"	
		msg += "NPG Alert System"	
		
		try:
			if GET_MAILING_LIST("NP Return")["enabled"]:
				if send_mail(subject, msg, GET_MAILING_LIST("NP Return")["from"], GET_MAILING_LIST("NP Return")["to"], fail_silently=True):
					LogInput("INFO: email was sent:  " + msg,"alerts","portout")
				else:
					LogInput("ERROR: Email wasn't sent: " + str(traceback.format_exc()),"alerts","npreturn")
		except:
			LogInput("ERROR: Email wasn't sent: " + str(traceback.format_exc()),"alerts","npreturn")


from django import forms
from django.forms import ValidationError
from django.forms import ModelForm 

from django.contrib.auth.models import User

from django.forms.fields import DateField, ChoiceField, MultipleChoiceField
from django.forms.widgets import RadioSelect, CheckboxSelectMultiple
from django.forms.extras.widgets import SelectDateWidget 


            
        
class LoginForm(forms.Form):  
    username    = forms.CharField(label=u'User Name:', max_length=50)  
    password    = forms.CharField(label=u'Password:', widget=forms.PasswordInput(render_value=False))        


class admin_create(forms.Form):
    username = forms.CharField(label = 'Username', max_length=50)
    email = forms.EmailField(label = 'Email')
    password = forms.CharField(label = 'Password',max_length = 30, widget=forms.PasswordInput(render_value=True))
    admin = forms.BooleanField(label='Admin user' , required=False)

    
from django.forms.fields import DateField, ChoiceField, MultipleChoiceField
from django.forms.widgets import RadioSelect, CheckboxSelectMultiple
from django.forms.extras.widgets import SelectDateWidget 

class Moderator(forms.Form):
    select = forms.RadioSelect()
    active = forms.BooleanField(label = 'Admin',required=False, initial = True)
    admin = forms.BooleanField(label = 'Admin',required=False)
    user = forms.CharField(widget=forms.HiddenInput) 
   
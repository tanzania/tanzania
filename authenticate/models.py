from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User


    
class UserProfile(models.Model):
	user = models.OneToOneField(User) 
	admin = models.BooleanField(default = False)
	is_first_login = models.BooleanField(default=True,
        verbose_name= u"Is first login?")
	dealer_id = models.CharField(max_length=50)
	class Meta:
		permissions = (
			("add_request", "Can add new request"),
			("received_requests", "Can see received requests"),
			("sent_requests", "Can see sent requests"),
			("reports", 'Can see reports'),
			("search", 'Can search requests'),
			("show_status_agent_name_in_sent_request_page", "show status/agent name in sent request page")
	        )
    

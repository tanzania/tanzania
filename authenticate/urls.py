from django.conf.urls import patterns, include, url
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import user_passes_test, permission_required, login_required

from authenticate.views import Moderator_view
from authenticate.views import admin_create_view
from authenticate.views import Administration


 #==============================================================================
 # Permission validation Function
 #==============================================================================
def admin_check(user):
	try:
		if user.userprofile.admin:
			return True
	## Return flase and redirect the non-admin authenticated user to the main redirection page.
		else:
			return False
	except:
		## Only users with no profile. Never happens, as all users have profiles when created. 
		return render_to_response('auth/unauthorized.html')
 
permission_needed = user_passes_test(admin_check, login_url = '/login/') 
 #==============================================================================
 # End of permission validation fucntion
 #==============================================================================

 
urlpatterns = patterns('authenticate.views',
	url(r'^$','LoginRequest',), 
	url(r'^administration/$', permission_needed(Administration)),
	url(r'^login/','LoginRequest',), 
	url(r'^logout/$','LogoutRequest',),  
	url(r'^profile/','user_profile'),
	#url(r'^moderator/',login_required(Moderator_view)),
	url(r'^moderator/', permission_needed(Moderator_view)),
	url(r'^admin_create/',permission_needed(admin_create_view)),    
)

urlpatterns += patterns('django.contrib.auth.views',
	url(r'^resetpassword/passwordsent/$', 'password_reset_done', ),
	url(r'^resetpassword/$', 'password_reset'),
	url(r'^reset_confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'password_reset_confirm'),
	url(r'^reset/done/$', 'password_reset_complete'),
	url(r'^change/$', 'password_change'),
	url(r'^change/done/$', 'password_change_done'),
)

## all the django authentication templates default to registration/template_name.html
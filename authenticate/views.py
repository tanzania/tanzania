from django.http import HttpResponseRedirect, HttpResponse
from django import forms 
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from authenticate.models import UserProfile
from authenticate.forms import LoginForm, Moderator, admin_create
from django.contrib.auth.models import User
from django.template.response import TemplateResponse
from authenticate.models import UserProfile
from django.db.models import Q
from django.contrib.auth.decorators import user_passes_test, permission_required
from django.views.decorators.csrf import csrf_protect


from configuration import LogInput


@csrf_protect
def Administration(request):
	PageName = "Administration"
	LogInput('Inside SentRequest')
	
	return render_to_response('auth/index.html', locals(), context_instance = RequestContext(request))


@csrf_protect
def Moderator_view(request):
	
	users = User.objects.all()
	
	if request.POST:
		
		form = Moderator(request.POST)
		if form.is_valid():
			
			active = form.cleaned_data['active']
			admin = form.cleaned_data['admin']  
			user_id = form.cleaned_data['user']
			
			delete = request.POST.get('Delete', False)
			update = request.POST.get('Update', False)
			
			if delete:
				
				## When you delete it, you delete all the related records :))
				User.objects.get(pk=user_id).delete()
				
			elif update:
				
				user = User.objects.get(pk=user_id)
				user.is_active = active
				user.save()
				
				profile = UserProfile.objects.get(user = user)
				profile.admin = admin
				profile.save()
				
			else:
				return HttpResponse('You try to hack us !!!')	
					
			return render_to_response(
				'auth/moderator.html', 
				{
					'form' : form,
					'users' : users, 
					'valid' : [active, admin, user_id, delete],													
				},
				context_instance = RequestContext(request)
			)
	else: 
		
		form = Moderator()
				
	return render_to_response(
		'auth/moderator.html',
		{
			'form' : form,
			'users' : users,
		},
		context_instance=RequestContext(request)
	)	
	  
		
@csrf_protect
def admin_create_view(request): 
	if request.method == 'POST':
		form = admin_create(request.POST)
		
		if form.is_valid():
			
			username = form.cleaned_data['username']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			admin = form.cleaned_data['admin']
			
							
			## Checking if username or email is taken.
			if User.objects.filter( Q(username = username) | Q(email = email) ):
				return HttpResponse('user or email is taken .. ')
			
			user_created = User.objects.create_user(username, email, password)
			
			profile = UserProfile(user = user_created, admin = admin )
			
#				profile.user = user_created
#				profile.admin = admin				
							 
			profile.save()
			
			print profile
			
			
			# Value passed from the post
			u = username
			
			if admin:
				message = 'User %s has been created successfully with admin privlieges' % u 
			else:
				message = 'User %s has been created successfully' % u
				
			
			return render_to_response('auth/admin_create.html',{
															'form' : form,
															'mm' : message,
															},context_instance=RequestContext(request)
															
									  )
		else:
			return render_to_response(
				'auth/admin_create.html',
				{
					'form' : form,
					'mm' : 'Problem in processing your data',
				},
				context_instance=RequestContext(request)
			)
	else:
		
		form = admin_create()
				
	return render_to_response('auth/admin_create.html',
							  {'form':form},context_instance=RequestContext(request))
	 
		
	
@csrf_protect
def LoginRequest(request):
	if request.user.is_authenticated():
		return redirect('/request/new/') 
		
	if request.method == 'POST':
		form = LoginForm(request.POST)
		
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			
			cus_authenticated = authenticate(username = username, password = password)
			
			if cus_authenticated is not None:
				login(request, cus_authenticated)
				
				## Catching the "next" passed parameter after successful login when accessing some view fn.
				try:
					if request.GET['next']:
						redirect_url = request.GET['next'] 
						return HttpResponseRedirect(redirect_url) 
					else:
						redirect('/request/new/') 
				except:
					return redirect('/request/new/') 
				
				
			else:
					return render_to_response('auth/login.html',{'form':form},context_instance=RequestContext(request))
		else:
					return render_to_response('auth/login.html',{'form':form},context_instance=RequestContext(request))
		
	else:
		form = LoginForm()
	
	return TemplateResponse(request,
		'auth/login.html',
		{
			'form':form
		},
	)			 

def LogoutRequest(request):
	logout(request)
	return HttpResponseRedirect('/login/')





from django.contrib.auth.models import User
from django.contrib.sessions.models import Session, SessionManager, SessionStore
from authenticate.models import UserProfile
import datetime
 
def user_profile(request):



	#if request.POST:
	u = request.user.get_profile()
	user_profile = UserProfile.objects.all()
	
	#		for user in user_profile:
	#			if user.gender == 'male':
	#				user.gender = 'female'
	#			user.save()	
	
	
	request.session['has_commented'] = True
	
	
	LIST = ' / '.join([
		request.user.username,
		'<br/><br/>',
		
		repr(request.session.get_expiry_age()),
		repr(request.session.get('has_commented',False)),  
		repr(request.session.items()),  
		repr(request.COOKIES.items()),
		
	])
	
	return HttpResponse(LIST) 

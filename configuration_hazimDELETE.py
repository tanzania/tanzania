import datetime
import os

def GetOSBserver(request):
	# Live system
	if request == "active":
		#return "10.192.176.230:9101"
		#return "10.192.176.231:9101"
		#TEST BSCS
		return "10.192.176.10:9140"
		
	# Test system
	if request == "notactive":
		return "10.254.196.5:9126"

def GET_STP_SERVERS(request):
	
	# Primary STP
	if request == "STP1":
		return {
					'server' : '10.188.32.83',
					'port' : '5873',
					'user' : '', 
					'password' : '', 
				}

	# Secondory STP
	if request == "STP2":
		return {
					'server' : '10.254.238.61',
					'port' : '5873', 
					'user' : '', 
					'password' : '', 
				}	

	'''
	# TEST STP
	if request == "STP1":
		return {
					'server' : '10.188.32.182',
					'port' : '5873', 
					'user' : '', 
					'password' : '', 
				}	
	'''
def GetRequest(request):

	MessageDict={
		99 : "ErrorNotification",									# Custome we added
		1 : "NPRequest",													# Kuwait Spec
		2 : "NPRequestConfirmation",							# Kuwait Spec
		3 : "NPRequestReject",										# Kuwait Spec
		4 : "NPRequestCancel",										# Kuwait Spec
		7 : "NPDonorReject",											# Kuwait Spec
		8 : "NPDonorAccept",											# Kuwait Spec
		9 : "NPExecuteCancel",										# Kuwait Spec
		12 : "NPExecuteBroadcast",								# Kuwait Spec
		14 : "NPReturn",													# Kuwait Spec
		13 : "NPReturnCancel",										# Kuwait Spec
		16 : "NPReturnBroadcast",									# Kuwait Spec
		
		19 : "NPActivated",												# Kuwait Spec
		20 : "NPDeactivated",												# Kuwait Spec
		
		25 : "NPBillingResolution",								# Sudan Spec
		26 : "NPBillingResolutionEnd",						# Sudan Spec
		27 : "NPBillingResolutionReceived",				# Sudan Spec
		28 : "NPBillingResolutionAlert",					# Sudan Spec
		29 : "NPBillingResolutionAlertReceived",	# Sudan Spec
		30 : "NPBillingResolutionError",					# Sudan Spec

		35 : "NPRingFenceRequest",								# Kuwait Spec
		36 : "NPRingFenceDeny",										# Kuwait Spec
		37 : "NPRingFenceApprove",								# Kuwait Spec
		45 : "NPEmergencyRestore",								# Kuwait Spec
		46 : "NPEmergencyRestoreDeny",						# Kuwait Spec
		47 : "NPEmergencyRestoreApprove",					# Kuwait Spec
		22 : "FinalBillNotification",							# Kuwait Spec
		98 : "MessageAck",												# Custome we added
	}
	
	ReverseMessageDict = dict([(v,k) for (k,v) in MessageDict.iteritems()])
	
	if MessageDict.get(request):
		return MessageDict.get(request)
	else:
		return ReverseMessageDict.get(request)
	
################################
def GetStatus(request):
	
	# all available status, start with status ID, status Name, Status description
	StatusDict={
		1 : ("Request Sent", "Request sent waiting SMS confirmation"),
		2 : ("Request Rejected", "Request rejected invalid information"),
		3 : ("Request Rejected", "Request rejected SMS wasn't received"),
		4 : ("Request Canceled", "Request canceled"),
		5 : ("Request Confirmed", "Request confirmed by customer's SMS"),
		6 : ("Request Rejected", "Request rejected by donor"),
		7 : ("Request Accepted", "Request accepted by donor"),
		8 : ("Request Accepted", "Request auto accepted by CRDB"),
		9 : ("Activation Request Received", "Request has been activated"),
		10 : ("Number Activated", "Porting has been completed"),
		11 : ("Request Execution Canceled", "Request execution canceled"),
		12 : ("Request Deactivated", "Request has been deactivated"),

		40 : ("Emergency Restore Active", ""),
		41 : ("Emergency Restore Denied", "Denied by the CRDB help desk"),
		42 : ("Emergency Restore Approve", "Approved by the CRDB help desk"),
		43 : ("Emergency Restore failed", "Emergency Restore validation failed"),

		50 : ("Ring fence sent", "New ring fence awaiting help desk approval"),
		51 : ("Ring fence approved", "Ring fence approved and active"),
		52 : ("Ring fence failed", "Ring fence validation failed"),
		53 : ("Ring fence denied", "Ring fence rejected by help desk"),

		70 : ("NPReturn Active", "NPReturn Active"),
		71 : ("NPReturn Failed", "NPReturn validation failed"),
		72 : ("NPReturn Canceled", "NPReturn Canceled"),
		73 : ("NPReturn Complete", "NPReturn Complete"),
		74 : ("NPReturn Cancel Failed", "NPReturn cancel failed"),
		
		150 : ("Waiting Internal", "Waiting Internal System Update"),
		151 : ("Agent Create Account", "After account creating please activate"),
	}
	
	if request == "StatusDict":
		return StatusDict
	else:
		return StatusDict.get(request)	
################################

def GetOperator(request):

	OperatorDict={
		0 : "NPCS",
		2 : "Zain",
		3 : "Wataniya",
		4 : "Viva",
		12 : "Zain2",
		13 : "Wataniya2",
		14 : "Viva2",
	}
	
	ReverseOperatorDict = dict([(v,k) for (k,v) in OperatorDict.iteritems()])
	
	if OperatorDict.get(request):
		return OperatorDict.get(request)
	else:
		return ReverseOperatorDict.get(request)
	
	################################


def GetConfig(request):
	
	##################################
	# IP Acceptance
	if request == "ENABLE_IP_SCREENING":
		#return 'True'
		return 'False'

	if request == "ALLOWED_IP":
		return '192.168.5.3'

	##################################
	# Django Settings
	if request == "COMPANY_NAME":
		return '4GTSS'

	if request == "SERVER_URL":
		return 'http://81.22.20.83:8201'

	if request == "ROOT_DIR":
		return '/4gtss/mount'

	if request == "DJANGO_DIR":
		return '/site/npg_hazim'

	##################################
	# Django Settings
	if request == "DEBUG":
		#return 'True'
		return 'False'

	if request == "TIME_ZONE":
		return 'Asia/Bahrain'

	##################################
	# Default/Main Database Settings
	if request == "DEFAULT_DB_HOST":
		return '127.0.0.1'

	if request == "DEFAULT_DB_NAME":
		return 'npg_system_hazim'

	if request == "DEFAULT_DB_USER":
		return 'npg_hazim'

	if request == "DEFAULT_DB_PASSWORD":
		return 'npg_hazim'

	##################################
	# Voice Gateway Database Settings
	if request == "ENABLE_VOICEGATWAY_UPDATE":
		#return 'TRUE'
		return 'FALSE'
	
	# Keep duplicate of voice gateway database locally 
	if request == "ENABLE_LOCALDB_UPDATE":
		#return 'TRUE'
		return 'FALSE'

	if request == "VGW_DB_NAME":
		return 'npg_system_demo2'
		#return 'vgwlive'

	if request == "VGW_TABLE_NAME":
		return 'npg_vgw'

	if request == "VGW_DB_HOST":
		return 'localhost'

	if request == "VGW_DB_USER":
		return 'npg_demo'

	if request == "VGW_DB_PASSWORD":
		return 'pNcKyYw3qUyP7MxR'

	##################################
	# Operator's Settings
	if request == "ROUTING_NUMBER":
	#MTN
	# #return '403'
	# #Sudani
	    return 'A6'

	if request == "SENDER_ID":
		return "03"

	##################################
	# Central System Settings - SUDS Connection
	if request == "SOAP_CONNECTION":
		#return 'TEST'
		return 'LIVE'

	# For Live server
	if request == "CSL":
		return {
					'url' : 'https://m2m.kw.numport.net/services/NpcdbService?wsdl',
					# MTN
					'user' : 'soap_03',
					'password' : 'soap_03',
					
				}

	# For the test server
	if request == "CST":
		return {
					'url' : 'https://m2mnpkw.test.systor.st/services/NpcdbService?wsdl',
					'user' : 'soap_03', 
					'password' : 'soap_03', 
				}
				
	# For MTN TABS
	if request == "MTN TABS":
		return {
					'url' : 'https://m2mnpsd.test.systor.st/services/NpcdbService?wsdl',
					'user' : 'test2_25', 
					'password' : 'test2_25', 
				}

	##################################
	# XSD Settings
	if request == "XSD_LOCATION":
		return GetConfig("ROOT_DIR") + GetConfig("DJANGO_DIR") + '/soap/xsd/nptypes.xsd'

		# for windows use: 'M:/site/hazimsite/wsdl/nptypes.xsd'
		# for Linux use:   GetConfig("ROOT_DIR") + GetConfig("DJANGO_DIR") + '/wsdl/nptypes.xsd'

	# File attachment
	if request == "STORAGE":
		return GetConfig("ROOT_DIR") + GetConfig("DJANGO_DIR") + '/storage/'

	##################################
	# E-Mail Settings
	if request == "ENABLE_EMAIL":
		return 'TRUE'

	if request == "AUTHENTICATE_USER":
		return 'noreply@lightspeed.local'

	if request == "AUTHENTICATE_PASS":
		return 'lightspeed'

	if request == "FROM_EMAIL":
		return 'noreply@4gtss.com'

	if request == "RECIPIENT_LIST":
		return [
					'hazim.samour@solutionsfortelecom.com', 
				]

	##################################
	# Allowed upload files

	if request == "FILE_FORMAT":
		return ('.pdf','.tiff','.jpg')

##################################
from django.utils.encoding import smart_str
import codecs
# Logging Function
def LogInput(NewLog, NPOrderID="", Function=""):
	now = datetime.datetime.now()

	year = "0" + str(now.year) if len(str(now.year)) == 1 else str(now.year)
	month = "0" + str(now.month) if len(str(now.month)) == 1 else str(now.month)
	day = "0" + str(now.day) if len(str(now.day)) == 1 else str(now.day)
	'''
	dir = GetConfig("ROOT_DIR") + GetConfig("DJANGO_DIR") + '/log/' + year
	
	if not os.path.exists(dir):
		os.makedirs(dir)
	'''
	dir = "/mnp/log/live/" + '/log/' + year + "/" + month + "/" + day + "/"
	if not os.path.exists(dir):
		os.makedirs(dir)

	LogFileName = dir + "Log-" + str(NPOrderID) + "-" + str(Function)
	LogFile = codecs.open(LogFileName, "a",'utf-8')
	
	TimeStamp = "[%s]  " % (GetTimeStamp("log"))
	
	LogFile.write("".join(TimeStamp))
	
	#LogFile.write("".join(NewLog))
	#LogFile.write("".join(NewLog.decode("utf-8")))
	#LogFile.write("".join(smart_str(NewLog)))
	LogFile.write("".join(NewLog.decode('utf-8'))) 
	
	
	LogFile.write("".join("\n"))
	
	#LogFile.close()
##################################

def LogMessageResutls(InMessage):
	LogMessage = ""
	LogMessageDB = "], DB["
	
	# this is if the message is a dictinaroy
	try:
		#LogInput('inside LogMessageResutls the first TRY')
		try:
			LogMessage = "%s[" % GetRequest(int(InMessage['MessageTypeID']))
		except:
			LogMessage = "unset["
			
		#LogInput('LogMessage' + LogMessage)
		for key,value in InMessage.iteritems():
			if key in ('AttachmentList'):
				LogMessage += "%s=<%s>; " % (key,key)
			else:
				LogMessage += "%s=%s; " % (key,value)
		#LogInput('LogMessage' + LogMessage)

		LogMessage = LogMessage[:-2]
		return LogMessage + "]"

	# this is if the message is a instance
	except:
		try:
			LogMessage = "%s[" % GetRequest(int(InMessage['MessageTypeID']))
		except:
			LogMessage = "unset["
			
		
		messages = InMessage._meta.fields
			
		for message in messages:
			key = message.name
			value = eval("InMessage.%s" % key)
			
			if key not in ("MessageLocalID","message_created","message_modified","direction","new_message","related") and value is not None:
				LogMessage += "%s=%s; " % (key,value)
			elif key in ("MessageLocalID","message_created","message_modified","direction","new_message","related") and value is not None:
				LogMessageDB += "%s=%s; " % (key,value)
			else:
				pass
	
	LogMessage = LogMessage[:-2]
	LogMessageDB = LogMessageDB[:-2]
	
	return LogMessage + LogMessageDB + "]"


'''  # To be fixed by Bahaa and used for better preformance
def LogMessageResutls(InMessage):
	LogMessage = ""
	LogMessageDB = "], DB["
	
	LogInput("The InMessage type is : " + type(InMessage))
	
	# this is if the message is a dictinaroy, we can check for type of incoming message
	try:
		if isinstance(InMessage,dict):
			#LogInput('inside LogMessageResutls the first TRY')
			LogMessage = "%s[" % GetRequest(int(InMessage['MessageTypeID']))
			#LogInput('LogMessage' + LogMessage)
			for key,value in InMessage.iteritems():
				if key in ('AttachmentList'):
					LogMessage += "%s=<%s>; " % (key,key)
				else:
					LogMessage += "%s=%s; " % (key,value)
			#LogInput('LogMessage' + LogMessage)
		
			LogMessage = LogMessage[:-2]
			return LogMessage + "]"
		
		# this is if the message is a instance
		else:
			LogMessage = "%s[" % GetRequest(int(InMessage.MessageTypeID))

			messages = InMessage._meta.fields

			for message in messages:
				key = message.name
				value = eval("InMessage.%s" % key)

				if key not in ("MessageLocalID","message_created","message_modified","direction","new_message") and value is not None:
					LogMessage += "%s=%s; " % (key,value)
				elif key in ("MessageLocalID","message_created","message_modified","direction","new_message") and value is not None:
					LogMessageDB += "%s=%s; " % (key,value)
				else:
					pass

	except Exception as error:
		LogInput(error)
		return "There was an error..!!!!"

	LogMessage = LogMessage[:-2]
	LogMessageDB = LogMessageDB[:-2]

	return LogMessage + LogMessageDB + "]"

'''
def GetTimeStamp(Type):
	now = datetime.datetime.now()

	year = "0" + str(now.year) if len(str(now.year)) == 1 else str(now.year)
	month = "0" + str(now.month) if len(str(now.month)) == 1 else str(now.month)
	day = "0" + str(now.day) if len(str(now.day)) == 1 else str(now.day)
	hour = "0" + str(now.hour) if len(str(now.hour)) == 1 else str(now.hour)
	minute = "0" + str(now.minute) if len(str(now.minute)) == 1 else str(now.minute)
	second = "0" + str(now.second) if len(str(now.second)) == 1 else str(now.second)
	millisecond = str(now.microsecond / 1000)
	millisecond = "0" + millisecond if len(millisecond) == 2 else millisecond
	millisecond = "00" + millisecond if len(millisecond) == 1 else millisecond

	if Type == "file":
		return "%s_%s_%s_%s_%s_%s" % (day,month,year,hour,minute,second)

	if Type == "log":
		return "%s-%s-%s %s:%s:%s,%s" % (year,month,day,hour,minute,second,millisecond)

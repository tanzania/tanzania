from __future__ import absolute_import
from celery import Celery

celery = Celery('forker.celery',
                broker='amqp://npg_hazim@127.0.0.1:5672/npg_hazim',
                backend='amqp://npg_hazim@127.0.0.1:5672/npg_hazim',
                include=['forker.tasks'])

# Optional configuration, see the application user guide.
celery.conf.update(
    CELERY_TASK_RESULT_EXPIRES=3600,
)

if __name__ == '__main__':
    celery.start()
from celery.task.schedules import crontab 
from celery.decorators import periodic_task , task
#from celery import task
import celery

from STP_app.stp import Initializer, Failed_Retry
from int_ras.ras_process import IncomingAcceptPostpaid, IncomingAcceptPrepaid
from int_ras.models import IntegrationRAS
from int_osb.osb_proccess import OSB_int
from int_osb.bill_notification import BillNotification
from reports.views import Reports
from time import sleep
import datetime
from django.utils import timezone
from web.models import NpMessages
from django.core.mail import send_mail
from configuration import LogInput, GET_MAILING_LIST, GetRequest
import traceback

#from celery import Celery
#celery = Celery('tasks', backend='amqp', broker='amqp://npg_hazim:npg_hazim@127.0.0.1:5672/npg_hazim')


# @celery.task( 
  # ignore_result = True, 
  # max_retries = None, 
  # default_retry_delay = 5 * 60
  # run_every = timedelta(seconds = 50) ## For periodic tasks
  # routing_key = 'foo.bars'
  # )

@celery.task()
def add(x, y):
 
 sleep(5)
 print 'celery is printing things: ' + str( datetime.datetime.now() )
 return x + y

## time limit overrides default retry delay
#@celery.task(default_retry_delay=5, max_retries = 3,)
#@task(default_retry_delay=5, max_retries = 2, queue="lineup", time_limit=10 )
#@celery.task(default_retry_delay=5, max_retries = 2, queue="lineup", time_limit=10 )



@celery.task(queue="stp")
def stp_teller(inmessage):
 try:
  print 'Inside STP Teller'
  init = Initializer(inmessage)
  init.start(inmessage)
 except Exception, exc:
  stp_teller.retry( exc=exc, countdown=5 )
 try:
  return str(inmessage.NPOrderID)
 except:
  pass

@periodic_task(queue="sch_reports",run_every=crontab(hour=23, minute=59))
def GenerateAllReports(day="",month="",year=""):
 today = datetime.datetime.now()
 if day:
 	useDay = day
 else:
 	useDay = today.day
 if month:
 	useMonth = month
 else:
 	useMonth = today.month
 if year:
 	useYear = year
 else:
 	useYear = today.year
 Report = Reports(day=useDay,month=useMonth,year=useYear,Force=True)
 Report.PortOutReport()
 Report.PortInReport()
 try:
  return "GenerateAllReports"
 except:
  pass

'''
@periodic_task(queue="sch_alerts",run_every=crontab())
def TESTSendEmailPortIN():
	#This is an alert email will be sent every 6 hours to 3 emails
	#This task will find all the port in request that are awaiting activation and has not been activated.
	
	from web.models import NpMessages
	from django.core.mail import send_mail
	from configuration import LogInput
	import traceback

	FoundMessages = NpMessages.objects.filter(Originator="NPG", MessageTypeName="NPRequest", status__in = [7,8])

	subject = 'NPG Alert - Port IN Timing Out'
	msg = "Hello,"
	msg += "\n\n"
	msg += "The following orders require your attention before timing out:"
	msg += "\n"
	msg += "\n"
	msg += "     Order ID                Phone #                 Order Time                   User"
	msg += "\n"
	for each in FoundMessages:
		msg += "%s      %s      %s      %s\n" % (each.NPOrderID, each.ValidationMSISDN,each.message_modified.strftime("%Y-%m-%d %I:%M %p"),each.user)

	msg += "\n"	
	msg += "Thank you,"	
	msg += "\n"	
	msg += "NPG Alert System"	
	
	try:
		if send_mail(subject, msg, 'mnp-npg@wataniya.com', ['bahaa.latif@4gtss.com','hazim.samour@4gtss.com'], fail_silently=True):
			LogInput("INFO: email was sent:  " + msg,"alerts","alerts")
	except:
		LogInput("ERROR: Email wasn't sent: " + str(traceback.format_exc()),"alerts","alerts")

	return "SendEmailPortIN"
'''

@periodic_task(queue="sch_alerts",run_every=crontab(minute=0, hour='1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23'))
#@periodic_task(queue="alerts",run_every=crontab())
def SendEmailPortIN():
	'''
	This is an alert email will be sent every 6 hours to 3 emails
	This task will find all the port in request that are awaiting activation and has not been activated.
	
	'''
	if GET_MAILING_LIST("Port IN")["enabled"]:
		FoundMessages = NpMessages.objects.filter(Originator="NPG", MessageTypeName="NPRequest", status__in = [7,8])
	
		subject = 'NPG Alert - Port IN Timing Out'
		msg = "Hello,"
		msg += "\n\n"
		msg += "The following orders require your attention before timing out:"
		msg += "\n"
		msg += "\n"
		msg += "     Order ID                Phone #                 Order Time                   User"
		msg += "\n"
		for each in FoundMessages:
			msg += "%s      %s      %s      %s\n" % (each.NPOrderID, each.ValidationMSISDN,each.message_modified.strftime("%Y-%m-%d %I:%M %p"),each.user)
	
		msg += "\n"	
		msg += "Thank you,"	
		msg += "\n"	
		msg += "NPG Alert System"	
		
		if FoundMessages:
			try:
				if send_mail(subject, msg, GET_MAILING_LIST("Port IN")["from"], GET_MAILING_LIST("Port IN")["to"], fail_silently=True):
					LogInput("INFO: email was sent:  " + msg,"alerts","alerts")
			except:
				LogInput("ERROR: Email wasn't sent: " + str(traceback.format_exc()),"alerts","alerts")
		else:
			LogInput("INFO: SendEmailPortIN no message was found","alerts","alerts")
	else:
		LogInput("INFO: Port IN alert disabled...","alerts","alerts")
	return "SendEmailPortIN"

@periodic_task(queue="sch_notification",run_every=crontab(hour=11, minute=31))
def DailyBillNotification():
  NewProcess = BillNotification()
  NewProcess.DailyRun()
  return "DailyBillNotification"

@celery.task(queue="portoutVerfiy")
def InRequestTask(inmessage):
 NewProcess = OSB_int(inmessage)
 NewProcess.PortOutProcess()
 return str(inmessage.NPOrderID)

@celery.task(queue="RingFence")
def IncomingRingFenceTask(inmessage):
  NewProcess = OSB_int(inmessage)
  NewProcess.RingFence()
  return str(inmessage.NPOrderID)

@celery.task(queue="RingFence")
def RemoveRingFenceTask(inmessage):
  NewProcess = OSB_int(inmessage)
  NewProcess.RemoveRingFence()
  return str(inmessage.NPOrderID)

@celery.task(queue="portin")
def InAcceptPostTask(inmessage):
 #IncomingAcceptPostpaid
 IncomingAcceptPostpaid(inmessage)
 return str(inmessage.NPOrderID)

@celery.task(queue="portin")
def InAcceptPreTask(inmessage):
 #IncomingAcceptPrepaid
 IncomingAcceptPrepaid(inmessage)
 return str(inmessage.NPOrderID)

@periodic_task(queue="sch_npreturn",run_every=crontab(hour=16, minute=31))
def OutNPReturn():
 from int_osb.npreturn import SendNPReturn
 SendNPReturn()
 return "OutNPReturn"

@celery.task(queue="npreturnIN")
def InNPReturn(inmessage):
 NewProcess = OSB_int(inmessage)
 NewProcess.NPReturn()
 return str(inmessage.NPOrderID)

@celery.task(queue="portout")
def InActivatedTask(inmessage):
 NewProcess = OSB_int(inmessage)
 NewProcess.IncomingNPActivate()
 return str(inmessage.NPOrderID)

@celery.task(queue="portin")
def InBroadcastTask(inmessage):
 NewProcess = OSB_int(inmessage)
 NewProcess.PortInExecute()
 return str(inmessage.NPOrderID)

@celery.task(queue="portin")
def InAcceptTask(inmessage):
 print("starting PortINProcess")
 try:
  NewProcess = OSB_int(inmessage)
  NewProcess.PortINProcess()
 except:
  print("failed to try PortINProcess")
 print("done PortINProcess")
 
 #RASMessage = IntegrationRAS.objects.get(np_messages=inmessage, PhoneNumber="RANGE")
 #print("finding RASMessage > " + str(RASMessage))
 
 for RASMessage in inmessage.IntegrationRAS.all():
  if RASMessage.PhoneNumber == "RANGE":
   if RASMessage.SubscriptionType == "prepaid":
    print("prepaid")
    IncomingAcceptPrepaid(inmessage)
    #InAcceptPreTask.delay(inmessage)
  
   elif RASMessage.SubscriptionType == "postpaid":
    print("postpaid")
    IncomingAcceptPostpaid(inmessage)
    #InAcceptPostTask.delay(inmessage)
 
 # adding the auto activation, to avoid manual activation by agents
 LogInput("INFO:    Preparing auto activation message to CRDB",str(inmessage.NPOrderID),"process")
 try:
  from soap.handle_out_message import HandleOutgoingMessage
  NewMessage = NpMessages(
   MessageTypeID = GetRequest('NPActivated'),
   NPOrderID = str(inmessage.NPOrderID),
   user = "NPG Auto",
  )
  NewMessage.save()
  Respond = HandleOutgoingMessage(NewMessage, None)
  LogInput("INFO:    Auto activation sent, the response: " + str(Respond),str(inmessage.NPOrderID),"process")
 except:
  LogInput("ERROR:    Auto activation failed: " + str(traceback.format_exc()),str(inmessage.NPOrderID),"process")

 return str(inmessage.NPOrderID)



'''
@periodic_task(run_every=60 , queue="lineup" , time_limit=10, )
def retrier():
 try:
  print 'Retrier is working on failed numbers ...'
  FR = Failed_Retry() 
  FR.failed_retry()
 except Exception, exc: 
  retrier.retry(exc=exc, countdown=5,)  
'''


'''
try:
 raise Exception('Too much  things to do isA')
 init = Initializer()
 init.start(inmessage)
except Exception, exc: 
 stp_teller.retry( args=[inmessage,],exc=exc ) ## we can overrid default values here
'''
'''
try:
  raise Exception('Too much  things to do isA')
  init = Initializer()
  init.start(inmessage)
 except Exception, exc:
        stp_teller.retry(exc=exc) ## we can overrid default values here
'''


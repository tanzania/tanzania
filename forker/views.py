# Create your views here.

from configuration import GetConfig, LogInput
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

# Importing my tasks
from tasks import add

from time import sleep


def index(request):
	result = add.delay(140,139)
	
	## Result has those attributes:
	# 'app', 'backend', 'build_graph', 'get_leaf', 'graph', 'collect', 
	# 'failed', 'forget',  'iterdeps', 'revoke', 
	# 'parent', 'children', 
	# 'state', 'status', 'successful', 'supports_native_join', 'serializable',
	# 'task_id',  'id','task_name', 'traceback', 'wait'
	# 'info', 'ready', 'result', 'get',

	## possible usage options of results
		# >>> result = add.delay(4, 4)
		# >>> result.ready() # returns True if the task has finished processing.
		# False
		# >>> result.result # task is not ready, so no return value yet.
		# None
		# >>> result.get()   # Waits until the task is done and returns the retval.
		# 8
		# >>> result.result # direct access to result, doesn't re-raise errors.
		# 8
		# >>> result.successful() # returns True if the task didn't end in failure.
		# True
	
	# Hold on till the result processing completed, otherwise the status/state will be "PENDING"
	sleep(4) 		   ## if less than 3 seconds, it will be still pending or in "PENDING" state

	if result.successful():
		return HttpResponse("successful background process ..<br/>" +
							"result.ready: " 		+ str(result.ready()) 		+ '<br/>' +
							"result.wait: " 		+ str(result.wait()) 			+ '<br/>' +
							"result.result: " 		+ str(result.wait()) 			+ '<br/>' +
							"result.state: " 		+ str(result.state)			+ '<br/>' +
							"result.status: " 		+ str(result.status)		+ '<br/>' +
							"result.info: " 		+ str(result.info) 			+ '<br/>' +
							"result.children: " 	+ str(result.children) 		+ '<br/>' +
							"result.parent: " 		+ str(result.parent)		+ '<br/>' +
							"result.task_id: " 		+ str(result.task_id) 		+ '<br/>' +
							"result.id: " 			+ str(result.id) 			+ '<br/>' +
							"result.task_name: " 	+ str(result.task_name) 	+ '<br/>' 
							
							)
	else:	
		return HttpResponse("Check failed ... " + result.status)
	

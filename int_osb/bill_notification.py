from web.models import NpMessages, FinalBillNotification
from int_osb.services import GetCustomerBalance, GetContractInfo
from configuration import LogInput, GetRequest, GetConfig
from datetime import date, timedelta
import traceback

class BillNotification():

	def __init__(self,):
		SMS1ToDate = date.today() - timedelta(42)
		SMS1FromDate = date.today() - timedelta(43)
		SMS2ToDate = date.today() - timedelta(50)
		SMS2FromDate = date.today() - timedelta(51)
		SMS3ToDate = date.today() - timedelta(58)
		SMS3FromDate = date.today() - timedelta(59)
		self.AllSMSList = [(SMS1ToDate,SMS1FromDate),(SMS2ToDate,SMS2FromDate),(SMS3ToDate,SMS3FromDate)]
		LogInput("INFO:   Generating all boradcast message for the following dates ...." + str(self.AllSMSList) ,"Bill","Notification") 

		self.StartDay = "2013-06-16"
		self.ToDate = date.today() - timedelta(55)
		self.FromDate = date.today() - timedelta(58)
		
	def GetTodayMessages(self,):
		pass

	def RetryNotAbleToFetch(self,):
		FoundNot = FinalBillNotification.objects.filter(status = "Not able to fetch")
		Allmsg=[]
		for each in FoundNot:
			Allmsg.append(each.np_messages)
		self.HandelMessages(Allmsg)
	
	def GetBroadcastMessages(self,FromDate,ToDate):
		return NpMessages.objects.filter(PortingTime__range=[FromDate, ToDate], MessageTypeName="NPExecuteBroadcast", DonorName = GetConfig("COMPANY_NAME"))

	def HandelMessages(self,AllMessages):
		for msg in AllMessages:
			LogInput("INFO:   Handling order ...." + str(msg.NPOrderID),"Bill","Notification")
			DataBaseMessage = self.SetStatus(DBmsg = self.SetDBMessage(msg), TotalAmount = self.GetTotalAmount(msg))
			if DataBaseMessage.status == "Fetched from BSCS":
				self.UpdateStatusAndSave(DataBaseMessage)
			else:
				DataBaseMessage.save()
			#self.SendTONPS(DataBaseMessage)
			#DataBaseMessage = self.UpdateStatus(DataBaseMessage)

	def main(self,):
		AllMessages = self.GetBroadcastMessages(self.StartDay, self.ToDate)
		self.HandelMessages(AllMessages)
		
	def UpdateStatusAndSave(self,NewEntry):
		result = self.SendTONPS(NewEntry)
		try:
			FoundNot = FinalBillNotification.objects.get(NPOrderID=NewEntry.NPOrderID)
			try:
				if result == 200:
					NewEntry.pk = FoundNot.pk
					NewEntry.date_created = FoundNot.date_created
					if FoundNot.status == "First Notification sent":
						NewEntry.status = "Second Notification sent"
					elif FoundNot.status == "Second Notification sent" or FoundNot.status == "Second Notification failed to fetch amount":
						NewEntry.status = "Third Notification sent"
					NewEntry.save()
			except:
				NewEntry.pk = FoundNot.pk
				NewEntry.date_created = FoundNot.date_created
				if FoundNot.status == "First Notification sent":
					NewEntry.status = "Second Notification failed to fetch amount"
				elif FoundNot.status == "Second Notification sent":
					NewEntry.status = "Third Notification sent failed to fetch amount"
				NewEntry.save()
		except FinalBillNotification.DoesNotExist:
			if result == 200:
				NewEntry.status = "First Notification sent"
				NewEntry.save()
			else:
				print("Respond from NPS not 200, nothing done")
		
	'''
	def FirstTimeAll(self,):
		msgs = FinalBillNotification.objects.filter(status="Failed to send to NPS", Balance__gte = float(5.000))
		for each in msgs:
			result = self.SendTONPS(each)
			if result == 200:
				each.status = "First Notification sent"
				print "200 first not"
			else:
				each.status = "Failed to send to NPS"
				print "not 200 failed"
			each.save()
	'''
	def FirstTimeAll(self,):
		ToDate = self.FromDate
		FromDate = self.StartDay
		AllBCMsg = self.GetBroadcastMessages(FromDate=FromDate,ToDate=ToDate)
		self.HandelMessages(AllBCMsg)

	def SendTONPS(self,order):
		from soap.handle_out_message import HandleOutgoingMessage
		result = HandleOutgoingMessage(self.NewNpMessage(order), "NPG")
		return result[0]
	
	def DailyRun(self,):
		LogInput("INFO:   DailyRun started ....","Bill","Notification")
		for eachDate in self.AllSMSList:
			LogInput("INFO:   starting for range of  ...." + str(eachDate),"Bill","Notification")
			ToDate = eachDate[0]
			FromDate = eachDate[1]
			AllBCMsg = self.GetBroadcastMessages(FromDate=FromDate,ToDate=ToDate)
			self.HandelMessages(AllBCMsg)
	
	def NewNpMessage(self,order):
		NewMessage = NpMessages(
			MessageTypeID = GetRequest('NPFinalBillNotification'),
			NPOrderID = order.NPOrderID,
			OriginalNPOrderID = order.NPOrderID,
			Balance = order.Balance,
			DueDate = order.DueDate,
			user = "NPG",
		)
		NewMessage.save()
		return NewMessage


	def SetStatus(self,DBmsg,TotalAmount):
		if TotalAmount == "Failed":
			DBmsg.status = "Not able to fetch"
		else:
			DBmsg.status = "Fetched from BSCS"
			DBmsg.Balance = TotalAmount
			if TotalAmount < 0.999:
				DBmsg.status = "Amount is not covered"
			if TotalAmount == 0.000:
				DBmsg.status = "Amount is zero"
			if TotalAmount < 0.000:
				DBmsg.status = "Amount is negative"
		LogInput("INFO:   Status set to  ...." + str(DBmsg.status),"Bill","Notification")
		return DBmsg

	def SetDBMessage(self,order):
		return FinalBillNotification(
			np_messages_id = order.pk,
			PortingTime = order.PortingTime,
			NPOrderID = order.NPOrderID,
			DueDate = order.PortingTime + timedelta(90),
		)

	def GetTotalAmount(self,order):
		BSCSmessage = {"NPOrderID" : order.NPOrderID}
		TotalAmount = 0
		for Route in order.RoutingInfoList.all():
			list = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 
			for number in list:
				BSCSmessage["PhoneNumber"] = "965" + str(number)
				LogInput("INFO:   Getting amount for ...." + str(BSCSmessage["PhoneNumber"]),"Bill","Notification")
				Amount = self.GetBalance(BSCSmessage)
				LogInput("INFO:   Amount is ...." + str(Amount),"Bill","Notification")
				if Amount == "Failed":
					return False
				else:
					TotalAmount += Amount
		LogInput("INFO:   Add all amount in order ...." + str(TotalAmount),"Bill","Notification")
		return TotalAmount

	def GetBalance(self,BSCSmessage):
		try:
			BSCSmessage["CustomerID"] = GetContractInfo(BSCSmessage)["CustomerID"]
			return GetCustomerBalance(BSCSmessage)["CurrentBalance"]
		except:
			return "Failed"


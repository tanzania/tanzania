from soap.handle_out_message import HandleOutgoingMessage
from web.models import NpMessages, RoutingInfoList
from configuration import LogInput, GetRequest
import traceback
import os
import datetime
import socket
import time
import os
import traceback

class SendNPReturn():
	# this file has the process to initied an npreturn from files dropped under /mnp/npreturn
	# it will open all files located inside the above folder
	# Fix the preimission 
	# find the numbers inside those files and send an npreturn message to CRDB
	
	def __init__(self,):
		self.path = "/mnp/npreturn"
		DonePath = "/mnp/npreturn/done"
		ErrorPath = "/mnp/npreturn/error"
		
		# fix the premission of all files before working on them
		LogInput("INFO:   Fixing files permissions","NPReturn","Daily")
		try:
			self.ChangeFilePermission()
		except:
			LogInput("ERROR:   Failed to fix files permissions   > " + str(traceback.format_exc()),"NPReturn","Daily") 

		LogInput("INFO:   Files Fixed","NPReturn","Daily")
		
		data = {}
		for dir_entry in os.listdir(self.path):
			dir_entry_path = os.path.join(self.path, dir_entry)
			if os.path.isfile(dir_entry_path) and not dir_entry.startswith('.'):
				with open(dir_entry_path, 'r') as my_file:
					data[dir_entry] = my_file.read()

		if data:
			LogInput("INFO:   Found the following files " + str(data),"NPReturn","Daily")
			for FileName,Data in data.iteritems():
				NumbersList = filter(None, Data.replace('\n',',').replace('\r', ',').replace(' ',',').split(','))
				NumbersList = self.RemoveAreaCode(NumbersList)
				LogInput("INFO:   for file %s found numbers %s " % (FileName,NumbersList),"NPReturn","Daily")

				source = self.path + "/" + FileName

				# make sure file is not empty, and there are numbers
				if NumbersList:
					for number in NumbersList:
						try:
							NewMessage = self.GenerateMessage(number)
						except:
							LogInput("ERROR:   Failed to generate messages " + str(traceback.format_exc()),"NPReturn","Daily")
						
						Respond = HandleOutgoingMessage(NewMessage, "Auto NPG")
						LogInput("INFO:   Result of sending message for above number %s " % (str(Respond)),"NPReturn","Daily")
						
					#print Respond
					# move to the correct folder after complation of sending messages, 
					#so check if the response from CRDB is 200 or if the file is not empty
					if Respond[0] == 200:
						destination = DonePath + "/" + str(datetime.datetime.now()) + "-" + FileName
						os.rename(source, destination)
						LogInput("INFO:   File %s has been moved to done folder  " % (FileName),"NPReturn","Daily")
					else:
						destination = ErrorPath + "/" + str(datetime.datetime.now()) + "-" + FileName
						os.rename(source, destination)
						LogInput("INFO:   File '%s' has been moved to error folder  " % (FileName),"NPReturn","Daily")
				else:
					LogInput("INFO:   No numbers founds inside file","NPReturn","Daily")
					destination = ErrorPath + "/" + str(datetime.datetime.now()) + "-" + FileName + "-EMPTY"
					os.rename(source, destination)
					LogInput("INFO:   File '%s' has been moved to error folder  " % (FileName),"NPReturn","Daily")

				LogInput("INFO:   %s" % (destination),"NPReturn","Daily")
		else:
			LogInput("INFO:   No files found","NPReturn","Daily")

	def RemoveAreaCode(self,NumbersList):
		NewNumbersList = []
		for each in NumbersList:
			if len(each) > 8:
				NewNumbersList.append(each[3:])
			else:
				NewNumbersList.append(each)
		return NewNumbersList

	def ChangeFilePermission(self,):
		# This funciton will fix the permission for the files inside the npreturn folder
		self.s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)#main socket
		self.s.connect(('lmnp2',21))
		self.FTPOutput(self.getline())
		self.FTPOutput(self.execute("USER npreturn"))
		self.FTPOutput(self.execute("PASS NPReturn"))
		self.FTPOutput(self.execute("PASV"))
		for dir_entry in os.listdir(self.path):
			if os.path.isfile(self.path + "/" + dir_entry) and not dir_entry.startswith('.'):
				LogInput("INFO:   Fixing permission for file: " + str(self.path + "/" + dir_entry),"NPReturn","Daily")
				self.FTPOutput(self.execute('SITE CHMOD 777 ' + dir_entry))

	def FTPOutput(self,output):
		LogInput("FTP:   " + str(output),"NPReturn","Daily")
	# Those functions are related to the ftp client to be able to establish the connection and send commands
	def sendcmd(self,cmd):
		self.s.send(cmd+"\r\n")
	def getline(self,):
		return self.s.recv(1024)
	def execute(self,cmd):
		self.sendcmd(cmd)
		return self.getline()
	###################
	
	
	def GenerateMessage(self,number):
		NewMessage = NpMessages(
			MessageTypeID = GetRequest('NPReturn'),
			user = "Auto NPG",
		)
		NewMessage.save()
		NewMessage.RoutingInfoList.create(PhoneNumberStart = number,PhoneNumberEnd = number)
		
		return NewMessage
from web.models import NpMessages,DonorRejectList
from int_osb.services import *
from int_ras.models import IntegrationRAS
from int_ras.services import RingFence as RingFenceRASService
from configuration import LogInput, GetRequest

import soap
from time import sleep
import traceback

class OSB_int():
	def __init__(self,InMessage):
		self.AllPhoneNumbers = []
		self.InMessage = InMessage
		self.MainNPOrderID = InMessage.NPOrderID
		'''
		if InMessage.MessageTypeName == "NPExecuteBroadcast":
			LogInput("Inside osb_proccess if message NPExecuteBroadcast ---",self.MainNPOrderID,"OSB")
			try:
				self.Order = InMessage
	
				for Route in InMessage.RoutingInfoList.all():
					list = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 
					for number in list:
						self.AllPhoneNumbers.append("965" + str(number))			
			except Exception as e:
				# this is vaild for incoming NpRequest only
				for Route in self.InMessage.RoutingInfoList.all():
					list = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 
					for number in list:
						self.AllPhoneNumbers.append("965" + str(number))			
			
		else:
		'''
		try:
			self.Order = NpMessages.objects.get(NPOrderID = InMessage.NPOrderID, MessageTypeName="NPRequest")

			for Route in Order.RoutingInfoList.all():
				list = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 
				for number in list:
					self.AllPhoneNumbers.append("965" + str(number))			
		except Exception as e:
			# this is vaild for incoming NpRequest only
			for Route in self.InMessage.RoutingInfoList.all():
				list = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 
				for number in list:
					self.AllPhoneNumbers.append("965" + str(number))
		
##############################################################################
# Port in process, for each number
##############################################################################
	def PortINProcess(self,):
		# DO THE processs for every single number
		
		LogInput("INFO:  Port IN Process started  (internal) ....",self.MainNPOrderID,"OSB") 
		LogInput("INFO:  Port IN Process started  (Creating List of all number) ....",self.MainNPOrderID,"OSB") 
		
		RASMessage = IntegrationRAS.objects.get(np_messages=self.Order, PhoneNumber="RANGE")
		
		for self.PhoneNumber in self.AllPhoneNumbers:
			self.BSCSMessage = {
				"PhoneNumber" : self.PhoneNumber,
				"NPOrderID" : self.MainNPOrderID,
			}
			LogInput("INFO:  PortINProcess started  (Registering Number) ....",self.MainNPOrderID,"OSB") 
			# Register Phone number
			try:
				PhoneNumberRegisterReturn = PhoneNumberRegister(self.BSCSMessage)
				RASMessage.pk = None
				RASMessage.DNID = PhoneNumberRegisterReturn["DNID"]
				RASMessage.PhoneNumber = self.PhoneNumber
				#print(str(RASMessage))
				RASMessage.save()
			except:
				LogInput("ERROR:   Failed to register phone number ....\n" + str(traceback.format_exc()),self.MainNPOrderID,"OSB") 
			# find contract status, if fails jump to expect
			try:
				LogInput("INFO:  PortINProcess started  (Finding contract) ....",self.MainNPOrderID,"OSB") 

				ContractInfo = GetContractInfo(self.BSCSMessage)
				
				# If contract is suspended
				if ContractInfo["ContractStatus"] == "3":
					LogInput("PortINProcess started  (Contract is suspended) ....",self.MainNPOrderID,"OSB") 

					NewMessage = NpMessages(
						MessageTypeID = GetRequest('NPActivated'),
						NPOrderID = str(self.Order.NPOrderID),
						user = "NPG Auto",
					)
					NewMessage.save()
					# User is not set
					LogInput("INFO:  PortINProcess started  (Activating Order) ....",self.MainNPOrderID,"OSB") 
					Respond = soap.handle_out_message.HandleOutgoingMessage(NewMessage, None)
				
				#if contract is Deactive
				elif ContractInfo["ContractStatus"] == "4":
					LogInput("INFO:  PortINProcess started  (contract is Deactivated) ....",self.MainNPOrderID,"OSB") 
					# 151 is for the customer to activate 
					self.Order.status = 151
			# if contract check fails
			except Exception as e:
				LogInput("INFO:  PortINProcess started  (Failed to check contract) ....\n" + str(traceback.format_exc()),self.MainNPOrderID,"OSB") 
				self.Order.status = 151

##############################################################################
# Port out process, for each number
##############################################################################
	def PortInExecute(self,):
		for self.PhoneNumber in self.AllPhoneNumbers:
			self.BSCSMessage = {
				"PhoneNumber" : self.PhoneNumber,
				"NPOrderID" : self.MainNPOrderID,
			}
			ContractInfo = GetContractInfo(self.BSCSMessage)
			#print(self.BSCSMessage)
			#print(ContractInfo)
			LogInput("contract info: " + str(ContractInfo),self.MainNPOrderID,"OSB")
			# Check the responce if not 500
			# - 1 - 	The MSISDN is not a valid number on the Donor's network
			if ContractInfo["ResponseStatus"] == "500":
				LogInput("ERROR:    Can't get contract info......",self.MainNPOrderID,"OSB")			
			elif ContractInfo["ResponseStatus"] == "200":
				# verify the contract status, only if active = 2
				# - 4 - 	The MSISDN is already subject to suspension for outgoing and incoming calls, i.e. not active
				if ContractInfo["ContractStatus"] == 3:
					self.BSCSMessage["ContractID"] = ContractInfo["ContractID"]
					self.BSCSMessage["ContractStatus"] = 2
					self.BSCSMessage["Reason"] = 4
					#print(self.BSCSMessage)
					try:
						#print("inside try")
						ContractUpdate(self.BSCSMessage)
						LogInput("INFO:  Contract Updated ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
					except:
						#print("ERROR 95:")
						LogInput("ERROR 95:   Contract was not Updated ...." + self.PhoneNumber + "  \n" + str(traceback.format_exc()),self.MainNPOrderID,"OSB")

		
##############################################################################
# Port out process, for each number
##############################################################################
	def EmergencyRestoreExecute(self,):
		pass

##############################################################################
# Port Out Completion of the process, when we received NP activate
##############################################################################
	def IncomingNPActivate(self,):
		# RAS Integration 
		from int_ras.services import PortOutDeactivation
		for self.PhoneNumber in self.AllPhoneNumbers:
			LogInput("INFO:  Deactivating Port Out number: " + self.PhoneNumber,self.MainNPOrderID,"OSB")
			try:
				self.BSCSMessage = {
					"PhoneNumber" : self.PhoneNumber,
					"NPOrderID" : self.MainNPOrderID,
					"PhoneNumberNoCode" : self.PhoneNumber.split('965',1)[1],
				}
				
				# B2B deactivation, disalbed till ferther confirmation from Uzir, 
				'''
				if self.Order.CustomerType == 'business':
					try:
						LogInput("INFO:  Deactivating business Port Out ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
						self.B2BPortOutAccount()
					except:
						LogInput("ERROR:   Deactivating business Port Out has failed for .... " + self.PhoneNumber,self.MainNPOrderID,"OSB")
						LogInput("ERROR:   " + str(traceback.format_exc()),self.MainNPOrderID,"OSB")
				'''
				result = PortOutDeactivation(self.BSCSMessage)
				LogInput("The Result for %s of PortOutDeactivation: %s" % (self.BSCSMessage["PhoneNumberNoCode"], str(result)), str(self.MainNPOrderID),"process")
				tries = 0
				while result.get("ResponseStatus") == '500' and tries != 5:
					tries = tries + 1
					LogInput("ResponseStatus > " + str(result.get("ResponseStatus")))
					if result.get("ResponseStatus") == "200":
						LogInput("INFO:  RAS  PortOutDeactivation try # -%s- for %s  SUCESS > %s" % (str(tries), str(self.BSCSMessage["PhoneNumber"]), str(result)),str(self.MainNPOrderID),"process")
					else:
						LogInput("ERROR:  RAS  PortOutDeactivation try # -%s- for %s  ERROR > %s" % (str(tries), str(self.BSCSMessage["PhoneNumber"]), str(result)),str(self.MainNPOrderID),"process")
					# wait 5 seconds
					result = PortOutDeactivation(self.BSCSMessage)
					sleep(5)
	
				
				# get the contract information
				ContractInfo = GetContractInfo(self.BSCSMessage)
				
				# update contract status to 4  REMOVED FOR RAS
				#ContractUpdateStatus = self.ContractUpdateDef(ContractInfo)
				x = 0
				while (ContractInfo["ContractStatus"] != 4) and x <= 240:
					#print("current contract stats = " + str(ContractInfo["ContractStatus"]))
					#print("the contract: " + str(ContractInfo))
					ContractInfo = GetContractInfo(self.BSCSMessage)
					sleep(10)
					x = x + 1
				
				try:
					from alert import EmailAlert
					
					if x == 241:
						EmailAlert(AlertType = "PortOut",Message=self.BSCSMessage)
				except:
					LogInput("ERROR:   Failed to send alert ....\n" + str(traceback.format_exc()),"alerts","portout") 
	
				
				print("tried: " + str(x))
				
				try:
					LogInput("INFO:  PortOutDeactivation for %s tried %s" % (str(x), str(self.BSCSMessage["PhoneNumber"])),str(self.MainNPOrderID),"process")
				except:
					pass
				
				PhoneNumberExportInfo = self.PhoneNumberExportDef()
				#print(PhoneNumberExportInfo)
				# Change Porting Request status to "a" and reason to 55
				#print(self.PortingRequestWriteDef(PhoneNumberExportInfo,NewReason=55,NewStatus="a"))
				self.PortingRequestWriteDef(PhoneNumberExportInfo,NewReason=55,NewStatus="a")
				
				# Find the chnaged Request
				PortingRequestInfo = self.PortingRequestSearchDef()
				#PortingRequestInfo["Status"] = "xxxxxx"
				#print(str(PortingRequestInfo))
				# keep looking for the changed request, don't move forwared till request is found
				y = 0
				try:
					while(PortingRequestInfo["Status"] != "a") and y <= 120:
						#print("not a yet")
						PortingRequestInfo = self.PortingRequestSearchDef()
						sleep(10)
						y = y + 1
				except:
					LogInput("ERROR:   Tried: %s, Failed to PortingRequestWrite > NewReason=55 > NewStatus=a...  message is %s ....\n %s" % (str(y), str(PortingRequestInfo) ,str(traceback.format_exc())),self.MainNPOrderID,"OSB") 
	
				try:
					LogInput("INFO:  PortingRequestWrite for %s tried %s" % (str(y), str(self.BSCSMessage["PhoneNumber"])),str(self.MainNPOrderID),"process")
				except:
					pass
				# Change Porting Request status to "s" and reason to 64
				self.PortingRequestWriteDef(PhoneNumberExportInfo,NewReason=64,NewStatus="s")
			except:
				LogInput("ERROR:   Failed to deactivate number: " + self.PhoneNumber + "    " + str(traceback.format_exc()),self.MainNPOrderID,"OSB") 
		# send NP Deactivate to CRDB after all the numbers has been deactivated internally	
		self.SendNPDeactivatedToNPS()

##############################################################################
# Port Out Completion for B2B account added on 14th Auguest 2013
##############################################################################
	def B2BPortOutAccount(self,):
		self.BSCSMessage["CustomerID"] = GetContractInfo(self.BSCSMessage)["CustomerID"]
		LogInput("INFO:  B2B Port Out Deactivation ....",self.MainNPOrderID,"OSB")

		self.BSCSMessage["PaymentID"] = PaymentArrangementWrite(self.BSCSMessage)["PaymentID"]
		LogInput("INFO:  B2B Port Out Deactivation - Step 1 complete....",self.MainNPOrderID,"OSB")

		self.BSCSMessage["CustomerIDHigh"] = GetCustomerBalance(self.BSCSMessage)["CustomerIDHigh"]
		LogInput("INFO:  B2B Port Out Deactivation - Step 2 complete....",self.MainNPOrderID,"OSB")

		CustomerHierarchyWrite(self.BSCSMessage)
		LogInput("INFO:  B2B Port Out Deactivation - Step 3 complete....",self.MainNPOrderID,"OSB")

		CustomerFlatWrite(self.BSCSMessage)
		LogInput("INFO:  B2B Port Out Deactivation - Step 4 complete....",self.MainNPOrderID,"OSB")


##############################################################################
# Port out process, for each number for consumer
##############################################################################
#Code	Description  -- NPS error codes
#1	The MSISDN is not a valid number on the Donor's network
#2	The MSISDN and the identifier  do not match (for subscriber)
#3	The MSISDN and the Corporate Registration Number do not match (for Corporate)
#4	The MSISDN is already subject to suspension for outgoing and incoming calls, i.e. not active
#5	MNP Bill Payment not made
#6	Any other reason agreed to by NTEC and notified to the operators in writing
#####################
	def PortOutProcess(self,):
		# DO THE processs for every single number 
		self.VerficationStatus = 0
		# prepare the donor reject message
		self.NewNPDonorReject = NpMessages(
			MessageTypeID = GetRequest('NPDonorReject'),
			NPOrderID = str(self.Order.NPOrderID),
			user = "NPG Auto",
		)
		self.NewNPDonorReject.save(commit=False)
		
		LogInput("INFO:  Check if customer type ....",self.MainNPOrderID,"OSB")
		if self.Order.CustomerType == 'consumer':
			LogInput("INFO:  Consumer customer ....",self.MainNPOrderID,"OSB")
			for self.PhoneNumber in self.AllPhoneNumbers:
				self.BSCSMessage = {
					"PhoneNumber" : self.PhoneNumber,
					"NPOrderID" : self.MainNPOrderID,
					"PhoneNumberNoCode" : self.PhoneNumber.split('965',1)[1],
				}

				LogInput("INFO:  Checking phone number ....  " + self.PhoneNumber,self.MainNPOrderID,"OSB")
				try:
					# get all contract information
					LogInput("INFO:  Get contract info ....  " + self.PhoneNumber,self.MainNPOrderID,"OSB")
					ContractInfo = GetContractInfo(self.BSCSMessage)


					LogInput("INFO:  contract info: " + str(self.PhoneNumber),self.MainNPOrderID,"OSB")
					# Check the responce if not 500
					# - 1 - 	The MSISDN is not a valid number on the Donor's network

					LogInput("INFO:  Checking contract validity: " + str(self.PhoneNumber),self.MainNPOrderID,"OSB")
					if self.VerfiyContractExist(ContractInfo["ResponseStatus"]):

						self.BSCSMessage["ContractStatus"] = ContractInfo["ContractStatus"]
						LogInput("INFO:  Checking Contract status: NEW >>" + str(self.BSCSMessage["ContractStatus"]),self.MainNPOrderID,"OSB")
						if self.VerfiyContractStatus(self.BSCSMessage["ContractStatus"]):
						
							try:
								# Get cusgtomer information from OSB
								LogInput("INFO:  Getting customer info with phone number " + str(self.PhoneNumber),self.MainNPOrderID,"OSB")

								self.BSCSMessage["CustomerID"] = ContractInfo["CustomerID"]
								CustomerInfo = GetCustomerInfo(self.BSCSMessage)
								LogInput("INFO:  Got customer info starting verification for number " + str(self.PhoneNumber),self.MainNPOrderID,"OSB")
								LogInput("INFO:  Verifying customer id and id type ",self.MainNPOrderID,"OSB")

								if self.VerfiyCustomerIDInformation(CustomerInfo):
									LogInput("INFO:  Get customer Balance ",self.MainNPOrderID,"OSB")
									
									# try to get customer balance 
									try:
										CustomerBalance = GetCustomerBalance(self.BSCSMessage)
										LogInput("INFO:  customer Balance information fetched > " + str(self.PhoneNumber) ,self.MainNPOrderID,"OSB")
										try:
											CustomerBalanceALL = GetNewCheckBalance(self.BSCSMessage)
											LogInput("INFO:  NEW SYSTEM - customer Balance information fetched > " + str(CustomerBalanceALL["CurrentBalance"]),self.MainNPOrderID,"OSB")
											LogInput("INFO:  Verifying Consumer Payment Responsible",self.MainNPOrderID,"OSB")
											# verfiy payment responsible
											if self.VerfiyConsumerPaymentResponsible(ContractInfo,CustomerBalance):
												LogInput("INFO:  Consumer Payment Responsible verified",self.MainNPOrderID,"OSB")
												#verify balance
												if self.VerfiyBalance(CustomerBalanceALL["CurrentBalance"], 5.0):
													# all verfication succeeded
													if self.VerficationStatus == 500:
														pass
													else:
														self.VerficationStatus = 200
														LogInput("INFO:  The number >  " + self.PhoneNumber + " is fully verify OK...",self.MainNPOrderID,"OSB")
										except:
											NewMessage = DonorRejectList(
												np_messages_id = self.NewNPDonorReject.pk,
												DonorRejectReason = 1,
												DonorRejectMessage = "Customer doesn't exist",
												PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
												PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
											)
											NewMessage.save()
											self.VerficationStatus = 500
											LogInput("ERROR 17:  unable to get customer CustomerBalanceALL from new system\n" + str(traceback.format_exc()),self.MainNPOrderID,"OSB")
									except Exception as e:
										NewMessage = DonorRejectList(
											np_messages_id = self.NewNPDonorReject.pk,
											DonorRejectReason = 1,
											DonorRejectMessage = "Customer doesn't exist",
											PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
											PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
										)
										NewMessage.save()
										self.VerficationStatus = 500
										LogInput("ERROR 16:  unable to get customer information\n" + str(traceback.format_exc()),self.MainNPOrderID,"OSB")

							except Exception as e:
								NewMessage = DonorRejectList(
									np_messages_id = self.NewNPDonorReject.pk,
									DonorRejectReason = 1,
									DonorRejectMessage = "Customer doesn't exist",
									PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
									PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
								)
								NewMessage.save()
								self.VerficationStatus = 500
								LogInput("Error 6: > \n" + str(traceback.format_exc()),self.MainNPOrderID,"OSB")
								LogInput("ERROR 7:  Failed trying to get the customer information ",self.MainNPOrderID,"OSB")
				# There was error comunicating with OSB to get the contract information					
				except Exception as e:
					# Check the responce if not 500 THIS can be if connection FAILD VERY IMPORTANT
					NewMessage = DonorRejectList(
						np_messages_id = self.NewNPDonorReject.pk,
						DonorRejectReason = 1,
						DonorRejectMessage = "Contract doesn't exist or the MSISDN doesn't exist",
						PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
						PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
					)
					NewMessage.save()
					self.VerficationStatus = 500
					LogInput("Error 8:  > \n" + str(traceback.format_exc()),self.MainNPOrderID,"OSB")
					LogInput("Verification Error 9:  Contract doesn't exist or the MISMSIDN",self.MainNPOrderID,"OSB")
					LogInput("ERROR 10:  Failed trying to get the contract information",self.MainNPOrderID,"OSB")

		else:
			TotalBalanceOfALlLines = 0
			LogInput("INFO:  Business process ....",self.MainNPOrderID,"OSB")
			for self.PhoneNumber in self.AllPhoneNumbers:
				self.BSCSMessage = {
					"PhoneNumber" : self.PhoneNumber,
					"NPOrderID" : self.MainNPOrderID,
					"PhoneNumberNoCode" : self.PhoneNumber.split('965',1)[1],
				}
				LogInput("INFO:  Checking phone number ....  " + self.PhoneNumber,self.MainNPOrderID,"OSB")
				try:
					# get all contract information
					LogInput("INFO:  getting contract info ....  " + self.PhoneNumber,self.MainNPOrderID,"OSB")
					ContractInfo = GetContractInfo(self.BSCSMessage)
					self.BSCSMessage["ContractStatus"] = ContractInfo["ContractStatus"]

					LogInput("INFO:  contract info: " + str(self.PhoneNumber),self.MainNPOrderID,"OSB")
					# Check the responce if not 500
					# - 1 - 	The MSISDN is not a valid number on the Donor's network
					LogInput("INFO:  Checking contract valid: " + str(self.PhoneNumber),self.MainNPOrderID,"OSB")
					if self.VerfiyContractExist(ContractInfo["ResponseStatus"]):
						LogInput("INFO:  Checking Contract status: >>" + str(self.BSCSMessage["ContractStatus"]),self.MainNPOrderID,"OSB")
						if self.VerfiyContractStatus(self.BSCSMessage["ContractStatus"]):
							try:
								# Get cusgtomer information from OSB
								LogInput("INFO:  Getting customer info with phone number " + str(self.PhoneNumber),self.MainNPOrderID,"OSB")
								self.BSCSMessage["CustomerID"] = ContractInfo["CustomerID"]
								CustomerInfo = GetCustomerInfo(self.BSCSMessage)
								LogInput("INFO:  Got customer info starting verification for number " + str(self.PhoneNumber),self.MainNPOrderID,"OSB")
								LogInput("INFO:  Verifying customer id and id type ",self.MainNPOrderID,"OSB")



								LogInput("INFO:  Get Company Registration ",self.MainNPOrderID,"OSB")

								# we get the first flot from the consumer code of the contract 
								self.BSCSMessage["CustomerCode"] = ContractInfo["ConsumerCode"].split('.',2)[0] + '.' + ContractInfo["ConsumerCode"].split('.',2)[1]
								# we get the company top level contract
								LogInput("ContractInfoSplit > " + str(self.BSCSMessage["CustomerCode"]) ,self.MainNPOrderID,"OSB")
								
								CompanyTopLevel = GetCompanyTopLevel(self.BSCSMessage)
								# we get the cr from the top level contract
								LogInput("CompanyTopLevel > " + str(CompanyTopLevel) ,self.MainNPOrderID,"OSB")
								self.BSCSMessage["CustomerID"] = CompanyTopLevel["CustomerID"]
								CompanyRegistration = GetCompanyRegistration(self.BSCSMessage)
								#LogInput("INFO:  Company Registration from BSCS > " + str(CompanyRegistration["CompanyRegistration"]))
								#LogInput("INFO:  Company Registration from Message > " + str(CompanyRegistration["CompanyRegistration"]))
								# send the CR and contract info to verfiy the CR
								LogInput("CompanyRegistration > " + str(CompanyRegistration) ,self.MainNPOrderID,"OSB")
								if self.VerfiyCompanyRegistration(CustomerInfo,CompanyRegistration,ContractInfo):
									
									LogInput("INFO:  Get customer Balance ",self.MainNPOrderID,"OSB")
									
									# try to get customer balance 
									try:
										CustomerBalance = GetCustomerBalance(self.BSCSMessage)
										LogInput("INFO:  customer Balance information fetched > " + str(self.PhoneNumber) ,self.MainNPOrderID,"OSB")
										try:
											CustomerBalanceALL = GetNewCheckBalance(self.BSCSMessage)
											LogInput("INFO:  NEW SYSTEM - customer Balance information fetched > " + str(CustomerBalanceALL["CurrentBalance"]),self.MainNPOrderID,"OSB")
											#verify balance
											TotalBalanceOfALlLines = float(CustomerBalanceALL["CurrentBalance"]) + float(TotalBalanceOfALlLines)
											if self.VerfiyBalanceB2B(TotalBalanceOfALlLines, 30.0):
												# all verfication succeeded
												if self.VerficationStatus == 500:
													pass
												else:
													self.VerficationStatus = 200
													LogInput("INFO:  The number >  " + self.PhoneNumber + " is fully verify OK...",self.MainNPOrderID,"OSB")
										except:
											NewMessage = DonorRejectList(
												np_messages_id = self.NewNPDonorReject.pk,
												DonorRejectReason = 1,
												DonorRejectMessage = "Customer doesn't exist",
												PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
												PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
											)
											NewMessage.save()
											self.VerficationStatus = 500
											LogInput("ERROR 17:  unable to get customer CustomerBalanceALL from new system\n" + str(traceback.format_exc()),self.MainNPOrderID,"OSB")
									except Exception as e:
										NewMessage = DonorRejectList(
											np_messages_id = self.NewNPDonorReject.pk,
											DonorRejectReason = 1,
											DonorRejectMessage = "Customer doesn't exist",
											PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
											PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
										)
										NewMessage.save()
										self.VerficationStatus = 500
										LogInput("ERROR 16:  unable to get customer information\n" + str(traceback.format_exc()),self.MainNPOrderID,"OSB")

							except Exception as e:
								NewMessage = DonorRejectList(
									np_messages_id = self.NewNPDonorReject.pk,
									DonorRejectReason = 1,
									DonorRejectMessage = "Customer doesn't exist",
									PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
									PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
								)
								NewMessage.save()
								self.VerficationStatus = 500
								LogInput("Error 6: > \n" + str(traceback.format_exc()),self.MainNPOrderID,"OSB")
								LogInput("ERROR 7:  Failed trying to get the customer information ",self.MainNPOrderID,"OSB")
				# There was error comunicating with OSB to get the contract information					
				except Exception as e:
					# Check the responce if not 500 THIS can be if connection FAILD VERY IMPORTANT
					NewMessage = DonorRejectList(
						np_messages_id = self.NewNPDonorReject.pk,
						DonorRejectReason = 1,
						DonorRejectMessage = "Contract doesn't exist or the MSISDN doesn't exist",
						PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
						PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
					)
					NewMessage.save()
					LogInput("Error 8:  > \n" + str(traceback.format_exc()),self.MainNPOrderID,"OSB")
					LogInput("Verification Error 9:  Contract doesn't exist or the MISMSIDN",self.MainNPOrderID,"OSB")
					self.VerficationStatus = 500

					LogInput("ERROR 10:  Failed trying to get the contract information",self.MainNPOrderID,"OSB")

		# Send accept or reject
		self.SendAutoResponseToNPCS()

	def SendAutoResponseToNPCS(self,):
		from soap.handle_out_message import HandleOutgoingMessage
		#send out NPDonorReject if one number fails to verfiy
		if self.VerficationStatus == 500:
			self.NewNPDonorReject.save()
			# User is not set
			LogInput("INFO:  PortINProcess verification failed sending NPDonorReject ....",self.MainNPOrderID,"OSB") 
			Respond = soap.handle_out_message.HandleOutgoingMessage(self.NewNPDonorReject, None)
			LogInput("INFO:  Respond of NPDonorReject ...." + str(Respond),self.MainNPOrderID,"OSB") 
		elif self.VerficationStatus == 200:
			NewMessage = NpMessages(
				MessageTypeID = GetRequest('NPDonorAccept'),
				NPOrderID = str(self.Order.NPOrderID),
				user = "NPG Auto",
			)
			#same the accpet message 
			NewMessage.save()
			# delete the reject message
			self.NewNPDonorReject.delete()
			# User is not set
			#LogInput("Sending NPDonorAccept > ....",self.MainNPOrderID,"OSB") 
			Respond = soap.handle_out_message.HandleOutgoingMessage(NewMessage, None)
			#LogInput("INFO:  Respond of NPDonorAccept ...." + str(Respond),self.MainNPOrderID,"OSB") 

	def VerfiyContractExist(self,ResponseStatus):
		if ResponseStatus == "500":
			NewMessage = DonorRejectList(
				np_messages_id = self.NewNPDonorReject.pk,
				DonorRejectReason = 1,
				DonorRejectMessage = "Mobile number doesn't exist",
				PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
				PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
			)
			NewMessage.save()
			LogInput("Verification Error 1:  Contract doesn't exist or the MISMSIDN" + str(self.PhoneNumber),self.MainNPOrderID,"OSB")
			self.VerficationStatus = 500
			return False
						
		elif ResponseStatus == "200":
			# verify the contract status, only if active = 2
			# - 4 - 	The MSISDN is already subject to suspension for outgoing and incoming calls, i.e. not active
			LogInput("INFO:  Contract info exist: " + str(self.PhoneNumber),self.MainNPOrderID,"OSB")
			
			return True

	def VerfiyContractStatus(self,ContractStatus):
		if ContractStatus != 2:
			NewMessage = DonorRejectList(
				np_messages_id = self.NewNPDonorReject.pk,
				DonorRejectReason = 4,
				DonorRejectMessage = "The number is suspended",
				PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
				PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
			)
			NewMessage.save()
			LogInput("Verification Error 2:  Contract is not active > " + str(ContractStatus),self.MainNPOrderID,"OSB")
			self.VerficationStatus = 500
			return False
		return True
	
	'''
	# User this line to call the function >> self.VerfiyContractPlan(ContractInfo["Plan"])
	def VerfiyContractPlan(self,ContractPlan):
		LogInput("INFO:  contract plan from OSB:  " + str(ContractPlan),self.MainNPOrderID,"OSB")
		LogInput("INFO:  contract plan from NPS:  " + self.InMessage.SubscriptionType,self.MainNPOrderID,"OSB")
		if ContractPlan == 17 and self.InMessage.SubscriptionType == "postpaid":
			NewMessage = DonorRejectList(
				np_messages_id = self.NewNPDonorReject.pk,
				DonorRejectReason = 1,
				DonorRejectMessage = "The request doesn't have the correct service plan (Prepaid/Postpaid)",
				PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
				PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
			)
			NewMessage.save()
			LogInput("Verification Error 3:  the request doesn't have the correct plan" + str(self.PhoneNumber),self.MainNPOrderID,"OSB")
			self.VerficationStatus = 500
			return False
		return True
	'''

	def VerfiyCompanyRegistration(self,CustomerInfo,CompanyRegistration,ContractInfo):
		# verfiy the CR information ######################### Business id verfication 
		# CR check
		LogInput("INFO:  ID Type from OSB:  " + str(CustomerInfo["IDType"]),self.MainNPOrderID,"OSB")
		LogInput("INFO:  ID Type from NPS:  " + str(self.InMessage.CustomerIDType),self.MainNPOrderID,"OSB")
		LogInput("INFO:  Customer ID from OSB:  " + str(CustomerInfo["Passport"]),self.MainNPOrderID,"OSB")
		LogInput("INFO:  Customer ID from NPS:  " + str(self.InMessage.CustomerID),self.MainNPOrderID,"OSB")
		LogInput("INFO:  CR from OSB:  " + str(CompanyRegistration["CompanyRegistration"]),self.MainNPOrderID,"OSB")

		if (str(ContractInfo["ConsumerCode"])[0] != "1" and self.InMessage.CustomerIDType == 3 and str(CompanyRegistration["CompanyRegistration"]) == str(self.InMessage.CustomerID)):
			LogInput("INFO:  Company CR verified",self.MainNPOrderID,"OSB")
			
			return True

		# CR information is incorrect
		# -  2  -	The MSISDN and the identifier  do not match (for subscriber)
		LogInput("INFO:  Company CR doesn't match",self.MainNPOrderID,"OSB")
		NewMessage = DonorRejectList(
			np_messages_id = self.NewNPDonorReject.pk,
			DonorRejectReason = 2,
			DonorRejectMessage = "ID/CR mismatch",
			PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
			PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
		)
		NewMessage.save()
		LogInput("Verification Error 5:  ID mismatch",self.MainNPOrderID,"OSB")
		self.VerficationStatus = 500
		return False

	def VerfiyCustomerIDInformation(self,CustomerInfo):
		# verfiy the passport information 
		# IDType 1 = Passport
		# IDType 2 = ID Card - civil id
		# adrIdno    is the passport #
		LogInput("INFO:  CustomerIDType from OSB:  " + str(CustomerInfo["IDType"]),self.MainNPOrderID,"OSB")
		LogInput("INFO:  CustomerIDType from NPS:  " + str(self.InMessage.CustomerIDType),self.MainNPOrderID,"OSB")
		LogInput("INFO:  CustomerID from OSB:  " + str(CustomerInfo["Passport"]),self.MainNPOrderID,"OSB")
		LogInput("INFO:  CustomerID from NPS:  " + str(self.InMessage.CustomerID),self.MainNPOrderID,"OSB")
		LogInput("INFO:  CivilID from OSB:  " + str(CustomerInfo["CivilID"]),self.MainNPOrderID,"OSB")
		
		if (CustomerInfo["IDType"] == 1 and self.InMessage.CustomerIDType == 1 and str(CustomerInfo["Passport"]).lower() == str(self.InMessage.CustomerID).lower()) or (int(self.InMessage.CustomerIDType) == 2 and str(CustomerInfo["CivilID"]) == str(self.InMessage.CustomerID)):
			LogInput("INFO:  customer id and id type verified",self.MainNPOrderID,"OSB")
			return True
		# Passport information is incorrect
		# -  2  -	The MSISDN and the identifier  do not match (for subscriber)
		LogInput("INFO:  customer id and id doesn't match",self.MainNPOrderID,"OSB")
		NewMessage = DonorRejectList(
			np_messages_id = self.NewNPDonorReject.pk,
			DonorRejectReason = 2,
			DonorRejectMessage = "ID mismatch",
			PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
			PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
		)
		NewMessage.save()
		LogInput("Verification Error 5:  Passport information is incorrect",self.MainNPOrderID,"OSB")
		self.VerficationStatus = 500
		return False

	def VerfiyConsumerPaymentResponsible(self,ContractInfo,CustomerBalance):
		LogInput("INFO:  Checking if account is business",self.MainNPOrderID,"OSB")
		LogInput("INFO:  The split of ConsumerCode > " + str(ContractInfo["ConsumerCode"][:0]),self.MainNPOrderID,"OSB")
		LogInput("INFO:  PaymentResponsible is :  " + str(CustomerBalance["PaymentResponsible"]),self.MainNPOrderID,"OSB")
		LogInput("INFO:  verifying if PaymentResponsible > :  " + str(CustomerBalance["PaymentResponsible"]),self.MainNPOrderID,"OSB")
		LogInput("ContractInfo['ConsumerCode'][0] > " + ContractInfo["ConsumerCode"][0],self.MainNPOrderID,"OSB")
		LogInput("CustomerBalance['PaymentResponsible'] > " + str(CustomerBalance["PaymentResponsible"]),self.MainNPOrderID,"OSB")
		if str(ContractInfo["ConsumerCode"])[0] == "1" or (str(ContractInfo["ConsumerCode"])[0] != "1" and CustomerBalance["PaymentResponsible"] == 1):
			LogInput("INFO:  PaymentResponsible verified > :  " + str(CustomerBalance["PaymentResponsible"]),self.MainNPOrderID,"OSB")
			# Check the balance if it is less then 5 kd processd
			return True
		else:
			LogInput("INFO:  verifying if PaymentResponsible FAILD  " + str(CustomerBalance),self.MainNPOrderID,"OSB")
			NewMessage = DonorRejectList(
				np_messages_id = self.NewNPDonorReject.pk,
				DonorRejectReason = 2,
				DonorRejectMessage = "The MISIDN belongs to a business account",
				PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
				PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
			)
			NewMessage.save()
			LogInput("Verification Error 6:  Business account porting with consumer request",self.MainNPOrderID,"OSB")
			self.VerficationStatus = 500
			return False

	def VerfiyBalance(self,CustomerBalance, Amount):
		LogInput("INFO:  Verifying Customer Balance : =  " + str(CustomerBalance),self.MainNPOrderID,"OSB")
		if float(CustomerBalance) < float(Amount):
			LogInput("Customer Balance verified ... ok",self.MainNPOrderID,"OSB")
			return True
				
		LogInput("INFO :  Consumer balance is over %s KD > %s" % (Amount,CustomerBalance),self.MainNPOrderID,"OSB")
		NewMessage = DonorRejectList(
			np_messages_id = self.NewNPDonorReject.pk,
			DonorRejectReason = 5,
			DonorRejectMessage = "Payment not made",
			PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
			PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
		)
		NewMessage.save()
		LogInput("Verification Error 7:  There is unpaid amount",self.MainNPOrderID,"OSB")
		self.VerficationStatus = 500
		return False

	def VerfiyBalanceB2B(self,CustomerBalance, Amount):
		LogInput("INFO:  Verifying Customer Balance : =  " + str(CustomerBalance),self.MainNPOrderID,"OSB")
		if float(CustomerBalance) < float(Amount):
			LogInput("Customer Balance verified ... ok",self.MainNPOrderID,"OSB")
			return True
				
		LogInput("INFO :  Consumer balance is over %s KD > %s" % (Amount,CustomerBalance),self.MainNPOrderID,"OSB")
		NewMessage = DonorRejectList(
			np_messages_id = self.NewNPDonorReject.pk,
			DonorRejectReason = 5,
			DonorRejectMessage = "The total outstanding of the ported number/s is over 30 KD",
			PhoneNumberStart = self.PhoneNumber.split('965',1)[1],
			PhoneNumberEnd = self.PhoneNumber.split('965',1)[1],
		)
		NewMessage.save()
		LogInput("Verification Error 7:  There is unpaid amount",self.MainNPOrderID,"OSB")
		self.VerficationStatus = 500
		return False


	def GetContractInfoDef(self,):
		return GetContractInfo(self.BSCSMessage)


	def ContractUpdateDef(self,InContractInfo):
		self.BSCSMessage["ContractID"] = InContractInfo["ContractID"]
		self.BSCSMessage["ContractStatus"] = 4
		self.BSCSMessage["Reason"] = 69
		return ContractUpdate(self.BSCSMessage)


	def PhoneNumberExportDef(self,):
		self.BSCSMessage["npcode"] = 1
		self.BSCSMessage["destPlcodePub"] = "MNPE"
		self.BSCSMessage["errorCodeInd"] = "T"
		self.BSCSMessage["Reason"] = 61
		return PhoneNumberExport(self.BSCSMessage)

	def PortingRequestWriteDef(self,PhoneNumberExportInfo,NewReason,NewStatus):
		self.BSCSMessage["PortingRequestID"] = PhoneNumberExportInfo["PortingRequestID"]
		self.BSCSMessage["Reason"] = NewReason
		self.BSCSMessage["Status"] = NewStatus
		return PortingRequestWrite(self.BSCSMessage)


	def PortingRequestSearchDef(self,):
		self.BSCSMessage["status"] = "a"
		return PortingRequestSearch(self.BSCSMessage)

	def SendNPDeactivatedToNPS(self,):
		from soap.handle_out_message import HandleOutgoingMessage
		NewMessage = NpMessages(
			MessageTypeID = GetRequest('NPDeactivated'),
			NPOrderID = str(self.Order.NPOrderID),
			user = "NPG Auto",
		)
		NewMessage.save()
		Respond = HandleOutgoingMessage(NewMessage, None)

	def RingFence(self,):
		for self.PhoneNumber in self.AllPhoneNumbers:
			self.BSCSMessage = {
				"PhoneNumber" : self.PhoneNumber,
				"PhoneNumberNoCode" : self.PhoneNumber.split('965',1)[1],
				"NPOrderID" : self.MainNPOrderID,
				"RingFence" : "RING FENCE",
				"RingFenceRASAlert" : "Customer on Ring Fence due to Number Portability",
			}
			self.BSCSMessage["ContractID"] = GetContractInfo(self.BSCSMessage)["ContractID"]

			LogInput("INFO:  Ring Fence Received, updating BSCS ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
			try:
				SetRingFanceFlag(self.BSCSMessage)
			except:
				LogInput("ERROR   RING FENCE > GetCustomerInfoWrite:   failed to check GetCustomerInfoWrite info ...." + self.PhoneNumber + " " + str(traceback.format_exc()),self.MainNPOrderID,"OSB")

			LogInput("INFO:  Ring Fence Received, updating RAS ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
			try:
				LogInput("INFO:  Ring Fence RAS sending .... " + self.PhoneNumber,self.MainNPOrderID,"OSB")
				result = RingFenceRASService(self.BSCSMessage)
				LogInput("INFO:  Ring Fence RAS result .... " + str(result) + " " + self.PhoneNumber,self.MainNPOrderID,"OSB")
			except:
				LogInput("ERROR   RING FENCE > RingFenceRASService:   failed to check GetCustomerInfoWrite info ...." + self.PhoneNumber + " " + str(traceback.format_exc()),self.MainNPOrderID,"RAS")
	
	def RemoveRingFence(self,):
		for self.PhoneNumber in self.AllPhoneNumbers:
			self.BSCSMessage = {
				"PhoneNumber" : self.PhoneNumber,
				"PhoneNumberNoCode" : self.PhoneNumber.split('965',1)[1],
				"NPOrderID" : self.MainNPOrderID,
				"RingFence" : "",
				"RingFenceRASAlert" : "",
			}
			self.BSCSMessage["ContractID"] = GetContractInfo(self.BSCSMessage)["ContractID"]

			LogInput("INFO:  Ring Fence Received, removing from BSCS ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
			try:
				SetRingFanceFlag(self.BSCSMessage)
			except Exception as e:
				LogInput("ERROR   RING FENCE > GetCustomerInfoWrite:   failed to check GetCustomerInfoWrite info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")

			LogInput("INFO:  Ring Fence Received, removing from RAS ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
			try:
				RingFenceRASService(self.BSCSMessage)
			except Exception as e:
				LogInput("ERROR   RING FENCE > RingFenceRASService:   failed to check GetCustomerInfoWrite info ...." + self.PhoneNumber,self.MainNPOrderID,"RAS")


	def NPReturn(self,):
		from int_ras.services import NPReturn as RAS_NPReturn
		for self.PhoneNumber in self.AllPhoneNumbers:
			if self.PhoneNumber.startswith("9656"):
				self.BSCSMessage = {
					"PhoneNumber" : self.PhoneNumber,
					"PhoneNumberNoCode" : self.PhoneNumber.split('965',1)[1],
					"NPOrderID" : self.MainNPOrderID,
				}
				LogInput("INFO:  NP Return action, adding with PhoneNumberReImport from BSCS .... " + self.PhoneNumber,self.MainNPOrderID,"OSB")
				# Return the number in BSCS
				try:
					result = PhoneNumberReImport(self.BSCSMessage)
					LogInput("INFO:  Returning number to BSCS .... " + str(result),self.MainNPOrderID,"OSB")
				except:
					LogInput("ERROR   NP Return > PhoneNumberReImport:   failed ...." + self.PhoneNumber + " --- "  + str(traceback.format_exc()),self.MainNPOrderID,"OSB")
				
				# Return the number in RAS
				try:
					result = RAS_NPReturn(self.BSCSMessage)
					LogInput("INFO:  Returning number to RAS .... " + str(result),self.MainNPOrderID,"OSB")
				except:
					LogInput("ERROR   NP Return > RAS_NPReturn:   failed ...." + self.PhoneNumber + " --- "  + str(traceback.format_exc()),self.MainNPOrderID,"OSB")
				
				# Send email alert that number has returned
				try:
					from alert import EmailAlert
					EmailAlert(AlertType = "NPReturn",Message=self.BSCSMessage)
				except:
					LogInput("ERROR:   Failed to send email alert ....\n" + str(traceback.format_exc()),"alerts","npreturn") 


	
	def EmergencyRestore(self,):
		pass
	'''
	def SendRejectMessage(self,):

		NewMessage = NpMessages(
			MessageTypeID = GetRequest('NPDonorReject'),
			NPOrderID = str(self.Order.NPOrderID),
			user = "NPG Auto",
		)
		NewMessage.save()
		# User is not set
		LogInput("PortINProcess started  (Activating Order) ....",self.MainNPOrderID,"OSB") 
		Respond = soap.handle_out_message.HandleOutgoingMessage(NewMessage, None)
	'''
	
	
	
'''
################################################################
CONTRACTS.SEARCH
try:
	GetContractInfo({"PhoneNumber" : self.PhoneNumber})
except Exception as e:
	LogInput("ERROR GetContractINFO:  failed to check GetContractInfo info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
################################################################
ADDRESS.READ
try:
	GetCustomerInfo({"CustomerID" : ContractInfo["CustomerID"]})
except Exception as e:
	LogInput("ERROR GetCustomerINFO:  failed to check GetCustomerInfo info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
################################################################
CUSTOMER.READ
try:
	CustomerBalance({"CustomerID" : ContractInfo["CustomerID"]})
except Exception as e:
	LogInput("ERROR CustomerBalance:   failed to check CustomerBalance info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
################################################################
CONTRACT.WRITE
try:
	Message = { 
							"ContractID" : "",
							"ContractStatus" : "",
							"Reason" : "",
						}
	ContractUpdate(Message):
except Exception as e:
	LogInput("ERROR ContractUpdate:   failed to check ContractUpdate info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")

To get contract ID :
GetContractInfo({"PhoneNumber" : self.PhoneNumber})["ContractID"]
################################################################
ENHANCED_REQUEST.READ
try:
	Message = { 
							"TransactionID" : "",
						}
	CheckStatusRequest(Message):
except Exception as e:
	LogInput("ERROR CheckStatusRequest:   failed to check CheckStatusRequest info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
################################################################
PHONE_NUMBER.EXPORT     update MSISDN
try:
	Message = { 
							"errorCodeInd" : "",
							"PhoneNumber" : "",
							"destPlcodePub" : "",
							"errorCodeInd" : "",
							"Reason" : "",
						}
	self.PhoneNumberExport(Message)
except Exception as e:
	LogInput("ERROR self.PhoneNumberExport:   failed to check self.PhoneNumberExport info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
################################################################
PORTING_REQUEST.SEARCH     Find porting request MSISDN
try:
	Message = { 
							"status" : "",
							"PhoneNumber" : "",
						}
	PortingRequestSearch(Message)
except Exception as e:
	LogInput("ERROR PortingRequestSearch:   failed to check PortingRequestSearch info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
################################################################
PORTING_REQUEST_REMARK.NEW
try:
	Message = { 
							"PortingRequestID" : "",
							"Description" : "",
						}
	PortingRequestRemarkNew(Message)
except Exception as e:
	LogInput("ERROR PortingRequestRemarkNew:   failed to check PortingRequestRemarkNew info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
################################################################
PORTING_REQUEST.WRITE
try:
	Message = { 
							"PortingRequestID" : "",
							"Reason" : "",
							"Status" : "",
						}
	PortingRequestWrite(Message)
except Exception as e:
	LogInput("ERROR PortingRequestWrite:   failed to check PortingRequestWrite info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
################################################################
PHONE_NUMBER.IMPORT
Message = { 
						"destPlcodePub" : "DTAG1",
						"hmcodePub" : "GSM 95",
						"localPrefix" : 965,
						"npcode" : 1,
						"srcPlcodePub" : "MNPE",
						"suffix" : "inInformation["PhoneNumber"].split('965',1)[1]",
					}
self.PhoneNumberRegister(Message)
except Exception as e:
	LogInput("ERROR self.PhoneNumberRegister:   failed to check self.PhoneNumberRegister info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
################################################################
CUSTOMER_INFO.READ
try:
	GetCustomerInfoRead({"CustomerID" : ContractInfo["CustomerID"]})
except Exception as e:
	LogInput("ERROR GetCustomerInfoRead:   failed to check GetCustomerInfoRead info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
################################################################
CUSTOMER_INFO.WRITE
try:
	Message = {
							"CustomerID" : ContractInfo["CustomerID"],
							"RingFence" : "RING FENCE",
						})
	GetCustomerInfoWrite(Message)
except Exception as e:
	LogInput("ERROR GetCustomerInfoWrite:   failed to check GetCustomerInfoWrite info ...." + self.PhoneNumber,self.MainNPOrderID,"OSB")
################################################################
'''

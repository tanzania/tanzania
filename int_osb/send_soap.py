from configuration import GetConfig, LogInput, GetRequest, LogMessageResutls, GET_MAILING_LIST
from suds import WebFault
from suds.client import Client
from suds.transport.http import HttpAuthenticated
from suds.transport.https import HttpTransport
from urllib2 import HTTPBasicAuthHandler, build_opener, install_opener, urlopen, BaseHandler
import base64
import traceback

## Importing WSSE
from suds.wsse import *

from SNMP_app.alerts import send_alert, send_clear

def SendSoap(service_name, url, Message):
	
	try:
		LogInput("INFO:     Setting client","SOAP","BSCS")
		
		client = SetSudsClient(url)

		#############################################################
		# Setting WSSE Security header
		#############################################################

		security=Security()
		token=UsernameToken("MNPUSER1","MNPUSER1")
		security.tokens.append(token)
		client.set_options(wsse=security)

		#############################################################
		# End of WSSE header setting
		#############################################################
		
		#LogInput("int_osb.send_soap >>   Destination Services has been queried --> %s" % client,"SOAP","BSCS")
		LogInput("INFO:     Destination URL: " + url ,"SOAP","BSCS")
		#LogInput("int_osb.send_soap >>   Sending soap Message to '%s' Destination " % service_name ,"SOAP","BSCS")

		#result = client.service.service_name(**Message)

		# client.factory.create("APIObject")
		
		LogInput("INFO:     Sent Message: " + str(Message),"SOAP","BSCS")
		Command = "client.service." + service_name + "(**Message)"
		LogInput( "INFO:     SUDS Command: " + str(Command),"SOAP","BSCS")	

		result = eval(Command)

		LogInput("INFO:     Sent message: " + str(client.last_sent()) ,"SOAP","BSCS")
		LogInput("INFO:     Received message: " + str(client.last_received()) ,"SOAP","BSCS")
		#LogInput("Results > \n" + str(result) ,"SOAP","BSCS")
		
		# EMAIL alert machenisem for any failer in sending soap messages
		try:
			if result[0] == 500:
				if GET_MAILING_LIST("SOAP Error")["enabled"]:
					from django.core.mail import send_mail
					subject = 'NPG Alert - SOAP ERROR BSCS '
					msg = "Sent message > \n" + str(client.last_sent()) + "\n\n\nReceived message > \n" + str(client.last_received())
					send_mail(subject, msg, GET_MAILING_LIST("SOAP Error")["from"], GET_MAILING_LIST("SOAP Error")["to"], fail_silently=True)
				else:
					LogInput("INFO:    Email alert disabled ","SOAP-ERROR","BSCS")

				LogInput("INFO:     Sent message: " + str(client.last_sent()) ,"SOAP-ERROR","BSCS")
				LogInput("INFO:     Received message: " + str(client.last_received()) ,"SOAP-ERROR","BSCS")
		except:
			pass

		""" 	
		########################################

		ContractsSearchBeanIn = client.factory.create("ContractsSearchBeanIn")
		ContractsSearchBeanIn.dirnum   = "96566080863"
		ContractsSearchBeanIn.searcher = "ContractSearchWithoutHistory" 

		########################################

		ValuesBeansIn = client.factory.create("impl:com.lhs.ws.beans.v3.session_change.ValuesBeansIn")
		ValuesBeansIn.key = "BU_ID"
		ValuesBeansIn.value = "1"

		SessionChangeBeanIn = client.factory.create("SessionChangeBeanIn")
		SessionChangeBeanIn.ValuesBeansIn = ValuesBeansIn
		
		#SessionChangeBeanIn.values =  { "key" : "BU_ID", "value" : "1"}
		
		########################################
		"""
		
		#result = client.service.execute( ContractsSearchBeanIn, SessionChangeBeanIn )


		## Logging Sent message
		#LogInput("INFO:     Sent Message: " + str(client.last_sent()) ,"SOAP","BSCS")

		## Logging Sent message
		#LogInput("INFO     Received Message: \n" + str(client.last_received()) ,"SOAP","BSCS")

		#LogInput( "INFO     Message Sent with this result --> \n%s" % str(result) ,"SOAP","BSCS")
		send_clear(2, 50)
		return result
							
	except WebFault as detail:
		send_alert( 2 , 5 , 50 )
		LogInput("ERROR:    Failed to send to: " + url ,"SOAP-BSCS","ERROR")
		LogInput("ERROR:    Failed, service name: " + service_name ,"SOAP-BSCS","ERROR")
		LogInput("ERROR:    Failed, Message: " + str(Message) ,"SOAP-BSCS","ERROR")
		LogInput("ERROR:    Exception happened in the connection:  " + str(traceback.format_exc()) ,"SOAP-BSCS","ERROR")
		
	
	except:
		send_alert( 2 , 5 , 50 )
		LogInput("ERROR:    Failed to send to: " + url ,"SOAP-BSCS","ERROR")
		LogInput("ERROR:    Failed, service name: " + service_name ,"SOAP-BSCS","ERROR")
		LogInput("ERROR:    Failed, Message: " + str(Message) ,"SOAP-BSCS","ERROR")
		LogInput("ERROR:    Exception happened in the connection:  " + str(traceback.format_exc()) ,"SOAP-BSCS","ERROR")

	 

#===============================================================================
# Second main fucntion
#===============================================================================
def SetSudsClient(url):
	
	

	######################### Basic Authentication ####################################
	class HTTPSudsPreprocessor(BaseHandler):
		def http_request(self, req):
			req.add_header("Content-Type", "text/xml; charset=utf-8")
			req.add_header("WWW-Authenticate", "Basic realm='Control Panel'")
			#The below lines are to encode the credentials automatically
			cred=userid+":"+passwd
			if cred!=None:
				enc_cred=base64.encodestring(cred)
				req.add_header("Authorization", "Basic "+ enc_cred.replace("\012",""))
			return req
		https_request = http_request
	
	http = HttpTransport()
	opener = build_opener(HTTPSudsPreprocessor)
	http.urlopener = opener
	
	# for Performance we cache the wsdl file and xsd
	#cache10 = client.options.cache
	#cache10.setduration(days=10)
	 
	######################### For  Basic Authentication #################################
	#client = Client(url, location=url.replace("?wsdl",""), transport=http, cache=None, timeout=90, faults=False, retxml=True)
	
		
	#return Client(url, location=url.replace("?wsdl",""), transport=http, timeout=90, faults=False)
	return Client(url, location=url.replace("?wsdl",""), timeout=90, faults=False, autoblend=True)
	#####################################################################################

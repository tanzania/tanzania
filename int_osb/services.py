from send_soap import SendSoap
from send_soapBiling import SendSoapBiling

from configuration import GetOSBserver, LogInput
from datetime import datetime
import traceback
from time import sleep

from SNMP_app.alerts import send_alert, send_clear

# Check Balance
def GetNewCheckBalance(inInformation):
	'''
	####################################################
	# 		 
	#		GetTotalDuesProxy
	#		 Check Balance
	#
	####################################################
	'''
	service_name = "getBillInfo"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	url = "http://10.192.178.30/CommonServices_SB/ProxyService/GetTotalDuesProxy?wsdl"

	Message = { 
		"msisdn" : inInformation["PhoneNumber"],
	}

	try:
		Respond = SendSoapBiling(service_name, url, Message)
		send_clear(3, 1)
	except:
		send_alert( 3 , 1 , 1 )
		LogInput("ERROR:  OSB  GetNewCheckBalance  %s  FAILD to SendSoap >>>/n %s" % (str(inInformation["PhoneNumber"]), str(traceback.format_exc())) ,str(inInformation["NPOrderID"]),"process")
	
	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  OSB  GetNewCheckBalance try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1][0]
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
					"CurrentBalance" : MessageContent["totalDueAmount"],
				}
				send_clear(3, 1)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  GetNewCheckBalance try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoapBiling(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 3 , 1 , 1 )
		LogInput("ERROR:  OSB  GetNewCheckBalance  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	send_alert( 3 , 1 , 1 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic


def GetContractInfo(inInformation):
	'''
	####################################################
	# 		 
	#		
	#		Port OUT - 1 - 1   CONTRACTS.SEARCH
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/ContractsSearch?wsdl"
	Message = { 
		"input" : {
			"searcher" : "ContractSearchWithoutHistory",
			"dirnum" : inInformation["PhoneNumber"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}
	try:
		Respond = SendSoap(service_name, url, Message)
		#print(Respond)
		send_clear(2, 3)
	except:
		send_alert( 2 , 1 , 3 )
		LogInput("ERROR:  OSB  GetContractInfo  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200 and Respond[1]["contracts"] != "":
				LogInput("INFO:  OSB  GetContractInfo try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1][0][0][0]
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
					"ContractID" : MessageContent['coId'],
					"CustomerID" : MessageContent['csId'],
					"Plan" : MessageContent['rpcode'],
					"ContractStatus" : MessageContent['coStatus'],
					"ConsumerCode" : MessageContent['csCode'],
				}
				send_clear(2, 3)
				return ResponseDic
			else:
				LogInput("ERROR:  OSB  GetContractInfo try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")

			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 3 )
		LogInput("ERROR:  OSB  GetContractInfo  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	send_alert( 2 , 1 , 3 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	return ResponseDic


#Port OUT - 1 - 2    ADDRESS.READ
def GetCustomerInfo(inInformation):
	'''
	####################################################
	# 		 
	#		Port OUT - 1 - 2    ADDRESS.READ
	#		 
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/AddressRead?wsdl"
	Message = { 
		"input" : {
			"csId" : inInformation["CustomerID"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}
	try:
		Respond = SendSoap(service_name, url, Message)
		#print(Respond)
		send_clear(2, 2)
	except:
		send_alert( 2 , 1 , 2 )
		LogInput("ERROR:  OSB  GetCustomerInfo  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			# check if it is 200 and the contract is not empty
			if Respond[0] == 200:
				LogInput("INFO:  OSB  GetCustomerInfo try # -%s- for  %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1]
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
					"CivilID" : MessageContent['adrSocialseno'],
					"Passport" : MessageContent['adrIdno'],
					"IDType" : MessageContent['idtypeCode'],
					"CustomerID" : MessageContent['csId'],
				}
				send_clear(2, 2)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  GetCustomerInfo try # -%s- for  %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 2 )
		LogInput("ERROR:  OSB  GetCustomerInfo  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 2 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic


#Port OUT - 1 - 3  CUSTOMER.READ
def GetCustomerBalance(inInformation):
	'''
	####################################################
	# 		 
	#		Port OUT - 1 - 3  CUSTOMER.READ
	#		 
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/CustomerRead?wsdl"
	Message = { 
		"input" : {
			"csId" : inInformation["CustomerID"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		#print(Respond)
		send_clear(2, 6)
	except:
		send_alert( 2 , 1 , 6 )
		LogInput("ERROR:  OSB  GetCustomerBalance  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			# check if it is 200 and the contract is not empty
			if Respond[0] == 200:
				LogInput("INFO:  OSB  GetCustomerBalance try # -%s- for  %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1]
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
					"CurrentBalance" : MessageContent['csCscurbalance'][0],
					"PaymentResponsible" : MessageContent['paymentResp'],
					"CustomerIDHigh" : MessageContent['csIdHigh'],
				}
				send_clear(2, 6)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  GetCustomerBalance try # -%s- for  %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 6 )
		LogInput("ERROR:  OSB  GetCustomerBalance  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 6 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic



def ContractUpdate(inInformation):
	'''
	####################################################
	# 		 
	#		#Port OUT - 1 - 4    CONTRACT.WRITE
	#		 
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/ContractWrite?wsdl"
	Message = { 
		"input" : {
			"coId" : inInformation["ContractID"],
			"coStatus" : inInformation["ContractStatus"],
			"reason" : inInformation["Reason"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		#print(Respond)
		send_clear(2, 4)
	except:
		send_alert( 2 , 1 , 4 )
		LogInput("ERROR:  OSB  ContractUpdate  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			# check if it is 200 and the contract is not empty
			if Respond[0] == 200:
				LogInput("INFO:  OSB  ContractUpdate try # -%s- for  %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Update" : "True",
					"Respond" : Respond,
				}
				send_clear(2, 4)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  ContractUpdate try # -%s- for  %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 4 )
		LogInput("ERROR:  OSB  ContractUpdate  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 4 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic


def GetCompanyRegistration(inInformation):
	'''
	#  GetCompanyRegistration  CUSTOMER_INFO.READ to get CR
	# 
	# 
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/CustomerInfoRead?wsdl"
	
	Message = { 
		"input" : {
			"csId" : inInformation["CustomerID"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		#print(Respond)
		send_clear(2, 8)
	except:
		send_alert( 2 , 1 , 8 )
		LogInput("ERROR:  OSB  GetCompanyRegistration  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			# check if it is 200 and the contract is not empty
			if Respond[0] == 200:
				LogInput("INFO:  OSB  GetCompanyRegistration try # -%s- for  %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1]
				ResponseDic = {
					"ResponseStatus" : "200",
					"CompanyRegistration" : MessageContent["text16"],
					"Respond" : Respond,
				}
				send_clear(2, 8)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  GetCompanyRegistration try # -%s- for  %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 8 )
		LogInput("ERROR:  OSB  GetCompanyRegistration  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 8 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic


def SetRingFanceFlag(inInformation):
	'''
	####################################################
	# 		Ring Fence  CUSTOMER_INFO.READ to get CR  #######   RING FENCE 
	#		to set send > RING FENCE
	#		 	to remove send >  ""
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/ContractInfoWrite?wsdl"

	Message = { 
		"input" : {
			"coId" : inInformation["ContractID"],
			"combo18" : inInformation["RingFence"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		#print(Respond)
		send_clear(2, 5)
	except:
		send_alert( 2 , 1 , 5 )
		LogInput("ERROR:  OSB  SetRingFanceFlag  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			# check if it is 200 and the contract is not empty
			if Respond[0] == 200:
				LogInput("INFO:  OSB  SetRingFanceFlag try # -%s- for  %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Update" : "True",
					"Respond" : Respond,
				}
				send_clear(2, 5)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  SetRingFanceFlag try # -%s- for  %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 5 )
		LogInput("ERROR:  OSB  SetRingFanceFlag  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 5 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic



def CheckStatusRequest(inInformation):
	'''
	####################################################
	# 	
	#		
	#		 #Port OUT - 1 - 5    ENHANCED_REQUEST.READ NOT NEEDED
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/EnhancedRequestRead?wsdl"
	Message = { 
		"input" : {
			"transactionId" : inInformation["TransactionID"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		#print(Respond)
		send_clear(2, 10)
	except:
		send_alert( 2 , 1 , 10 )
		LogInput("ERROR:  OSB  CheckStatusRequest  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			# check if it is 200 and the contract is not empty
			if Respond[0] == 200:
				LogInput("INFO:  OSB  CheckStatusRequest try # -%s- for  %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1][0][0][0]
				ResponseDic = {
					"ResponseStatus" : "200",
					"ContractID" : MessageContent['coId'],
					"CustomerID" : MessageContent['csId'],
					"Status" : MessageContent['status'],
					"Respond" : Respond,
				}
				send_clear(2, 10)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  CheckStatusRequest try # -%s- for  %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 10 )
		pass
	send_alert( 2 , 1 , 10 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic


def PortingRequestSearch(inInformation):
	'''
	####################################################
	# 	
	#		Currently used for Broadcast message when porting in
	#		 This service has been giving an auto retry 5 times every 5 sec
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/PortingRequestSearch?wsdl"
	Message = { 
		"input" : {
			"dirnum" : inInformation["PhoneNumber"],
			"status" : inInformation["status"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		#print(Respond)
		send_clear(2, 11)
	except:
		send_alert( 2 , 1 , 11 )
		LogInput("ERROR:  OSB  PortingRequestSearch  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			# check if it is 200 and the contract is not empty
			if Respond[0] == 200:
				LogInput("INFO:  OSB  PortingRequestSearch try # -%s- for  %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1][0][0][0]
				ResponseDic = {
					"ResponseStatus" : "200",
					"ContractID" : MessageContent['coId'],
					"CustomerID" : MessageContent['csId'],
					"Status" : MessageContent['status'],
					"Respond" : Respond,
				}
				send_clear(2, 11)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  PortingRequestSearch try # -%s- for  %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 11 )
		pass
	send_alert( 2 , 1 , 11 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic



def PhoneNumberExport(inInformation):
	'''
	####################################################
	# 	
	#		
	#		 #Port OUT - 2   PHONE_NUMBER.EXPORT
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/PhoneNumberExport?wsdl"
	Message = { 
		"input" : {
			"npcode" : inInformation["npcode"],
			"portingDate" : datetime.now(),
			"phoneNumber" : inInformation["PhoneNumber"],
			"destPlcodePub" : inInformation["destPlcodePub"],
			"errorCodeInd" : inInformation["errorCodeInd"],
			"reason" : inInformation["Reason"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		#print(Respond)
		send_clear(2, 12)
	except:
		send_alert( 2 , 1 , 12 )
		LogInput("ERROR:  OSB  PhoneNumberExport  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			# check if it is 200 and the contract is not empty
			if Respond[0] == 200:
				LogInput("INFO:  OSB  PhoneNumberExport try # -%s- for  %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1]
				ResponseDic = {
					"ResponseStatus" : "200",
					"PortingRequestID" : MessageContent['portingRequestId'],
					"Status" : MessageContent['status'],
					"Reason" : MessageContent['reason'],
					"Respond" : Respond,
				}
				send_clear(2, 12)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  PhoneNumberExport try # -%s- for  %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 12 )
		pass
	send_alert( 2 , 1 , 12 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic


def PortingRequestRemarkNew(inInformation):
	'''
	####################################################
	# 	
	#		
	#		 #Port OUT - 3     PORTING_REQUEST_REMARK.NEW
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/PortingRequestRemarkNew?wsdl"
	Message = { 
							"input" : {
								"portingRequestId" : inInformation["PortingRequestID"],
								"description" : inInformation["Description"],
							},
							"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
						}
	try:
		Respond = SendSoap(service_name, url, Message)
		#print(Respond)
		send_clear(2, 13)
	except:
		send_alert( 2 , 1 , 13 )
		LogInput("ERROR:  OSB  PortingRequestRemarkNew  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			# check if it is 200 and the contract is not empty
			if Respond[0] == 200:
				LogInput("INFO:  OSB  PortingRequestRemarkNew try # -%s- for  %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Update" : "True",
					"Respond" : Respond,
				}
				send_clear(2, 13)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  PortingRequestRemarkNew try # -%s- for  %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 13 )
		pass
	send_alert( 2 , 1 , 13 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}



def PortingRequestWrite(inInformation):
	'''
	####################################################
	# 	
	#		
	#		 #Port OUT - 4     PORTING_REQUEST.WRITE
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/PortingRequestWrite?wsdl"
	Message = { 
		"input" : {
			"portingRequestId" : inInformation["PortingRequestID"],
			"reason" : inInformation["Reason"],
			"status" : inInformation["Status"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		send_clear(2, 14)
	except:
		send_alert( 2 , 1 , 14 )
		LogInput("ERROR:  OSB  PortingRequestWrite  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
		
	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  OSB  PortingRequestWrite try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
					"Update" : "True",
				}
				send_clear(2, 14)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  PortingRequestWrite try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 14 )
		LogInput("ERROR:  OSB  PortingRequestWrite  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 14 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic



def PhoneNumberRegister(inInformation):
	'''
	####################################################
	# 	
	#		
	#		 #Port IN - 1     PHONE_NUMBER.IMPORT
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/PhoneNumberImport?wsdl"
	Message = { 
		"input" : {
			"destPlcodePub" : "DTAG1",
			"hmcodePub" : "GSM 95",
			"localPrefix" : 965,
			"npcode" : 1,
			"portingDate" : datetime.now(),
			"srcPlcodePub" : "MNPE",
			"suffix" : inInformation["PhoneNumber"].split('965',1)[1],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		send_clear(2, 15)
	except:
		send_alert( 2 , 1 , 15 )
		LogInput("ERROR:  OSB  PhoneNumberRegister  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
		
	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  OSB  PhoneNumberRegister try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1]
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
					"DNID" : MessageContent["dnId"]
				}
				send_clear(2, 15)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  PhoneNumberRegister try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 15 )
		LogInput("ERROR:  OSB  PhoneNumberRegister  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 15 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic



def PhoneNumberReImport(inInformation):
	'''
	#################################################### 
	# 	
	#		
	#		 #NP Return - 1     PHONE_NUMBER.REIMPORT
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/PhoneNumberReimport?wsdl"
	Message = { 
		"input" : {
			"phoneNumber" : inInformation["PhoneNumber"],
			"npcode" : 1,
			"portingDate" : datetime.now(),
			"srcPlcode" : 1382,
			"reason" : 61,
			"errorCodeInd" : "T",
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		send_clear(2, 16)
	except:
		send_alert( 2 , 1 , 16 )
		LogInput("ERROR:  OSB  PhoneNumberReImport  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
		
	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  OSB  PhoneNumberReImport try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1]
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
				}
				send_clear(2, 16)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  PhoneNumberReImport try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 16 )
		LogInput("ERROR:  OSB  PhoneNumberReImport  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 16 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic


def GetCustomerInfoWrite(inInformation):
	'''
	####################################################
	# 	
	#		
	#		 #Ring Fence - 1     CUSTOMER_INFO.Write   Call to set ring fence "RING FENCE" and NULL for unringfence   NOT NEEDED
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/CustomerInfoWrite?wsdl"
	Message = { 
		"input" : {
			"csId" : inInformation["CustomerID"],
			"combo18" : inInformation["RingFence"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		send_clear(2, 9)
	except:
		send_alert( 2 , 1 , 9 )
		LogInput("ERROR:  OSB  GetCustomerInfoWrite  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
		
	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  OSB  GetCustomerInfoWrite try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
				}
				send_clear(2, 9)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  GetCustomerInfoWrite try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 9 )
		LogInput("ERROR:  OSB  GetCustomerInfoWrite  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 9 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic

   
def GetCompanyTopLevel(inInformation):
	'''
	####################################################
	# 	
	#		
	#		 #CR business - 1     CUSTOMERS.SEARCH
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/CustomersSearch?wsdl"
	Message = { 
		"input" : {
			"csCode" : inInformation["CustomerCode"],
			"csLevelCode" : 10,
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		send_clear(2, 7)
	except:
		send_alert( 2 , 1 , 7 )
		LogInput("ERROR:  OSB  GetCompanyTopLevel  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
		
	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  OSB  GetCompanyTopLevel try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1][1][0][0]
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
					"CustomerID" : MessageContent['csId'],
				}
				send_clear(2, 7)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  GetCompanyTopLevel try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 7 )
		LogInput("ERROR:  OSB  GetCompanyTopLevel  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 7 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic

def SetCustomerPenalty(inInformation):
	'''
	####################################################
	# 	
	#		
	#		 # Booking request write - charge deactivation penalty
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/BookingRequestWrite?wsdl"
	Message = { 
		"input" : {
			"csId" : inInformation["CustomerID"],
			"coId" : inInformation["ContractID"],
			"actionCode" : "I",
			"feeClass" : 3,
			"feeType" : "N",
			"numPeriods" : 1,
			"remark" : "Termination Fee OCC",
			"rpcode" : 2,
			"sncode" : 576,
			"spcode" : 1,
			"amount" : {
				"amount" : float(15),
				"currency" : "KWD",
			}								
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}},
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		send_clear(2, 17)
	except:
		send_alert( 2 , 1 , 17 )
		LogInput("ERROR:  OSB  SetCustomerPenalty  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
		
	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  OSB  SetCustomerPenalty try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1][1][0][0]
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
				}
				send_clear(2, 17)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  SetCustomerPenalty try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 17 )
		LogInput("ERROR:  OSB  SetCustomerPenalty  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 17 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic

def PaymentArrangementWrite(inInformation):
	'''
	####################################################
	# 	
	#		
	#		 PaymentArrangementWrite
	#        PAYMENT_ARRANGEMENT.WRITE
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/PaymentArrangementWrite?wsdl"
	Message = { 
		"input" : {
			"csId" : inInformation["CustomerID"],
			"cspPmntId" : int(-2),
			"cspCeiling" : {
				"amount" : float(0.0),
				"currency" : "KWD",
			},
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}}
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		send_clear(2, 18)
	except:
		send_alert( 2 , 1 , 18 )
		LogInput("ERROR:  OSB  PaymentArrangementWrite  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
		
	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  OSB  PaymentArrangementWrite try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				MessageContent = Respond[1]
				ResponseDic = {
					"ResponseStatus" : "200",
					"PaymentID" : MessageContent,
					"Respond" : Respond,
				}
				send_clear(2, 18)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  PaymentArrangementWrite try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 18 )
		LogInput("ERROR:  OSB  PaymentArrangementWrite  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 18 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	#print(ResponseDic)
	return ResponseDic

def CustomerHierarchyWrite(inInformation):
	'''
	####################################################
	# 	
	#		
	#		 CUSTOMER_HIERARCHY.WRITE
	#        
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/CustomerHierarchyWrite?wsdl"
	Message = { 
		"input" : {
			"csContresp" : 'T',
			"paymentResp" : 'T',
			"csId" : inInformation["CustomerID"],
			"csIdHigh" : inInformation["CustomerIDHigh"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}}
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		send_clear(2, 19)
	except:
		send_alert( 2 , 1 , 19 )
		LogInput("ERROR:  OSB  CustomerHierarchyWrite  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
		
	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  OSB  CustomerHierarchyWrite try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
				}
				send_clear(2, 19)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  CustomerHierarchyWrite try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 19 )
		LogInput("ERROR:  OSB  CustomerHierarchyWrite  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 19 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	return ResponseDic


def CustomerFlatWrite(inInformation):
	'''
	####################################################
	# 	
	#		
	#		 CUSTOMER_FLAT.WRITE
	#        
	#
	####################################################
	'''
	service_name = "execute"
	ServerInfo = GetOSBserver(GetOSBserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	url = "http://" + ServerAddress + "/ws_v3/services/CustomerFlatWrite?wsdl"
	Message = { 
		"input" : {
			"csId" : inInformation["CustomerID"],
		},
		"sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}}
	}

	try:
		Respond = SendSoap(service_name, url, Message)
		send_clear(2, 20)
	except:
		send_alert( 2 , 1 , 20 )
		LogInput("ERROR:  OSB  CustomerHierarchyWrite  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
		
	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  OSB  CustomerHierarchyWrite try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
				}
				send_clear(2, 20)
				return ResponseDic
				#print(ResponseDic)
			else:
				LogInput("ERROR:  OSB  CustomerHierarchyWrite try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
			# wait 5 seconds
			Respond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 2 , 1 , 20 )
		LogInput("ERROR:  OSB  CustomerHierarchyWrite  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 2 , 1 , 20 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	return ResponseDic

''' NOT NEEDED, not used
def PaymentArrangementAssignment(inInformation):
 ####################################################
 #    
 #  PAYMENT_ARRANGEMENT_ASSIGNMENT.WRITE
 #   
 #
 ####################################################
 service_name = "execute"
 url = "http://10.192.176.10:9140/ws_v3/services/PaymentArrangementAssignmentWrite?wsdl"
 Message = { 
  "input" : {
   "paymentId" : inInformation["PaymentID"],
   "termCode" : 0,
   "validFrom" : {
    "date" : (datetime.date.today() + datetime.timedelta(days=1)).strftime("%Y-%m-%dT00:00:00.000Z"),
   },
  },
  "sessionChange" : { "values" : { "item" : { "value" : "1", "key" : "BU_ID"}}}
 }
 Respond = SendSoap(service_name, url, Message)
 MessageContent = Respond[1]
 ResponseDic = {
  "ResponseStatus" : "200",
  "PaymentArrangementID" : MessageContent,
  "Respond" : Respond,
 }
 return ResponseDic
'''
from models import IntegrationRAS
from web.models import NpMessages
from django.forms import ModelForm, Textarea, extras
from django import forms
from django.forms.models import inlineformset_factory
from configuration import LogInput, GetRequest
from django.core.validators import RegexValidator

class RASForm(ModelForm):
	SUBSCRIPTION_TYPE = (
		('prepaid', 'Prepaid'),
		('postpaid', 'Postpaid'),
	)
	CUSTOMER_TYPE = (
		('consumer', 'Consumer'),
		('business', 'Business'),
	)

	SubscriptionType = forms.ChoiceField(
		label = "Subscription", 
		choices = SUBSCRIPTION_TYPE,
		#initial = 'ALL', 
		widget = forms.Select(),
		required=True,
	)
	CustomerType = forms.ChoiceField(
		label = "Customer Type", 
		choices = CUSTOMER_TYPE,
		#initial = 'ALL', 
		widget = forms.Select(),
		required=True,
	)
	# Added SIM Validation to be 19 digits long and ends with F < capital
	SIM = forms.CharField(
		label='SIM Number', 
		required=False,
		validators=[
			RegexValidator(
				regex='^[0-9]{19}[0123456789F]$',
				message='The SIM must be 19 digits long and end with 0,1,2,3,4,5,6,7,8,9 or F',
				code='invalid_SIM',
			),
		]

	)
	class Meta:
		model = NpMessages
		fields = (
			'SubscriptionType',
			'CustomerType',
			'SIM',
		)

	def clean(self):
		#cleaned_data = self.cleaned_data # individual field's clean methods have already been called
		SubscriptionType = self.cleaned_data.get("SubscriptionType")
		SIM = self.cleaned_data.get("SIM")
		LogInput("The SIM is: " + str(SIM))
		if SubscriptionType == "prepaid" and SIM == "":
			if not SIM:
				if not self._errors.has_key('SIM'):
					from django.forms.util import ErrorList
					self._errors['SIM'] = ErrorList()
					self._errors['SIM'].append('SIM # must be entered')
			raise forms.ValidationError("SIM # must be entered") 
			#print 'error here'

		#print self.cleaned_data
		#raise forms.ValidationError("Range is not correct.") 
		return self.cleaned_data

RASFormSet = inlineformset_factory(NpMessages, IntegrationRAS, form=RASForm, can_delete=False, extra=1)

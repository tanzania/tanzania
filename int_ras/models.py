from django.db import models
from web.models import NpMessages

# Create your models here.
class IntegrationRAS(models.Model):
	SUBSCRIPTION_TYPE = (
		('prepaid', 'Prepaid'),
		('postpaid', 'Postpaid'),
	)
	CUSTOMER_TYPE = (
		('consumer', 'Consumer'),
		('business', 'Business'),
	)
	np_messages = models.ForeignKey(NpMessages, related_name='IntegrationRAS')
	#np_messages = models.OneToOneField(NpMessages, primary_key=True, related_name='IntegrationRAS')
	PhoneNumber = models.CharField(max_length=12, blank=False, null=False)
	DNID = models.IntegerField(max_length=50, blank=False, null=True)
	SIM = models.CharField(max_length=254, blank=True, null=True)
	SubscriptionType = models.CharField("Subscription", max_length=20, choices = SUBSCRIPTION_TYPE, blank=False, null=True)
	CustomerType = models.CharField("Customer", max_length=20, choices = CUSTOMER_TYPE, blank=False, null=True)
	RASUpdated = models.NullBooleanField(default=False, db_index=True)
	dealer_id = models.CharField(max_length=50)

	class Meta:
		db_table = "np_integration_ras"

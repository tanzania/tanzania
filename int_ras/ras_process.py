from services import *
from time import sleep
from int_osb.services import *
from web.models import NpMessages
from int_ras.models import IntegrationRAS

##########################################################################
#
#			pre	Prepaid process
#
##########################################################################
def IncomingAcceptPrepaid(InMessage):
	AllPhoneNumbers = []

	try:
		Order = NpMessages.objects.get(NPOrderID = InMessage.NPOrderID, MessageTypeName="NPRequest")

		for Route in Order.RoutingInfoList.all():
			list = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 
			for number in list:
				AllPhoneNumbers.append(str(number))			
	except Exception as e:
		# this is vaild for incoming NpRequest only
		Order = InMessage
		for Route in InMessage.RoutingInfoList.all():
			list = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 
			for number in list:
				AllPhoneNumbers.append(str(number))			
	#print(Order.pk)
	for PhoneNumber in AllPhoneNumbers:
		#print(PhoneNumber)
		RASMessage = IntegrationRAS.objects.get(np_messages=Order, PhoneNumber=str("965" + PhoneNumber))
		SendMessage = {
			"PhoneNumberNoCode" : PhoneNumber,
			"NPOrderID" : Order.NPOrderID,
			"PhoneNumber" : PhoneNumber,
			"DNID" : RASMessage.DNID,
			"ICCID" : RASMessage.SIM,
			"PhoneNumber" : "965" + PhoneNumber,
			"DNID" : RASMessage.DNID,
			"SIM" : RASMessage.SIM,
		}
		'''
		# save the phone number to the ras system
		RASMessage.PhoneNumber = PhoneNumber
		RASMessage.save()
		'''
		LogInput("INFO:  RAS  Sending to NewPrepaidCustomer Message  %s" % (str(SendMessage)),str(Order.NPOrderID),"process")
		Results = NewPrepaidCustomer(SendMessage)
		LogInput("INFO:  RAS  sent >  Results: %s" % (str(Results)),str(Order.NPOrderID),"process")
		#print(Results)

		LogInput("INFO:  RAS  Sending to NewPrepaidCustomerPreactivation Message  %s" % ( str(SendMessage)),str(Order.NPOrderID),"process")
		Results = NewPrepaidCustomerPreactivation(SendMessage)
		LogInput("INFO:  RAS  sent >  Results: %s" % (str(Results)),str(Order.NPOrderID),"process")
		#print(Results)

##########################################################################
#
#			post	Postpaid process
#
##########################################################################
def IncomingAcceptPostpaid(InMessage):
	AllPhoneNumbers = []
	try:
		Order = NpMessages.objects.get(NPOrderID = Order.NPOrderID, MessageTypeName="NPRequest")
		for Route in Order.RoutingInfoList.all():
			list = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 
			for number in list:
				AllPhoneNumbers.append(str(number))			
	except Exception as e:
		# this is vaild for incoming NpRequest only
		Order = InMessage
		for Route in InMessage.RoutingInfoList.all():
			list = range(int(Route.PhoneNumberStart), int(Route.PhoneNumberEnd)+1) 
			for number in list:
				AllPhoneNumbers.append(str(number))			
	

	for PhoneNumber in AllPhoneNumbers:
		try:
			#print(PhoneNumber)
			RASMessage = IntegrationRAS.objects.get(np_messages=Order, PhoneNumber=str("965" + PhoneNumber))
			#RASMessage.pk = None
			#print("DNID >>> " + str(RASMessage.DNID))
			SendMessage = {
				"PhoneNumberNoCode" : PhoneNumber,
				"NPOrderID" : Order.NPOrderID,
				"DNID" : RASMessage.DNID,
				"CIVIL_ID" : InMessage.CustomerID,
				"DEALER_ID" : RASMessage.dealer_id,
			}
			'''
			if RASMessage.CustomerType == "business":
				SendMessage["DEALER_ID"] = "77000"
			elif RASMessage.CustomerType == "consumer":
				SendMessage["DEALER_ID"] = "999999"
			'''
			LogInput("INFO:  RAS  Sending Message  %s" % ( str(SendMessage)),str(Order.NPOrderID),"process")
			Results = NewPostpaidCustomer(SendMessage)
			LogInput("INFO:  RAS  After Sending Message >  Results: %s" % (str(Results)),str(Order.NPOrderID),"process")
			#print(Results)
			print("worked " + str(PhoneNumber))
		except:
			pass
			#print("Didn't work " + str(PhoneNumber))
##########################################################################
##########################################################################
##########################################################################
##########################################################################
#
#				Phone Number Register 		- Used for prepaid and postpaid - SHOULD NOT BE USED
#
##########################################################################
'''
def PhoneNumberRegisterDef(InPhoneNumber, NPOrderID):
	Message = { 
		"PhoneNumber" : InPhoneNumber,
	}
	result = PhoneNumberRegister(Message)
	tries = 0
	LogInput("INFO:  Results of PhoneNumberRegister(Message)  > %s" % (str(result)),str(NPOrderID),"process")
	
	while result.get("ResponseStatus") != '500' and tries != 5:
		tries = tries + 1
		
		if result.get("ResponseStatus") == '200':
			LogInput("INFO:  RAS  PhoneNumberRegister try # -%s- for %s  SUCESS > %s" % ( str(tries), str(InPhoneNumber), str(result)),str(NPOrderID),"process")
			return result["DNID"]
			
		else:
			LogInput("ERROR:  RAS  PhoneNumberRegister try # -%s- for %s  ERROR > %s" % (str(tries), str(InPhoneNumber), str(result)),str(NPOrderID),"process")
		# wait 5 seconds
		result = PhoneNumberRegister(Message)
		sleep(3)
'''
##########################################################################
#
#				New Prepaid Customer 		- Used for Prepaid only
#
##########################################################################
'''
def NewPrepaidCustomerDef(Message, InPhoneNumber, NPOrderID):
	from int_osb.services import PhoneNumberRegister
	result = NewPrepaidCustomer(Message)
	tries = 0

	while result.get("ResponseStatus") != '500' and tries != 5:
		tries = tries + 1
		LogInput("ResponseStatus > " + str(result.get("ResponseStatus")))
		if result.get("ResponseStatus") == "200":
			LogInput("INFO:  RAS  NewPrepaidCustomer try # -%s- for %s  SUCESS > %s" % (str(tries), str(InPhoneNumber), str(result)),str(NPOrderID),"process")
			return result
		else:
			LogInput("ERROR:  RAS  NewPrepaidCustomer try # -%s- for %s  ERROR > %s" % (str(tries), str(InPhoneNumber), str(result)),str(NPOrderID),"process")
		# wait 5 seconds
		result = NewPrepaidCustomer(Message)
		sleep(3)
'''
##########################################################################
#
#				New Postpaid Customer 		- Used for postpaid only
#
##########################################################################
'''
def NewPostpaidCustomerDef(Message, InPhoneNumber, NPOrderID):
	result = NewPostpaidCustomer(Message)
	tries = 0

	while result.get("ResponseStatus") != '500' and tries != 5:
		tries = tries + 1
		LogInput("ResponseStatus > " + str(result.get("ResponseStatus")))
		if result.get("ResponseStatus") == "200":
			LogInput("INFO:  RAS  NewPostpaidCustomer try # -%s- for %s  SUCESS > %s" % (str(tries), str(InPhoneNumber), str(result)),str(NPOrderID),"process")
			return result
		else:
			LogInput("ERROR:  RAS  NewPostpaidCustomer try # -%s- for %s  ERROR > %s" % (str(tries), str(InPhoneNumber), str(result)),str(NPOrderID),"process")
		# wait 5 seconds
		result = NewPostpaidCustomer(Message)
		sleep(3)
'''
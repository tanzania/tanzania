from configuration import GetConfig, LogInput, GetRequest, LogMessageResutls
from suds import WebFault
from suds.client import Client
from suds.transport.http import HttpAuthenticated
from suds.transport.https import HttpTransport
from urllib2 import HTTPBasicAuthHandler, build_opener, install_opener, urlopen, BaseHandler
import base64
import traceback

## Importing WSSE
from suds.wsse import *
from SNMP_app.alerts import send_alert, send_clear

def SendSoap(service_name, url, Message):
	
	try:
		#LogInput("INFO     int_ras.send_soap >>   Quering destination service","SOAP","RAS")
		ResultDic = {}
		client = SetSudsClient(url)

		LogInput("INFO:     Destination URL: " + url ,"SOAP","RAS")

		LogInput("INFO:     Sent Message: " + str(Message),"SOAP","RAS")
		Command = "client.service." + service_name + "(**Message)"
		LogInput( "INFO:     SUDS Command: " + str(Command),"SOAP","RAS")	
		
		result = eval(Command)
		ResultDic = {
			"result" : result,
			"sent" : client.last_sent(),
			"received" : client.last_received(),
		}
		
		LogInput("INFO:     Sent message: " + str(client.last_sent()) ,"SOAP","RAS")
		LogInput("INFO:     Received message: " + str(client.last_received()) ,"SOAP","RAS")


		# EMAIL alert machenisem for any failer in sending soap messages
		try:
			if result[0] == 500:
				if GET_MAILING_LIST("SOAP Error")["enabled"]:
					from django.core.mail import send_mail
					subject = 'NPG Alert - SOAP ERROR BSCS '
					msg = "Sent message > \n" + str(client.last_sent()) + "\n\n\nReceived message > \n" + str(client.last_received())
					send_mail(subject, msg, GET_MAILING_LIST("SOAP Error")["from"], GET_MAILING_LIST("SOAP Error")["to"], fail_silently=True)
				else:
					LogInput("INFO:    Email alert disabled ","SOAP-ERROR","RAS")

				LogInput("INFO:     Sent message: " + str(client.last_sent()) ,"SOAP-ERROR","RAS")
				LogInput("INFO:     Received message: " + str(client.last_received()) ,"SOAP-ERROR","RAS")
		except:
			pass


		send_clear(7, 150)
		return result, ResultDic
							
	except WebFault as detail:
		send_alert( 7 , 5 , 150 )
		LogInput("ERROR:    Failed to send to: " + url ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Failed, service name: " + service_name ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Failed, Message: " + str(Message) ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Exception happened in the connection:  " + str(traceback.format_exc()) ,"SOAP-RAS","ERROR")
		return (999,traceback.format_exc()),traceback.format_exc()
		
	
	except:
		send_alert( 7 , 5 , 150 )
		LogInput("ERROR:    Failed to send to: " + url ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Failed, service name: " + service_name ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Failed, Message: " + str(Message) ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Exception happened in the connection:  " + str(traceback.format_exc()) ,"SOAP-RAS","ERROR")
		return (999,traceback.format_exc()),traceback.format_exc()

#===============================================================================
# Second main fucntion
#===============================================================================
def SetSudsClient(url):
	######################### Basic Authentication ####################################
	class HTTPSudsPreprocessor(BaseHandler):
		def http_request(self, req):
			req.add_header("Content-Type", "text/xml; charset=utf-8")
			req.add_header("WWW-Authenticate", "Basic realm='Control Panel'")
			#The below lines are to encode the credentials automatically
			cred=userid+":"+passwd
			if cred!=None:
				enc_cred=base64.encodestring(cred)
				req.add_header("Authorization", "Basic "+ enc_cred.replace("\012",""))
			return req
		https_request = http_request
	
	http = HttpTransport()
	opener = build_opener(HTTPSudsPreprocessor)
	http.urlopener = opener
	
	return Client(url, location=url.replace("?wsdl",""), timeout=90, faults=False, autoblend=True)
	#####################################################################################

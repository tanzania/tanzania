from configuration import GetConfig, LogInput, GetRequest, LogMessageResutls
from suds import WebFault
from suds.client import Client
from suds.transport.http import HttpAuthenticated
from suds.transport.https import HttpTransport
from urllib2 import HTTPBasicAuthHandler, build_opener, install_opener, urlopen, BaseHandler
import base64
import traceback

## Importing WSSE
from suds.wsse import *
from SNMP_app.alerts import send_alert, send_clear

def SendSoapPre(service_name, url, Message):
	
	try:
		LogInput("INFO:     Destination URL: " + url ,"SOAP","RAS")
		try:
			client = SetSudsClient(url)
			LogInput("INFO:     Sent Message: " + str(Message),"SOAP","RAS")
		except:
			LogInput("ERROR:     Set client:  " + str(traceback.format_exc()) ,"SOAP","RAS")

		#############################################################
		# Setting WSSE Security header
		#############################################################

		#security=Security()
		#token=UsernameToken("NpgUser","Npg@1234")
		#security.tokens.append(token)
		#client.set_options(wsse=security)
		NS = ( 'wataniya',"http://osb.wataniya.com")
		CustomeHeader = Element('wataniya:auth', ns=NS) 
		username = Element('username',).setText('NpgUser')
		password = Element('password',).setText('Npg@1234')
		CustomeHeader.append(username)
		CustomeHeader.append(password)
		client.set_options(soapheaders=CustomeHeader)  
		#client.set_options(headers={"wataniya:auth" : {'username' : 'NpgUser', 'password' : 'Npg@1234'}})

		#############################################################
		# End of WSSE header setting
		#############################################################
		
		
		Command = "client.service." + service_name + "(**Message)"
		LogInput( "INFO:     SUDS Command: " + str(Command),"SOAP","RAS")	

		result = eval(Command)
		
		LogInput("INFO:     Sent message: " + str(client.last_sent()) ,"SOAP","RAS")
		LogInput("INFO:     Received message: " + str(client.last_received()) ,"SOAP","RAS")

		# EMAIL alert machenisem for any failer in sending soap messages
		try:
			if result[0] == 500:
				if GET_MAILING_LIST("SOAP Error")["enabled"]:
					from django.core.mail import send_mail
					subject = 'NPG Alert - SOAP ERROR BSCS '
					msg = "Sent message > \n" + str(client.last_sent()) + "\n\n\nReceived message > \n" + str(client.last_received())
					send_mail(subject, msg, GET_MAILING_LIST("SOAP Error")["from"], GET_MAILING_LIST("SOAP Error")["to"], fail_silently=True)
				else:
					LogInput("INFO:    Email alert disabled ","SOAP-ERROR","RAS")

				LogInput("INFO:     Sent message: " + str(client.last_sent()) ,"SOAP-ERROR","RAS")
				LogInput("INFO:     Received message: " + str(client.last_received()) ,"SOAP-ERROR","RAS")
		except:
			pass

		send_clear(7, 151)
		return result
							
	except WebFault as detail:
		send_alert( 7 , 5 , 151 )
		LogInput("ERROR:    Failed to send to: " + url ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Failed, service name: " + service_name ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Failed, Message: " + str(Message) ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Exception happened in the connection:  " + str(traceback.format_exc()) ,"SOAP-RAS","ERROR")
		
	
	except:
		send_alert( 7 , 5 , 151 )
		LogInput("ERROR:    Failed to send to: " + url ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Failed, service name: " + service_name ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Failed, Message: " + str(Message) ,"SOAP-RAS","ERROR")
		LogInput("ERROR:    Exception happened in the connection:  " + str(traceback.format_exc()) ,"SOAP-RAS","ERROR")
	 

#===============================================================================
# Second main fucntion
#===============================================================================
def SetSudsClient(url):
	######################### Basic Authentication ####################################
	class HTTPSudsPreprocessor(BaseHandler):
		def http_request(self, req):
			req.add_header("Content-Type", "text/xml; charset=utf-8")
			req.add_header("WWW-Authenticate", "Basic realm='Control Panel'")
			#The below lines are to encode the credentials automatically
			cred=userid+":"+passwd
			if cred!=None:
				enc_cred=base64.encodestring(cred)
				req.add_header("Authorization", "Basic "+ enc_cred.replace("\012",""))
			return req
		https_request = http_request
	
	http = HttpTransport()
	opener = build_opener(HTTPSudsPreprocessor)
	http.urlopener = opener
	
	return Client(url, location=url.replace("?wsdl",""), timeout=90, faults=False, autoblend=True)
	#####################################################################################

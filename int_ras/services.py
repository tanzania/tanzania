from send_soap import SendSoap
from send_soapPre import SendSoapPre

#from send_soapBiling import SendSoapBiling

from configuration import GetRASserver, LogInput
from datetime import datetime
import traceback
from time import sleep


from SNMP_app.alerts import send_alert, send_clear


# New Prepaid Customer
def NewPrepaidCustomer(inInformation):
	'''
	####################################################
	# 		for new Prepaid customer port in
	#		PrepaidMNPService
	#		
	#
	####################################################
	'''
	ServerInfo = GetRASserver(GetRASserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	service_name = "PrepaidMNP"
	url = "http://10.192.178.30:7008/rig/PrepaidMNPService?WSDL"
	#ser:PrepaidMNPRequest>
	#<ser:PrepaidMNPIn>
	Message = { "PrepaidMNPIn" : {
			"MSISDN" : inInformation["PhoneNumberNoCode"],
			"DNID" : inInformation["DNID"],
			"PRODUCT_TYPE" : "11-Silver 5 for 5",
			"ICCID" : inInformation["ICCID"],
		}
	}
	try:
		Respond, CompleteRespond = SendSoap(service_name, url, Message)
		send_clear(7, 100)
	except:
		send_alert( 7 , 5 , 100 )
		LogInput("ERROR:  RAS  NewPrepaidCustomer  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumberNoCode"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  RAS  NewPrepaidCustomer try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumberNoCode"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
				}
				send_clear(7, 100)
				return ResponseDic
			else:
				LogInput("ERROR:  RAS  NewPrepaidCustomer try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumberNoCode"]), str(Respond)),str(inInformation["NPOrderID"]),"process")

			# wait 5 seconds
			Respond, CompleteRespond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 7 , 5 , 100 )
		LogInput("ERROR:  RAS  NewPrepaidCustomer  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumberNoCode"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 7 , 5 , 100 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	return ResponseDic

def NewPrepaidCustomerPreactivation(inInformation):
	'''
	####################################################
	# 		for Prepaid only 
	#		Create MNPContractServiceProxy-(NMS)
	#		This service has an entery in /etc/hosts file, for PRODUCTION system please change in hosts file
	#
	####################################################
	'''
	ServerInfo = GetRASserver(GetRASserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	service_name = "createContract"
	url = "http://10.192.178.30/ContractServices_SB/Proxy_Services/CreateMNPContractServiceProxy?wsdl"
	Message = { 
		"msisdn" : inInformation["PhoneNumber"],
		"simNumber" : inInformation["SIM"],
		"msisdnID" : inInformation["DNID"],
		"channel" : "MNP",
	}
	try:
		Respond = SendSoapPre(service_name, url, Message)
		send_clear(7, 101)
	except:
		send_alert( 7 , 5 , 101 )
		LogInput("ERROR:  RAS  NewPrepaidCustomerPreactivation  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  RAS  NewPrepaidCustomerPreactivation try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
				}
				send_clear(7, 101)
				return ResponseDic
			else:
				LogInput("ERROR:  RAS  NewPrepaidCustomerPreactivation try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumber"]), str(Respond)),str(inInformation["NPOrderID"]),"process")

			# wait 5 seconds
			Respond = SendSoapPre(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 7 , 5 , 101 )
		LogInput("ERROR:  RAS  NewPrepaidCustomerPreactivation  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumber"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 7 , 5 , 101 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	return ResponseDic

def PortOutDeactivation(inInformation):
	'''
	####################################################
	# 		for Port out Deactivation 
	#		
	#		
	#
	####################################################
	'''
	ServerInfo = GetRASserver(GetRASserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	service_name = "DisconnectionMNP"
	url = "http://10.192.178.30:7008/rig/DisconnectionMNPService?wsdl"
	Message = { "DisconnectionMNPIn" : {
			"MSISDN" : inInformation["PhoneNumberNoCode"],
		}
	}
	try:
		Respond, CompleteRespond = SendSoap(service_name, url, Message)
		send_clear(7, 102)
	except:
		send_alert( 7 , 5 , 102 )
		LogInput("ERROR:  RAS  PortOutDeactivation  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumberNoCode"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  RAS  PortOutDeactivation try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumberNoCode"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
				}
				send_clear(7, 102)
				return ResponseDic
			else:
				LogInput("ERROR:  RAS  PortOutDeactivation try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumberNoCode"]), str(Respond)),str(inInformation["NPOrderID"]),"process")

			# wait 5 seconds
			Respond, CompleteRespond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 7 , 5 , 102 )
		LogInput("ERROR:  RAS  PortOutDeactivation  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumberNoCode"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 7 , 5 , 102 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	return ResponseDic

# Postpaid-(NMS)
def NewPostpaidCustomer(inInformation):
	'''
	####################################################
	# 
	#		Create MNPContractServiceProxy-(NMS)
	#		This service has an entery in /etc/hosts file, for PRODUCTION system please change in hosts file
	#
	####################################################
	'''
	ServerInfo = GetRASserver(GetRASserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	service_name = "PostpaidMNP"
	url = "http://10.192.178.30:7008/rig/PostpaidMNPService?WSDL"
	Message = { "PostpaidMNPIn" : 
		{
			"MSISDN" : inInformation["PhoneNumberNoCode"],
			"DNID" : inInformation["DNID"],
			"CIVIL_ID" : inInformation["CIVIL_ID"],
			"DEALER_ID" : inInformation["DEALER_ID"],
		}
	}
	try:
		Respond, CompleteRespond = SendSoap(service_name, url, Message)
		send_clear(7, 103)
	except:
		send_alert( 7 , 5 , 103 )
		LogInput("ERROR:  RAS  NewPostpaidCustomer  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumberNoCode"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  RAS  NewPostpaidCustomer try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumberNoCode"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
				}
				send_clear(7, 103)
				return ResponseDic
			else:
				LogInput("ERROR:  RAS  NewPostpaidCustomer try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumberNoCode"]), str(Respond)),str(inInformation["NPOrderID"]),"process")

			# wait 5 seconds
			Respond, CompleteRespond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 7 , 5 , 103 )
		LogInput("ERROR:  RAS  NewPostpaidCustomer  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumberNoCode"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 7 , 5 , 103 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	return ResponseDic

def RingFence(inInformation):
	'''
	####################################################
	# 
	#		RING Fence-(NMS)
	#		This is used for RAS integration, for Ring Fence process
	#
	####################################################
	'''
	ServerInfo = GetRASserver(GetRASserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	service_name = "AlertFromOraCRM"
	url = "http://10.192.178.30:31001/CreateAlertCRMToSiebel_SB/Proxy_Services/CreateAlertCRMReqSrvc?WSDL"
	Message = {
		"AlertFromOraCRM" : 
			{
				"msisdn" : inInformation["PhoneNumberNoCode"],
				"alertType" : "CRMAlert",
				"alert" : inInformation["RingFenceRASAlert"],
				#"alert" : "Customer on Ring Fence due to Number Portability",
			}
	}
	try:
		Respond, CompleteRespond = SendSoap(service_name, url, Message)
		send_clear(7, 104)
	except:
		send_alert( 7 , 5 , 104 )
		LogInput("ERROR:  RAS  RingFence  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumberNoCode"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  RAS  RingFence try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumberNoCode"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
				}
				send_clear(7, 104)
				return ResponseDic
			else:
				LogInput("ERROR:  RAS  RingFence try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumberNoCode"]), str(Respond)),str(inInformation["NPOrderID"]),"process")

			# wait 5 seconds
			Respond, CompleteRespond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 7 , 5 , 104 )
		LogInput("ERROR:  RAS  RingFence  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumberNoCode"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 7 , 5 , 104 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	return ResponseDic

def NPReturn(inInformation):
	'''
	####################################################
	# 
	#		NPReturn
	#		This is used for RAS integration, for NPReturn process
	#
	####################################################
	'''
	ServerInfo = GetRASserver(GetRASserver("CONNECTION_TYPE"))
	ServerAddress = ServerInfo["url"] + ":" + ServerInfo["port"]
	service_name = "MNPNumberReturn"
	url = "http://10.192.178.30:7008/rig/NumberReturnMNPService?WSDL"
	Message = {
		"MNPNumberReturnBeanIn" : 
			{
				"MSISDN" : inInformation["PhoneNumberNoCode"],
			}
	}
	try:
		Respond, CompleteRespond = SendSoap(service_name, url, Message)
		send_clear(7, 104)
	except:
		send_alert( 7 , 5 , 104 )
		LogInput("ERROR:  RAS  NPReturn  %s  FAILD to SendSoap > %s  >>/n %s" % (str(inInformation["PhoneNumberNoCode"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")

	try:
		TotalTry = ServerInfo["try"]
		tries = 0
		while tries != TotalTry:
			tries = tries + 1
			if Respond[0] == 200:
				LogInput("INFO:  RAS  NPReturn try # -%s- for %s  SUCESS > %s" % (str(tries), str(inInformation["PhoneNumberNoCode"]), str(Respond)),str(inInformation["NPOrderID"]),"process")
				ResponseDic = {
					"ResponseStatus" : "200",
					"Respond" : Respond,
				}
				send_clear(7, 104)
				return ResponseDic
			else:
				LogInput("ERROR:  RAS  NPReturn try # -%s- for %s  ERROR > %s" % (str(tries), str(inInformation["PhoneNumberNoCode"]), str(Respond)),str(inInformation["NPOrderID"]),"process")

			# wait 5 seconds
			Respond, CompleteRespond = SendSoap(service_name, url, Message)
			sleep(ServerInfo["delay"])
	except:
		send_alert( 7 , 5 , 104 )
		LogInput("ERROR:  RAS  NPReturn  %s  FAILD > %s  >>/n %s" % (str(inInformation["PhoneNumberNoCode"]),str(traceback.format_exc()), str(Respond)),str(inInformation["NPOrderID"]),"process")
	send_alert( 7 , 5 , 104 )
	ResponseDic = {
		"ResponseStatus" : "500",
		"Respond" : Respond,
	}
	return ResponseDic


from django.db import models
from django.forms import ModelForm
from django import forms


ALERTS = (
          ('Email','Email'),
          ('SMS','SMS'),
          ("Groups","Groups"),          
          )

TRANSPORT = (
          ('Email','Email'),
          ('SMS','SMS'),
          )

DIRECTION = (
          ('Sent','Sent'),
          ('Received','Received'),
          )

NPMESSAGES = (            
          ('NPRequest','NPRequest'),
          ('NPRequestCancel','NPRequestCancel'),
          ('NPDonorReject','NPDonorReject'),
          ('NPDonorAccept','NPDonorAccept'),
          
          ('NPExecutionCancel','NPExecutionCancel'),
          ('NPExecution','NPExecution'),
          ('NPReturn','NPReturn'),
          ('NPReturnExecution','NPReturnExecution'),
          
          ('NPBillingResolution','NPBillingResolution'),
          ('NPBillingResolutionEnd','NPBillingResolutionEnd'),
          ('NPBillingResolutionReceived','NPBillingResolutionReceived'),
          ('NPBillingResolutionAlert','NPBillingResolutionAlert'),
          
          ('NPBillingResolutionAlertReceived','NPBillingResolutionAlertReceived'),
          ('NPBillingResolutionError','NPBillingResolutionError'),
          )

class Alert(models.Model):
        
    npmessages = models.CharField(max_length=40, choices= NPMESSAGES , blank=False, null=False)
    direction = models.CharField(max_length=10, choices= DIRECTION , blank=False, null=False)
    group = models.CharField(max_length=50, blank=True, null=True)
    time = models.CharField(max_length=20, blank=True, null=True)
    message = models.CharField(max_length = 140, blank=False, null=False)
    
class Groups(models.Model):
    
    group_name  = models.CharField(max_length=20, blank=True, null=True)
    rcpt_name   = models.CharField(max_length=20, blank=False, null=False)
    type        = models.CharField(max_length=10, choices= TRANSPORT , blank=False, null=False)
    email_phone = models.CharField(max_length=50, blank=True, null=True)

class Status(models.Model):
    
    phone    = models.IntegerField(max_length=13,blank=True, null=True)
    email    = models.EmailField(blank=True, null=True)
    npmessages = models.CharField(max_length=40, choices= NPMESSAGES , blank=False, null=False)
    group    = models.CharField(max_length=20, blank=True, null=True)
    message = models.CharField(max_length = 140, blank=False, null=False)
    time     = models.DateTimeField(auto_now_add=True)
 
    
class alert_form(ModelForm):
    # Initial didn't work when attrs has been added to forms.Select, adding an empty tuple solved it
    #npmessages = forms.ChoiceField(widget=forms.Select(attrs={'class':'input-large'}),choices= NPMESSAGES, )
    
    class Meta:
        model = Alert
        fields = ('npmessages','direction','group','time','message')    
        
class groups_form(ModelForm):
    
    groups = forms.CharField(required=True)
    
    class Meta:        
        model = Groups
        fields = ('group_name', 'rcpt_name', 'type', 'email_phone')
        
class delete_group(forms.Form):
    #groups = forms.ChoiceField(widget=forms.Select(attrs={'class':'input-large'}),choices= NPMESSAGES, )
    groups = forms.CharField()
    
class delete_user(forms.Form):
    email_phone = forms.CharField()
    group_user  = forms.CharField()   
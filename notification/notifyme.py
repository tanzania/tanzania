from notification.models import Alert, Groups, Status
from configuration import GetConfig, LogInput, GetRequest

from django.core.mail import send_mail, send_mass_mail

class notifyme():
    '''
    Core notification object
    '''
    def __init__(self, iomessage, direction, response):
        
        self.phones=[]
        self.emails=[]
    
   # def start(self, iomessage, direction, response):  
          
        if direction == 'OUT':
            # Checking if message is successfully sent through its response
            if response[0] in ('200', 200):
                # Getting the message name
                print 'insdie success response'
                self.__core(iomessage)  
                print 'after core send mesasage'
                self.__save_status()
            else:
                pass      
        
        elif direction == 'IN':
            print 'inside direction out'
            self.__core(iomessage)
        
    
    def start(self):
        self.__send_email()
        self.__send_sms()
        
#===========================================================================
# Saving status messages
#===========================================================================
        
    def __save_status(self):

        # Saving emails
        for i in self.emails:
            status = Status()     
            # (mail, group, np message, message)
            status.email = i[0] 
            if i[1]:
                status.group = i[1]
            
            status.npmessages = i[2]
            
            status.message = i[3]
            
            status.save()    
             
        # saving phones        
        for i in self.phones:
            status = Status() 
            # (phone, group, np message, message)
            status.email = i[0] 
            if i[1]:
                status.group = i[1]   
            
            status.npmessages = i[2]
            
            status.message = i[3]
            
            status.save()     
#===========================================================================
# Sending Emails and SMSs
#===========================================================================
        
    def __send_email(self):
        # Format: (mail, group(if any), np message, message)
        print self.emails

#        datatuple = (
#                     ('Subject', 'Message.', 'from@example.com', ['john@example.com']),
#                     ('Subject', 'Message.', 'from@example.com', ['jane@example.com']),
#                    )
        
        # Appending NP message to the user's message
         
        datatuple = [ ('Alert Notification',                                                                     #Subject
                       item[3] + "\n\nYou requested to be notified on that message: " + item[2] + "\n4GTSS Team",#Message body + send np message  
                       'notifyme@4gtss.com',                                                                     #Sender
                       (item[0],                                                                                 #Recipients  
                        ) ) for item in self.emails]
        print 'datatuple',datatuple
        send_mass_mail(datatuple)
        
    def __send_sms(self):
        # Format: (phone, group(if any), np message, message)
        print self.phones
        
#=======================================================================
# Saving to DB and returning the needed data tuple.
#=======================================================================
    def __core(self, iomessage):
        
        # Getting the message name from MessageID
        npmessage = GetRequest(iomessage.get('MessageTypeID'))
        
        if npmessage in ("NPRequest","NPRequestCancel","NPDonorReject","NPDonorAccept","NPExecutionCancel", "NPExecution","NPReturn","NPReturnExecution","NPBillingResolution","NPBillingResolutionEnd","NPBillingResolutionReceived","NPBillingResolutionAlert","NPBillingResolutionAlertReceived","NPBillingResolutionError"):
            
            Alerts = Alert.objects.filter(npmessages=npmessage)
            if Alerts:
                print Alerts
                print 'Found message'
                for alert in Alerts:
                    
                    # Checking if it is a group
                    if alert.group:
                        
                        # Query the Groups DB
                        groups = Groups.objects.filter(group_name=alert.group)
                        print groups
                        for G in groups:
                            if G.type == 'SMS':
                                self.phones.append( (G.email_phone,alert.group, alert.npmessages, alert.message) )
                            elif G.type == 'Email':
                                self.emails.append( (G.email_phone,alert.group, alert.npmessages, alert.message) )
                            else:
                                pass
                              
                    # If it is not a group
                    else:
                        if alert.alert == 'SMS':
                            self.phones.append( (alert.email_phone,'', alert.npmessages, alert.message) )
                        elif alert.alert == 'Email':
                            self.emails.append( (alert.email_phone,'', alert.npmessages, alert.message) )
                        else:
                            pass    
                        
        
                            
    
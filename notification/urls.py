from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('notification',
    url(r'^$', "views.index"),
    url(r'^groups', "views.groups"),
    url(r'^manage', "views.mgmt"),  
)

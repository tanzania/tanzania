from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

# Models
from notification.models import Alert, Groups
# ModelForms
from notification.models import alert_form, groups_form, delete_group, delete_user

def index(request):
    form = alert_form()
    db_groups = Groups.objects.all()
    
    if request.POST:
        form = alert_form(request.POST)

        if form.is_valid():
            form.group = form.cleaned_data['group']
            form.direction = form.cleaned_data['direction']
            form.npmessages = form.cleaned_data['npmessages']
            form.message = form.cleaned_data['message']
            form.time = form.cleaned_data['time']
            
            print 'its a valid response'
            print form
            print 'form dictionary'
            print form.__dict__
            
            form.save()

            return render_to_response("notification/alerts.html",
                                      {'form'     :  form, 
                                       'success'  : 'Alert Saved ..',
                                       'db_groups':  db_groups,
                                       }, context_instance=RequestContext(request)
                                      )
        else:
            return render_to_response("notification/alerts.html",
                                                    {
                                                     'form':form,
                                                     'db_groups':  db_groups,
                                                     }, context_instance=RequestContext(request)
                                      )
    else:
         return render_to_response("notification/alerts.html",
                                                    {
                                                     'form':form,
                                                     'db_groups':  db_groups,
                                                     }, context_instance=RequestContext(request)
                                      )
def groups(request):
    form = groups_form()
    db_groups = Groups.objects.all()
    if request.POST:
       #fields = ('group_name', 'rcpt_name', 'type', 'email_phone')
        form = groups_form(request.POST)
     
        if form.is_valid():
            # If new groups is created
            if form.cleaned_data['groups'] == 'Add New ----':
                
                # If group already exists
                if Groups.objects.filter(group_name=form.cleaned_data['group_name']).exists():
                
                    return render_to_response("notification/groups.html",
                                      {'form'     :  form, 
                                       'Error'  : 'Group already exists ...',
                                       'db_groups':  db_groups,
                                       }, 
                                      context_instance=RequestContext(request)
                                      )
                else:
                    form.group_name = form.cleaned_data['group_name']
            # If an existing group is selected 
            else:
                print 'nice place'
                print form.cleaned_data['groups']
                form.group_name = form.cleaned_data['groups']
                print form.group_name
                    
            form.rcpt_name = form.cleaned_data['rcpt_name']
            form.type = form.cleaned_data['type']
            form.email_phone = form.cleaned_data['email_phone']
            
            # For some reason form.save doesn't work here, probably becuase of the the additionaly field in the modelform
            #form.save()
            
            Group = Groups()
            Group.group_name = form.group_name
            Group.rcpt_name = form.rcpt_name
            Group.type = form.type
            Group.email_phone = form.email_phone
            
            Group.save()
            
            
            return render_to_response("notification/groups.html",
                                      {'form'     :  form, 
                                       'success'  : 'Group Saved ..',
                                       'db_groups':  db_groups,
                                       }, 
                                      context_instance=RequestContext(request)
                                      )
        else:
            return render_to_response("notification/groups.html",
                                                    {
                                                     'form':form,
                                                     'db_groups':  db_groups,
                                                     }, context_instance=RequestContext(request)
                                      )
    else:
         return render_to_response("notification/groups.html",
                                                    {
                                                     'form':form,
                                                     'db_groups':  db_groups,
                                                     }, context_instance=RequestContext(request)
                                      )  
 
def mgmt(request):
     
     db_groups = Groups.objects.all()
     
     if request.POST:
         # First button is pressed
         if 'delete_group' in request.POST:
            form = delete_group(request.POST)
            
            if form.is_valid():
                form.groups = form.cleaned_data['groups']
                print 'form groups validated'
                                                 
                # Delete all the groups with this name
                success=''
                if Groups.objects.filter(group_name=form.groups).exists():
                    Groups.objects.filter(group_name=form.groups).delete()
                    success='Group Deleted ...'
                else:
                    success = "Group Doesn't exist"    
                
                return render_to_response("notification/mgmt.html",
                                                        {
                                                         'success': success,
                                                         'form':form,
                                                         'db_groups':  db_groups,
                                                         }, context_instance=RequestContext(request)
                                          )
            # Form is not valid
            else:
                return render_to_response("notification/mgmt.html",{'db_groups':  db_groups,}, context_instance=RequestContext(request))
                
         # Second button is pressed       
         elif 'delete_user' in request.POST:
             form = delete_user(request.POST)
             
             if form.is_valid():
                 form.email_phone = form.cleaned_data['email_phone']
                 form.group_user = form.cleaned_data['group_user']
                 print 'form email phone validated'  
                 
                 # Delete a user in a group
                 success=''
                 if Groups.objects.filter(group_name=form.group_user ,email_phone=form.email_phone).exists():
                    Groups.objects.filter(group_name=form.group_user ,email_phone=form.email_phone).delete()
                    success='User In Group Deleted ...'
                 else:
                    success = "User Doesn't Exist In This Group"

                   
                 return render_to_response("notification/mgmt.html",
                                                        {
                                                         'success':success,
                                                         'form':form,
                                                         'db_groups':  db_groups,
                                                         }, context_instance=RequestContext(request)
                                              )
             # Form is not valid    
             else:
                 return render_to_response("notification/mgmt.html",{'db_groups':  db_groups,}, context_instance=RequestContext(request))
         # None of those buttons        
         else:
             return render_to_response("notification/mgmt.html",{'db_groups':  db_groups,}, context_instance=RequestContext(request))        
     
     # Form is not submitted    
     else:
         return render_to_response("notification/mgmt.html",{'db_groups':  db_groups,}, context_instance=RequestContext(request))
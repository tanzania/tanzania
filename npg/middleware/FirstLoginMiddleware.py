# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
class FirstLoginMiddleware(object):
 
	def process_request(self, request):
		if request.user.is_authenticated():
			try:
        			profile = request.user.get_profile()
    			except UserProfile.DoesNotExist:
        			profile = UserProfile.objects.create(request.user)
				profile.save()

			if profile.is_first_login:
                                profile.is_first_login = False
                                profile.save()
                                #reverse('users:account_edit')
                                return HttpResponseRedirect("/change")

			else:
				return None
		else:
			return  None

# Django settings for npg project.
from configuration import GetConfig

import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'npg.settings'

#############################################
# Celery Configuration
#############################################
import djcelery
djcelery.setup_loader()
#BROKER_URL = 'django://'
'amqp://4gtss:4GTSS-pass@localhost:5672//'
ALLOWED_HOSTS = ['*']

CELERYD_CONCURRENCY = 1
CELERY_ACKS_LATE = True
############################################

DEBUG = False
TEMPLATE_DEBUG = DEBUG
PROJECT_PATH = os.path.realpath(os.path.dirname(os.path.dirname(__file__)))  
CRISPY_FAIL_SILENTLY = DEBUG
CRISPY_TEMPLATE_PACK = 'bootstrap'

ADMINS = (
	#('Hazim', 'samourh@4gtss.com'),
	#('Bahaa', 'bahaa.latif@4gtss.com'),
	#('Islam', 'islam.mohamed@4gtss.com'),
)
 
MANAGERS = ADMINS

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.mysql',						# Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
		'HOST': GetConfig('DEFAULT_DB_HOST'),						# Set to empty string for localhost. Not used with sqlite3.
		'PORT': '',													# Set to empty string for default. Not used with sqlite3.
		'NAME': GetConfig('DEFAULT_DB_NAME'),						# Or path to database file if using sqlite3.
		'USER': GetConfig('DEFAULT_DB_USER'),						# Not used with sqlite3.
		'PASSWORD': GetConfig('DEFAULT_DB_PASSWORD'),				# Not used with sqlite3.
		'OPTIONS': {
      #'charset': 'utf-8',
      #'use_unicode': True,
			'init_command': 'SET storage_engine=InnoDB',
		}
	},
}

### Email Configuration
EMAIL_USE_TLS = False
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
#EMAIL_HOST = '192.168.7.83'
#EMAIL_HOST = '192.168.7.89'
# The below line was enalbed on march 14 2014 by Wataniya request
EMAIL_HOST = '10.192.30.27'
EMAIL_HOST_USER = 'mnp-npg@wataniya.com'
EMAIL_HOST_PASSWORD = 'password@123'
EMAIL_PORT = 25
SERVER_EMAIL = 'mnp-npg@wataniya.com'
DEFAULT_FROM_EMAIL = 'mnp-npg@wataniya.com'
#############

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Kuwait'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = GetConfig("ROOT_DIR") + GetConfig("DJANGO_DIR") + '/templates/images/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'
STORAGE_ROOT = GetConfig("STORAGE")
#STORAGE_ROOT = GetConfig("ROOT_DIR") + GetConfig("DJANGO_DIR") + '/storage/'
STORAGE_URL = '/storage/'


# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = GetConfig("ROOT_DIR") + GetConfig("DJANGO_DIR") + '/templates/'
#STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'
#STATIC_URL = ''
ADMIN_MEDIA_PREFIX = '/static/admin/'
# Additional locations of static files
STATICFILES_DIRS = (
	# Put strings here, like "/home/html/static" or "C:/www/django/static".
	# Always use forward slashes, even on Windows.
	# Don't forget to use absolute paths, not relative paths.
	GetConfig("ROOT_DIR") + GetConfig("DJANGO_DIR") + '/templates/adminb'
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
	'django.contrib.staticfiles.finders.FileSystemFinder',
	'django.contrib.staticfiles.finders.AppDirectoriesFinder',
	'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '2s55w@0!+x()3=@ovzdjlt%t--#%8^$om2*7(8#9nl)&amp;bwy0@g'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
#	'django.template.loaders.eggs.Loader',
	'django.template.loaders.filesystem.Loader',
	 'django.template.loaders.app_directories.Loader',
)





TEMPLATE_CONTEXT_PROCESSORS = (
	"django.contrib.auth.context_processors.auth",
	'django.core.context_processors.request',							   
	"django.core.context_processors.debug",
	"django.core.context_processors.i18n",
	"django.core.context_processors.media",
	"django.core.context_processors.static",
	"django.core.context_processors.tz",
	"django.contrib.messages.context_processors.messages"
) 




MIDDLEWARE_CLASSES = (
	'django.middleware.common.CommonMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'npg.middleware.FirstLoginMiddleware.FirstLoginMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	# Uncomment the next line for simple clickjacking protection:
	# 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'npg.urls'

# Python dotted path to the WSGI application used by Django's runserver.
#WSGI_APPLICATION = 'npg.wsgi.application'

TEMPLATE_DIRS = (
	# Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
	# Always use forward slashes, even on Windows.
	# Don't forget to use absolute paths, not relative paths.
	GetConfig("ROOT_DIR") + GetConfig("DJANGO_DIR") + '/templates/',
)

INSTALLED_APPS = (
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	# Uncomment the next line to enable the admin:
	'django.contrib.admin',
	# Uncomment the next line to enable admin documentation:
	'django.contrib.admindocs',
	'authenticate',
	'soap',
	'send_soap',
	'web',
	'reports',
	'debug_toolbar',
	'sorl.thumbnail',
	'file_resubmit',
	'STP_app',
	### Celery Config ####
	'djcelery',
	'kombu.transport.django',
	'forker',
	#####################
	'django_evolution',
	'int_osb',
	'int_ras',
	'timers',
	'user_profile',
	'SNMP_app',	
)  


CACHES = {
	'default': {
		'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
	},
	'file_resubmit': {
		'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
		'LOCATION': GetConfig("ROOT_DIR") + GetConfig("DJANGO_DIR") + '/storage/temp/'
	}
}

DEBUG_TOOLBAR_PANELS = (
	'debug_toolbar.panels.version.VersionDebugPanel',
	'debug_toolbar.panels.timer.TimerDebugPanel',
	'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
	'debug_toolbar.panels.headers.HeaderDebugPanel',
	#'debug_toolbar.panels.profiling.ProfilingDebugPanel',
	'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
	'debug_toolbar.panels.sql.SQLDebugPanel',
	'debug_toolbar.panels.template.TemplateDebugPanel',
	'debug_toolbar.panels.cache.CacheDebugPanel',
	'debug_toolbar.panels.signals.SignalDebugPanel',
	'debug_toolbar.panels.logger.LoggingPanel',
)

DEBUG_TOOLBAR_CONFIG = {
	'INTERCEPT_REDIRECTS': False,
}

DATE_INPUT_FORMATS = (
	'%d-%m-%Y', 
	'%Y-%m-%d', 
	'%m/%d/%Y', 
	'%m/%d/%y', 
	'%b %d %Y',
	'%b %d, %Y', 
	'%d %b %Y', 
	'%d %b, %Y', 
	'%B %d %Y',
	'%B %d, %Y', 
	'%d %B %Y', 
	'%d %B, %Y',
)

DATETIME_INPUT_FORMATS = (
	'%Y-%m-%d %H:%M:%S', 
	'%Y-%m-%d %H:%M', 
	'%Y-%m-%d',
	'%m/%d/%Y %H:%M:%S', 
	'%m/%d/%Y %H:%M', 
	'%m/%d/%Y',
	'%m/%d/%y %H:%M:%S', 
	'%m/%d/%y %H:%M', 
	'%m/%d/%y'
)

if DEBUG:
	INTERNAL_IPS = ('127.0.0.1','192.168.5.10','41.35.95.8','41.47.36.100',)
	MIDDLEWARE_CLASSES += (
		'debug_toolbar.middleware.DebugToolbarMiddleware',
	)


# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
'''
LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'formatters': {
		'verbose': {
			'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
		},
		'simple': {
			'format': '%(levelname)s %(message)s'
		},
	},
	'filters': {
		'require_debug_false': {
			'()': 'django.utils.log.RequireDebugFalse'
		}
	},
	'handlers': {
		'null': {
			'level': 'DEBUG',
			'class': 'django.utils.log.NullHandler',
		},
		'console':{
			'level': 'DEBUG',
			'class': 'logging.StreamHandler',
			'formatter': 'simple'
		},
		'mail_admins': {
			'level': 'ERROR',
			'class': 'django.utils.log.AdminEmailHandler',
			'filters': ['special']
		}
	},
	'loggers': {
		'django': {
			'handlers': ['null'],
			'propagate': True,
			'level': 'INFO',
		},
		'django.request': {
			'handlers': ['mail_admins'],
			'level': 'ERROR',
			'propagate': False,
		},
		'myproject.custom': {
			'handlers': ['console', 'mail_admins'],
			'level': 'INFO',
			'filters': ['special']
		}
	}
}
'''
# URL of the login page.
LOGIN_URL = '/login/'

## User authentication settings 
AUTH_PROFILE_MODULE = 'authenticate.UserProfile'

## Session expirey when user closes the browser
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

## Session timesout after x seconds
SESSION_COOKIE_AGE = 10900

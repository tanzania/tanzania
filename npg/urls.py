from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.simple import redirect_to
from django.contrib.auth.decorators import login_required


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'npg.views.home', name='home'),
	# url(r'^npg/', include('npg.foo.urls')),
	# Uncomment the admin/doc line below to enable admin documentation:
	url(r'^admin/', include(admin.site.urls)),
	url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
	#url(r'', include('web.urls')),
	url(r'^', include('authenticate.urls')),
	url(r'^services/', include('soap.urls')),
	url(r'^request/', include('web.urls')),
	url(r'^reports/', include('reports.urls')),
	url(r'^forkyone/', include('forker.urls')),
	url(r'^timers/', include('timers.urls')),

	url(r'^nptypes.xsd$', "soap.expose_xsd.send_file"),
	
	# Uncomment the next line to enable the admin:
	# url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STORAGE_URL, document_root=settings.STORAGE_ROOT)

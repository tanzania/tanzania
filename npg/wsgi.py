"""
WSGI config for npg project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""

import os,sys

sys.path.append('/4gtss/mount/site')
sys.path.append('/4gtss/mount/site/npg')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "npg.settings")

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.

from django.core.wsgi import get_wsgi_application
#application = get_wsgi_application()
def stream_length(stream,chunk_size=10):
 #Read until data is shorter than 10. 
 #TODO: Confirm that this approach is right.
    length = 0
    data = stream.read(chunk_size)
    while data is not None and len(data) == chunk_size:
        length += chunk_size
        data = stream.read(chunk_size)
    length += len(data)
        
django_application = get_wsgi_application()
import StringIO        

def application(environ, start_response):
    #f=open('/4gtss/mount/site/npg/soapbox1.log','a')
    #f.write(environ)
    
    ## print 'enviroooooooooooooooooooooooon'
    ## print environ
    ## print '#############################'
    
    #stream = environ["wsgi.input"]
    #print stream.read(10)
    
    #I assumed that "1" means true, it may be also number of chunks. 
    if environ.get("mod_wsgi.input_chunked") == "1":
        stream = environ["wsgi.input"]
        #print stream
        #print 'type: ', type(stream)
        #print '\n All system properities and methods: ', dir(stream)
        #length = 0
        #for byte in stream:
        #    length+=1
        #    print byte
        #print length    
        
        
        #data = stream.next()
        data=''
        data = stream.read()
        #for byte in stream.next():
        #    data+=byte
        
        #The below try-except code works as stream.read(), it even worked in daemon mode, while read only works in embedded.
        #try:
        #    while True:
        #        data+= stream.next() 
        #except:
        #    print 'Done with reading the stream ...'         
            
        ## print data 
        environ["CONTENT_LENGTH"] = str(len(data))
        ## print 'Envrion content length: ',environ["CONTENT_LENGTH"]
        environ["wsgi.input"] = StringIO.StringIO(data)

        
        #environ["CONTENT_LENGTH"] = len(stream.read(length))        
        #environ["CONTENT_LENGTH"] = stream_length(stream)
        

    
    return django_application(environ, start_response)

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)

import npg.monitor
npg.monitor.start(interval=1.0)

#sys.stdout = sys.stderr
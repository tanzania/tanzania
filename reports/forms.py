from django import forms
import datetime

class ReportForm(forms.Form):
	allYears = range(2013, (datetime.datetime.now().year + 1))
	YEAR_CHOICES = []
	for eachYear in allYears:
		YEAR_CHOICES.append(
			(str(eachYear), str(eachYear)),
		)
	MONTH_CHOICES = (
		#('yearly','Yearly Report'),
		#('0','All'),
		('1','01 - January'),
		('2','02 - February'),
		('3','03 - March'),
		('4','04 - April'),
		('5','05 - May'),
		('6','06 - June'),
		('7','07 - July'),
		('8','08 - August'),
		('9','09 - September'),
		('10','10 - October'),
		('11','11 - November'),
		('12','12 - December'),
	)
	REPORT_CHOICES = (
		("Port Out","Port Out"),
		("Port In","Port In"),
	)
	'''
	REPORT_CHOICES = (
		("Summary","Summary"),
		("Port Out","Port Out"),
		("Port In","Port In"),
		("Cross Porting","Cross Porting"),
		("Accepted Requests","Accepted Requests"),
		("Rejected Requests","Rejected Requests"),
	)
	'''
	ReportType = forms.ChoiceField(
		label = "Report Type", 
		choices = REPORT_CHOICES,
		initial = "Summary", 
		widget = forms.Select(),
		required = False,
	)
	Year = forms.ChoiceField(
		label = "Year", 
		choices = YEAR_CHOICES,
		initial = datetime.datetime.now().year, 
		widget = forms.Select(),
		required = False,
	)
	Month = forms.ChoiceField(
		label = "Month", 
		choices = MONTH_CHOICES,
		initial = str(datetime.datetime.now().month), 
		widget = forms.Select(),
		required = False,
	)

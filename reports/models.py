from django.db import models

# Create your models here.


class ReportsEntery(models.Model):
	REPORT_TYPE = (
		('Port Out', 'Port Out'),
		('Port IN', 'Port IN'),
		('Reject Out', 'Reject Out'),
		('Reject IN', 'Reject IN'),
	)
	REPORT_DURATION = (
		('Daily', 'Daily'),
		('Monthly', 'Monthly'),
		('Yearly', 'Yearly'),
	)
	
	EnteryID = models.AutoField(max_length=20, primary_key=True)
	timestamp = models.DateTimeField(max_length=20, auto_now=True, auto_now_add=True)
	date = models.DateField(max_length=20, auto_now_add=False)
	day = models.SmallIntegerField(max_length=5)
	month = models.SmallIntegerField(max_length=5)
	year = models.SmallIntegerField(max_length=5)
	DonorName = models.CharField("Donor Name", max_length=20, blank=True, null=True) 
	RecipientName = models.CharField("Recipient Name", max_length=20, blank=True, null=True)
	report_duration = models.CharField(max_length=20, choices = REPORT_DURATION, blank=True, null=True)
	report_type = models.CharField(max_length=20, choices = REPORT_TYPE, blank=True, null=True)
	comment = models.CharField(max_length=200, blank=True, null=True)
	complete = models.BooleanField(default=False)
	count = models.IntegerField(max_length=20)
	phone_count = models.IntegerField(max_length=50, blank=True, null=True)


	class Meta:
		db_table = "np_ReportsEntery"



'''
class ReportsCount(models.Model):

	class Meta:
		db_table = "np_reports"



	EnteryID = models.AutoField(max_length=20, primary_key=True, blank=True)
	timestamp = models.DateTimeField(max_length=20, auto_now=False, auto_now_add=False, blank=True, null=True)
	date = models.DateField(max_length=20, auto_now_add=True, blank=True, null=True)
	day = models.CharField(max_length=20, blank=True, null=True)
	month = models.CharField(max_length=20, blank=True, null=True)
	year = models.CharField(max_length=20, blank=True, null=True)
	NPRequest = models.CharField(max_length=20, blank=True, null=True)
	NPRequestCancel = models.CharField(max_length=20, blank=True, null=True)
	NPDonorReject = models.CharField(max_length=20, blank=True, null=True)
	NPDonorAccept = models.CharField(max_length=20, blank=True, null=True)
	NPExecutionCancel = models.CharField(max_length=20, blank=True, null=True)
	NPExecution = models.CharField(max_length=20, blank=True, null=True)
	NPReturn = models.CharField(max_length=20, blank=True, null=True)
	NPReturnExecution = models.CharField(max_length=20, blank=True, null=True)
	NPBillingResolution = models.CharField(max_length=20, blank=True, null=True)
	NPBillingResolutionEnd = models.CharField(max_length=20, blank=True, null=True)
	NPBillingResolutionReceived = models.CharField(max_length=20, blank=True, null=True)
	NPBillingResolutionAlert = models.CharField(max_length=20, blank=True, null=True)
	NPBillingResolutionAlertReceived = models.CharField(max_length=20, blank=True, null=True)
	NPBillingResolutionError = models.CharField(max_length=20, blank=True, null=True)
	f14t10 = models.CharField(max_length=20, blank=True, null=True)
	f14t11 = models.CharField(max_length=20, blank=True, null=True)
	f14t13 = models.CharField(max_length=20, blank=True, null=True)
	f14t12 = models.CharField(max_length=20, blank=True, null=True)
	f10t14 = models.CharField(max_length=20, blank=True, null=True)
	f10t11 = models.CharField(max_length=20, blank=True, null=True)
	f10t13 = models.CharField(max_length=20, blank=True, null=True)
	f10t12 = models.CharField(max_length=20, blank=True, null=True)
	f11t14 = models.CharField(max_length=20, blank=True, null=True)
	f11t10 = models.CharField(max_length=20, blank=True, null=True)
	f11t13 = models.CharField(max_length=20, blank=True, null=True)
	f11t12 = models.CharField(max_length=20, blank=True, null=True)
	f13t14 = models.CharField(max_length=20, blank=True, null=True)
	f13t10 = models.CharField(max_length=20, blank=True, null=True)
	f13t11 = models.CharField(max_length=20, blank=True, null=True)
	f13t12 = models.CharField(max_length=20, blank=True, null=True)
	f12t14 = models.CharField(max_length=20, blank=True, null=True)
	f12t10 = models.CharField(max_length=20, blank=True, null=True)
	f12t11 = models.CharField(max_length=20, blank=True, null=True)
	f12t13 = models.CharField(max_length=20, blank=True, null=True)
	report_type = models.CharField(max_length=20, blank=True, null=True)
'''
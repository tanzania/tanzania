from django.conf.urls.defaults import patterns, include, url
from django.views.generic import DetailView, ListView
from configuration import GetConfig, LogInput
from django.contrib.auth.decorators import login_required
from views import index

urlpatterns = patterns('',
	#url(r'^$', "reports.views.index"),
	url(r'^$', login_required(index)),
)


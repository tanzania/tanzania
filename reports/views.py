from configuration import GetConfig, LogInput
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.conf import settings
from web.models import NpMessages
from models import ReportsEntery
from forms import ReportForm
from django.db.models import Sum
from tzinfo import UTC
# Time and Date
from django.utils import timezone
from dateutil.relativedelta import relativedelta
import datetime, time
import os.path
from datetime import date,timedelta

# Report Library files
import pygal
from pygal.style import LightSolarizedStyle,CleanStyle


@csrf_protect
def index(request):
	PageName = "Reports"

	if request.method == 'POST':
		
		form = ReportForm(request.POST)
		if form.is_valid():
			#form values
			Month = int(request.POST['Month'])	
			Year = int(request.POST['Year'])
			ReportType = request.POST['ReportType']

			LogInput("INFO   month selected: " + str(Month),'dev','4gtss')

			if Month == 0:
				LogInput("INFO   starting the report init",'dev','4gtss')
				Report = Reports(Force = False, year = Year)
				LogInput("INFO   report init done",'dev','4gtss')
				SerchResults =  Report.GenerateYearlyReportWeb()
				LogInput("INFO   GenerateYearlyReportWeb done",'dev','4gtss')

			else:
				Report = Reports(month=Month,Force = False,year=Year)
				if ReportType in ('Port Out'):
					Report.PortOutReport()
				elif ReportType in ('Port In'):
					Report.PortInReport()
					
				SerchResults = Report.GenerateMonthlyReportWeb(ReportType)
		
		
			if ReportType in ("Summary","Cross Porting","Accepted Requests","Rejected Requests"):
				return render_to_response('reports/index.html', locals(), context_instance = RequestContext(request))
	else:
		form = ReportForm()
		
	return render_to_response('reports/index.html', locals(), context_instance = RequestContext(request))

class Reports():
	def __init__(self, day="", month="", year="", Force = False, PerRequest = False):
		self.ReportModule = ReportsEntery()
		self.ReportModule.date = datetime.datetime.today()
		
		if day:
			self.ReportModule.day = day
		else:
			self.ReportModule.day = self.ReportModule.date.day
			
		if month:
			self.ReportModule.month = month
		else:
			self.ReportModule.month = self.ReportModule.date.month
			
		if year:
			self.ReportModule.year = year
		else:
			self.ReportModule.year = self.ReportModule.date.year

		self.Force = Force
		self.PerRequest = PerRequest

	def SaveToDB(self,):
		try:
			ReportsEnteryFound = ReportsEntery.objects.get(day = self.ReportModule.day, month = self.ReportModule.month, year = self.ReportModule.year, report_duration = self.ReportModule.report_duration, report_type = self.ReportModule.report_type, RecipientName = self.ReportModule.RecipientName, DonorName = self.ReportModule.DonorName)
			self.ReportModule.pk = ReportsEnteryFound.pk
			self.ReportModule.save()
		except ReportsEntery.DoesNotExist:
			self.ReportModule.save()
	
	def DailyReportsPerRequest(self,Filter):
		self.ReportModule.report_duration = "daily"

		DateFrom = datetime.datetime(self.ReportModule.year, self.ReportModule.month, self.ReportModule.day, 0, 0, 0, 0,tzinfo=UTC())
		DateTo   = datetime.datetime(self.ReportModule.year, self.ReportModule.month, self.ReportModule.day, 23, 59, 59, 999999, tzinfo=UTC())
		DateFrom-=timedelta(hours=3)
		DateTo-=timedelta(hours=3)
		Filter["PortingTime__range"] = [DateFrom, DateTo]
		
		#DayDate = datetime.date(self.ReportModule.year, self.ReportModule.month, self.ReportModule.day)
		#Filter["PortingTime__contains"] = DayDate

		FoundFilter = NpMessages.objects.filter(**Filter)

		FoundFilterDay = FoundFilter.filter(PortingTime__range = [DateFrom, DateTo])
		#FoundFilterDay = FoundFilter.filter(PortingTime__contains = DayDate)

		self.ReportModule.count = FoundFilterDay.count()
		self.ReportModule.phone_count = 0
		for eachFilter in FoundFilterDay:
			self.ReportModule.phone_count = self.ReportModule.phone_count + eachFilter.RoutingInfoList.all().aggregate(count_Phone_num=Sum("TotalNumber"))["count_Phone_num"]
		self.SaveToDB()


	def DailyReports(self,Filter):
		self.ReportModule.report_duration = "daily"
		#Filter["message_created__month"] = self.ReportModule.month

		#if it is december then the year should be next year, this will add one to the year value
		if self.ReportModule.month == 12:
			FindCorrectYear = self.ReportModule.year + 1
			FindCorrectMonth = 1
		else:
			FindCorrectYear = self.ReportModule.year
			FindCorrectMonth = self.ReportModule.month + 1

		#find total days in the selected month
		totalDaysInMonth = (date(FindCorrectYear, FindCorrectMonth, 1) - date(self.ReportModule.year, self.ReportModule.month, 1)).days
		
		#print "totalDaysInMonth: " + str(totalDaysInMonth)
		
		DateFrom = datetime.datetime(self.ReportModule.year, self.ReportModule.month, 1, 0, 0, 0, 0,tzinfo=UTC())
		DateTo   = datetime.datetime(self.ReportModule.year, self.ReportModule.month, totalDaysInMonth, 23, 59, 59, 999999, tzinfo=UTC())
		DateFrom-=timedelta(hours=3)
		DateTo-=timedelta(hours=3)
		Filter["PortingTime__range"] = [DateFrom, DateTo]
		
		#DayDate = datetime.date(self.ReportModule.year, self.ReportModule.month, self.ReportModule.day)
		#Filter["PortingTime__contains"] = DayDate
		
		#print "DateFrom: " + str(DateFrom)
		#print "DateTo: " + str(DateTo)
		#print "Filter normal: " + str(Filter)
		#print "Filter: " + str(Filter)

		FoundFilter = NpMessages.objects.filter(**Filter)
		print FoundFilter.query
		d = 1
		while d <= totalDaysInMonth:
		#print "day: " + str(d)
			self.ReportModule.day = d
			if not self.ReportsFound() or self.Force:
				DateFrom = datetime.datetime(self.ReportModule.year, self.ReportModule.month, d, 0, 0, 0, 0, tzinfo=UTC())
				DateTo = datetime.datetime(self.ReportModule.year, self.ReportModule.month, d, 23, 59, 59, 999999,tzinfo=UTC())
				DateFrom-=timedelta(hours=3)
				DateTo-=timedelta(hours=3)
				
				#DayDate = datetime.date(self.ReportModule.year, self.ReportModule.month, d)
				
				#print "\tDateFrom: " + str(DateFrom)
				#print "\tDateTo: " + str(DateTo)

				FoundFilterDay = FoundFilter.filter(PortingTime__range = [DateFrom, DateTo])	
				#FoundFilterDay = FoundFilter.filter(PortingTime__contains = DayDate)
				print FoundFilterDay.query
				self.ReportModule.count = FoundFilterDay.count()
					#print "ReportModule.count: " + str(self.ReportModule.count)

				# This will get you the total phone numbers in all the orders on that day
				self.ReportModule.phone_count = 0
				for eachFilter in FoundFilterDay:
					#print "self.ReportModule.phone_count: " + str(self.ReportModule.phone_count)
					#print "The aggregate: " + str(eachFilter.RoutingInfoList.all().aggregate(count_Phone_num=Sum("TotalNumber"))["count_Phone_num"])
					#print "NPOrderID: " + str(eachFilter.NPOrderID)
					#print "pk: " + str(eachFilter.pk)
					
					self.ReportModule.phone_count = self.ReportModule.phone_count + eachFilter.RoutingInfoList.all().aggregate(count_Phone_num=Sum("TotalNumber"))["count_Phone_num"]
				#print "Done for looop"
				self.SaveToDB()
				#print "saved to DB"
				# rest the module for new db entry
				self.ReportModule.pk = None
			d += 1
	
	def MonthlyReports(self,):
		'''
		This function would take the month and the year and generated the total of port in, port out of the month, also from which to which operator
		the information is stored in the db, for each month it would generate 6 entry
		'''
		ReportsType = ("Port Out", "Port In")
		self.ReportModule.report_duration = "monthly"
		self.ReportModule.day = 0

		for ReportType in ReportsType:
			AllReports = ReportsEntery.objects.filter(month=self.ReportModule.month,year=self.ReportModule.year,report_type=ReportType).exclude(day=0)
			if ReportType == "Port Out":
				ActOperator = "RecipientName"
				self.ReportModule.DonorName = GetConfig("COMPANY_NAME")
				self.ReportModule.RecipientName = "ALL"			
			elif ReportType == "Port In":
				ActOperator = "DonorName"
				self.ReportModule.DonorName = "ALL"
				self.ReportModule.RecipientName = GetConfig("COMPANY_NAME")			

			# Preparing the db info to be saved, those are the total for all operator of the month but for each report type
			self.ReportModule.phone_count = AllReports.all().aggregate(phone_count_sum=Sum("phone_count"))["phone_count_sum"]
			self.ReportModule.count = AllReports.all().aggregate(count_sum=Sum("count"))["count_sum"]
			self.ReportModule.report_type = ReportType
			self.SaveToDB()

			# Start new DB entry
			self.ReportModule.pk = None
			
			
			Operators = ("Zain", "Viva")
			for Operator in Operators:
				if ReportType == "Port Out":
					self.ReportModule.RecipientName = Operator			
				elif ReportType == "Port In":
					self.ReportModule.DonorName = Operator

				TheFilter = {
					ActOperator : Operator
				}
				OperatorReport = AllReports.filter(**TheFilter)
				
				
				# Preparing the db info to be saved, those are the total for all operator of the month but for each report type
				self.ReportModule.phone_count = OperatorReport.aggregate(phone_count_sum=Sum("phone_count"))["phone_count_sum"]
				self.ReportModule.count = OperatorReport.aggregate(count_sum=Sum("count"))["count_sum"]
				self.ReportModule.report_type = ReportType
				self.SaveToDB()
	
				# Start new DB entry
				self.ReportModule.pk = None
				

	def GenerateYearlyReportWeb(self,):
		AllReports = ReportsEntery.objects.filter(day = 0, year = self.ReportModule.year)
		SerchResults = {
			1 : {},
			2 : {},
			3 : {},
			4 : {},
			5 : {},
			6 : {},
			7 : {},
			8 : {},
			9 : {},
			10 : {},
			11 : {},
			12 : {},
			"year" : self.ReportModule.year,
			"ReportType" : "yearly",
		}
		for eachMonth in (1,2,3,4,5,6,7,8,9,10,11,12):
			MonthReport = AllReports.filter(month = eachMonth)
			for eachReport in MonthReport:
				SerchResults[eachMonth].update({
						eachReport.DonorName + "-" + eachReport.RecipientName : {
						"phone" : eachReport.phone_count,
						"count" : eachReport.count,
					}
				})
		LogInput("INFO   Search result for the year dictionary: " + str(SerchResults),'dev','4gtss')
		return SerchResults
				
	def GenerateMonthlyReportWeb(self,ReportType):
		'''
		This function will create a dictionary that can be used in the templete to parse all information into a nice table
		The dicinatry looks like:
		SerchResults = {
			"Monthly" : {
				"total_phone" : ,
				"total_count" : ,
				"Zain" : {
					"total_phone" :
					"total_count" :
				},
				"Viva" : {
					"total_phone" :
					"total_count" :
				},
			},
			"Daily" : {
				1 : {
					"total_phone" :
					"total_count" :
				},
				2 : {
					"total_phone" :
					"total_count" :
				},
				...
			},
			"month" : month,
			"year" : year,
			"ReportType" : ReportType,
		}			
		'''
		AllReports = ReportsEntery.objects.filter(month=self.ReportModule.month,year=self.ReportModule.year,report_type=ReportType).exclude(day=0)
		TheFilter = {}
		SerchResults = { 
			"Monthly" : {
				"total_phone" : AllReports.all().aggregate(phone_count_sum=Sum("phone_count"))["phone_count_sum"],
				"total_count" : AllReports.all().aggregate(count_sum=Sum("count"))["count_sum"],
			},
			"month" : self.ReportModule.month,
			"year" : self.ReportModule.year,
			"ReportType" : ReportType,
		}
		if ReportType == "Port Out":
			ActOperator = "RecipientName"
		elif ReportType == "Port In":
			ActOperator = "DonorName"
		
		print("ActOperator = " + ActOperator)
		Operators = ("Zain", "Viva")
		for Operator in Operators:
			TheFilter = {
				ActOperator : Operator
			}
			SerchResults["Monthly"][Operator] = {
				"phone" : AllReports.filter(**TheFilter).aggregate(phone_count_sum=Sum("phone_count"))["phone_count_sum"],
				"count" : AllReports.filter(**TheFilter).aggregate(count_sum=Sum("count"))["count_sum"],
			}
		i = 1
		SerchResults["Daily"] = {}
		for report in AllReports:
			if ReportType == "Port Out":
				OperatorName = report.RecipientName
			elif ReportType == "Port In":
				OperatorName = report.DonorName
			
			if not SerchResults["Daily"].get(report.day):
				SerchResults["Daily"][report.day] = {}
			SerchResults["Daily"][report.day].update({
				OperatorName : { 
					"phone" : report.phone_count,
					"count" : report.count,
				},
			})

			#Find the daily total
			NewRep = AllReports
			try:
				DailyRep = NewRep.filter(day=i)
			except NewRep.MultipleObjectsReturned:
				dailyreports =  NewRep.objects.filter(day=i)
				DailyRep = dailyreports[0]
			if DailyRep and i in  SerchResults["Daily"]:
				SerchResults["Daily"][i]["total_phone"] = DailyRep.all().aggregate(phone_count_sum=Sum("phone_count"))["phone_count_sum"]
				SerchResults["Daily"][i]["total_count"] = DailyRep.all().aggregate(count_sum=Sum("count"))["count_sum"]
			i = i + 1
		
		# Find the total of findings = total days of the months, * 2 to show correctly in the web
		try:
			SerchResults["TotalDays"] = max(SerchResults["Daily"]) * 2
		except:
			pass
		return SerchResults


	def YearlyReports(self,ReportType):
		pass
	
	def ReportsFound(self,):
		try:
			ReportsEnteryFound = ReportsEntery.objects.get(day = self.ReportModule.day, month = self.ReportModule.month, year = self.ReportModule.year, report_duration = self.ReportModule.report_duration, report_type = self.ReportModule.report_type, RecipientName = self.ReportModule.RecipientName, DonorName = self.ReportModule.DonorName)
			return True
		except ReportsEntery.MultipleObjectsReturned:
			ReportsEnteryFound = ReportsEntery.objects.filter(day = self.ReportModule.day, month = self.ReportModule.month, year = self.ReportModule.year, report_duration = self.ReportModule.report_duration, report_type = self.ReportModule.report_type, RecipientName = self.ReportModule.RecipientName, DonorName = self.ReportModule.DonorName)
			ReportsEnteryFound = ReportsEnteryFound[0]
			return True
		except ReportsEntery.DoesNotExist:
			return False
	
	def PortOutReport(self,):
		otherOperators = ("Zain", "Viva")
		#print "all Operators: " + str(otherOperators)
		for Operator in otherOperators:
			#print "The Operator: " + Operator
			Filter = {
				"MessageTypeName" : "NPExecuteBroadcast",
				"DonorName" : GetConfig("COMPANY_NAME"),
				"RecipientName" : Operator,
				#"Originator" : "NPCS",
				#"status" : 10,
				#"message_created__year" : self.ReportModule.year,
			}
			#print "The filter: " + str(Filter)
			self.ReportModule.report_type = "Port Out"
			self.ReportModule.DonorName = GetConfig("COMPANY_NAME")
			self.ReportModule.RecipientName = Operator
			#print "All ReportModule: " + str(self.ReportModule)
			self.DailyReports(Filter)
			#return Filter

	def PortInReport(self,):
		otherOperators = ("Zain", "Viva")
		for Operator in otherOperators:
			Filter = {
				"MessageTypeName" : "NPExecuteBroadcast",
				"DonorName" : Operator,
				"RecipientName" : GetConfig("COMPANY_NAME"),
				#"Originator" : "NPG",
				#"status" : 10,
				#"message_created__year" : self.ReportModule.year,
			}
			self.ReportModule.report_type = "Port In"
			self.ReportModule.DonorName = Operator
			self.ReportModule.RecipientName = GetConfig("COMPANY_NAME")
			# this is added to refresh reports upone receiving nprequest brodcast
			if self.PerRequest:
				self.DailyReportsPerRequest(Filter)
			else:
				self.DailyReports(Filter)
			#return Filter

	def GenerateMissingReports(self,):
		pass

	def GenerateAllReports(self,):
		pass

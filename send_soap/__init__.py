from configuration import GetConfig, LogInput, GetRequest, LogMessageResutls
from suds import WebFault
from suds.client import Client
from suds.transport.http import HttpAuthenticated
from suds.transport.https import HttpTransport
from urllib2 import HTTPBasicAuthHandler, build_opener, install_opener, urlopen, BaseHandler
import traceback

from SNMP_app.alerts import send_alert, send_clear

import base64
#===============================================================================
# First main function
#===============================================================================
def SendToNPCS(message,MessageTypeID):
	MessageTypeName = GetRequest(int(MessageTypeID))	
	LogInput('INFO:    Sending message: ' + str(MessageTypeID) + " - " + LogMessageResutls(message),"SOAP","CRDB")
	
	try:
		LogInput('INFO:    Setting the client',"SOAP","CRDB")
		client = SetSudsClient()
		LogInput('INFO:    Client set ',"SOAP","CRDB")
		send_clear(9, 350)
	except:
		LogInput('ERROR:    Failed to set client ... ' +  str(traceback.format_exc()),"SOAP","CRDB")
		send_alert( 9 , 5 , 350 )
		
	
	LogInput('INFO:    The message is: ' + str(message),"SOAP","CRDB")
	Command = "client.service.Send" + MessageTypeName + "(**message)"
	LogInput('INFO:    The Command is: ' + Command,"SOAP","CRDB")

	try:
		result = eval(Command)
		LogInput('INFO:    The result is: ' + str(result),"SOAP","CRDB")
		LogInput("INFO:    Sent message: " + str(client.last_sent()) ,"SOAP","CRDB")
		LogInput("INFO:    Received message: " + str(client.last_received()) ,"SOAP","CRDB")
		send_clear(9, 350)
		return result
						
	except WebFault as detail:
		send_alert( 9 , 5 , 350 )
		LogInput('ERROR:    Suds WebFault Error: ',"SOAP","CRDB")
		
	except:
		send_alert( 9 , 5 , 350 )
		LogInput('ERROR:    Communication with the CS or CS is not responding right!!! ' +  str(traceback.format_exc()),"SOAP","CRDB")

def SetSudsClient():
	if GetConfig('SOAP_CONNECTION') == "LIVE":
		connctions = "CSL"
	elif GetConfig('SOAP_CONNECTION') == "TEST":
		connctions = "CST"
	userid = GetConfig(connctions).get('user')
	passwd = GetConfig(connctions).get('password')
	url = GetConfig(connctions).get('url')

	######################### Basic Authentication
	class HTTPSudsPreprocessor(BaseHandler):
		def http_request(self, req):
			req.add_header('Content-Type', 'text/xml; charset=utf-8')
			req.add_header('WWW-Authenticate', 'Basic realm="Control Panel"')
			#The below lines are to encode the credentials automatically
			cred=userid+':'+passwd
			if cred!=None:
				enc_cred=base64.encodestring(cred)
				req.add_header('Authorization', 'Basic '+ enc_cred.replace('\012',''))
			return req
		https_request = http_request
	
	http = HttpTransport()
	opener = build_opener(HTTPSudsPreprocessor)
	http.urlopener = opener
	
	# for Performance we cache the wsdl file and xsd
	#cache10 = client.options.cache
	#cache10.setduration(days=10)
	 
	######################### For  Basic Authentication #################################
	#client = Client(url, location=url.replace('?wsdl',''), transport=http, cache=None, timeout=90, faults=False, retxml=True)
	
	return Client(url, location=url.replace('?wsdl',''), transport=http, timeout=90, faults=False)
	#####################################################################################

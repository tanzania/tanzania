#
#
#		This file should be able to handle any outgoing message
#		it will receive the message to be sent out and the message that triggered this file
#		it will generate the soap message to be sent out
#		it will receive the response, store the sent out messages and the response in database
#		and make changes to the orginal message that triggered the process
#
#
from send_soap import SendToNPCS
from web.models import NpMessages , DonorRejectList, RoutingInfoList
from configuration import GetConfig, LogInput, GetRequest, LogMessageResutls
from django.core.files import File
from soap.np_process import NpProcess


import os

# this function will take the message to be sent out, and the message that trigger the sent out message as ActionMessage
def HandleOutgoingMessage(OutMessage, UserName):
	LogInput('Starting the process of handling the outgoing message',str(OutMessage.NPOrderID),"process")
	
	# this will generate and return the soap message
	SOAPMessage = GenerateSOAPMessage(OutMessage)
	LogInput('GenerateSOAPMessage',str(OutMessage.NPOrderID),"process")

	# this will send the soap message and return the respond
	Respond = SendSOAPMessage(SOAPMessage,OutMessage.MessageTypeID)
	LogInput('SendSOAPMessage',str(OutMessage.NPOrderID),"process")

	# this will generate the db message and return it, this is to check if it is NPRequest then use the orginal saved and update it	
	NewMessage = GenerateDBMessage(OutMessage, UserName)
	LogInput('GenerateDBMessage',str(OutMessage.NPOrderID),"process")
	
	# check the np order id if it is there
	#LogInput("Print NewMessage.NPOrderID: " + str(NewMessage.NPOrderID))

	# this will store the sent message and the respons, it will also return them
	MessageDB, RespondDB = StoreDBMessages(NewMessage, Respond)
	LogInput('StoreDBMessages',str(OutMessage.NPOrderID),"process")
	
	# This will update the action message that triggered this process
	NpProcess(MessageDB, RespondDB)
	#FinalRespond = UpdateMessage(MessageDB, RespondDB, ActionMessage)
	LogInput('NpProcess over',str(OutMessage.NPOrderID),"process")
	
	#return FinalRespond
	return Respond

def GenerateSOAPMessage(InMessage):
	# this will call the right the functions from below by the name of the message
	LogInput(str(InMessage),InMessage.NPOrderID,"process")
	'''
	# the NewRequestDict is a complete dict with all values available, this to be used for the soap message
	NewRequestDict = model_to_dict(instance, fields=[field.name for field in instance._meta.fields])
	
	# clear empty and non values from dic
	NewRequestDict = dict((k, v) for k,v in NewRequestDict.items() if v is not None and v != '')
						
	LogInput(str(NewRequestDict))
	LogInput(str(routing_formset))
	'''
	#if GetRequest(InMessage["MessageTypeID"]) == "NPRequest":
	#	GenMessage = eval(GetRequest(InMessage["MessageTypeID"]) + '(InMessage, ActionMessage)')
	
	#else:
	GenMessage = eval(GetRequest(InMessage.MessageTypeID) + '(InMessage)')
		
		
	return GenMessage
	
def SendSOAPMessage(InMessage,MessageTypeID):
	LogInput(str(InMessage))

	Respond = SendToNPCS(InMessage,MessageTypeID)
	
	return Respond
	
def GenerateDBMessage(InMessage, UserName):
	# find if the localid was already set or not, this is currently used for submitting new nprequest from web form
	#NewMessage = NpMessages(user = UserName, Originator = "NPG", **InMessage)
	InMessage.user = UserName
	InMessage.Originator = "NPG"

	return InMessage			


def StoreDBMessages(NewMessage, Respond):
												
	#LogInput(str(Respond))
												
	NewRespond = NpMessages()
	#LogInput("Updating message: " + str(NewMessage.MessageLocalID))

	LogInput(str(NewMessage))
	LogInput(str(Respond))
	
	if Respond[0] == 500:
		try:
			NewMessage.ack = "error"
			NewMessage.new_message = "NO"
			
			NewRespond.MessageTypeName = "ErrorNotification"
	
			#LogInput("printing -----------------> " + Respond[1]['ErrorNotification']['NpcdbRejectReason'])
			#NewRespond.MessageTypeName = 'ErrorNotification'
			
			try:
				# If NpOrderID is provided, add it to the response
				NewRespond.NpcdbRejectReason  = Respond[1][0]['NpcdbRejectReason']
			except:
				# Else, do nothing
				pass	
			try:
				# If NpOrderID is provided, add it to the response
				NewRespond.NpcdbRejectMessage  = Respond[1][0]['NpcdbRejectMessage']
			except:
				# Else, do nothing
				pass	
			try:
				# If NpOrderID is provided, add it to the response
				NewRespond.ErrorCode  = Respond[1][0]['ErrorCode']
			except:
				# Else, do nothing
				pass	
			try:
				# If NpOrderID is provided, add it to the response
				NewRespond.Description  = Respond[1][0]['Description']
			except:
				# Else, do nothing
				pass	
			'''
			for value, key in Respond[1]['ErrorNotification'].items():
				command = eval('NewRespond.' + key + ' = ' + value)
				LogInput(command)
			'''
			try:
				# If NpOrderID is provided, add it to the response
				NewRespond.NPOrderID = NewMessage.NPOrderID
			except:
				# Else, do nothing
				pass	
			try:
				# If NpOrderID is provided, add it to the response
				NewRespond.NPOrderID = NewMessage.NPOrderID = str(Respond[1]['ErrorNotification']['NPOrderID'])
			except:
				# Else, do nothing
				pass	
		except:
			LogInput("ERROR: returned 500 but not ErrorNotification : " + str(Respond[1]))

			#exit()
			
	elif Respond[0] == 200:
		NewRespond.MessageTypeName = 'MessageAck'
		NewMessage.ack = "ack"
		
		#try:
		try:
			if Respond[1]['MessageID']:
				NewRespond.MessageID = Respond[1]['MessageID']
		except:
			LogInput("MessageID doesn't exist in the Respond",NewMessage.NPOrderID,"process")
		try:
			if Respond[1]['NPOrderID']:
				NewRespond.NPOrderID = Respond[1]['NPOrderID']
				NewMessage.NPOrderID = NewRespond.NPOrderID
		except:
			LogInput("NPOrderID doesn't exist in the Respond",NewMessage.NPOrderID,"process")
		try:
			if Respond[1]['RecipientID']:
				NewRespond.RecipientID = Respond[1]['RecipientID']
		except:
			LogInput("RecipientID doesn't exist in the Respond",NewMessage.NPOrderID,"process")
		try:
			if Respond[1]['DonorID']:
				NewRespond.DonorID = Respond[1]['DonorID']
		except:
			LogInput("DonorID doesn't exist in the Respond",NewMessage.NPOrderID,"process")
		try:
			if Respond[1]['RangeHolderID']:
				NewRespond.RangeHolderID = Respond[1]['RangeHolderID']
		except:
			LogInput("RangeHolderID doesn't exist in the Respond",NewMessage.NPOrderID,"process")
		#except:
		#	pass
	#NewRespond.direction = "IN"
	
	#NewMessage.NPOrderID = int(NewRespond.NPOrderID)
	#LogInput("Updating message: " + str(NewMessage.MessageLocalID))
	
	#LogInput("NewMessage Contents: " + dir(NewMessage))
	
	NewMessage.save()
	
	NewRespond.related = NewMessage.MessageLocalID
	NewRespond.Originator = "NPCS"
	NewRespond.save()
	
	return NewMessage, NewRespond

###############################################################################
#  Below are the function to set as defult values for each outgoing message
# From here and below are the SOAP message DIC accourding to NPCS Spec
def NPRequest(message):

	RoutingInfoLists = RoutingInfoList.objects.filter(np_messages__pk = message.MessageLocalID)
	
	# This for is to get all the phone number ranges and place them inside the RoutingInfo List
	RoutingInfo = []	
	for Routing in RoutingInfoLists:
		RoutingInfo.append(
		{
			'PhoneNumberRange' : {
				'PhoneNumberStart' : Routing.PhoneNumberStart,
				'PhoneNumberEnd' : Routing.PhoneNumberEnd,
			},
			'RoutingNumber' : GetConfig('ROUTING_NUMBER'),
		})

	LogInput("RoutingInfo ==" + str(RoutingInfo))
	
	if message.EmergencyRestoreID:
		NewMessage = {
			'SenderID' : GetConfig('SENDER_ID'),
			'CustomerName' : message.CustomerName,
			'CustomerIDType' : message.CustomerIDType,
			'CustomerID' : message.CustomerID,
			'ServiceType' : "mobile",
			'CustomerType' : message.CustomerType,
			'EmergencyRestoreID' : message.EmergencyRestoreID,
			'SubscriptionType' : message.SubscriptionType,
			'RoutingInfoList'	: {'RoutingInfo' : RoutingInfo},
		}
	
	else:
		NewMessage = {
			'SenderID'				: GetConfig('SENDER_ID'),
			'CustomerName'		: message.CustomerName,
			'CustomerIDType'	: message.CustomerIDType,
			'CustomerID'			: message.CustomerID,
			'ServiceType'			: "mobile",
			'CustomerType'		: message.CustomerType,
			'RoutingInfoList'	: {'RoutingInfo' : RoutingInfo},
		}
	
		# Get the optinal fileds
		OptionalFileds = ("EmergencyRestoreID","BlockOrderID","BlockOrderCount","DateOfBirth","ValidationMSISDN","SubscriptionType","PortingTime")
	
		for item in OptionalFileds:
			LogInput("Testing optional item " + item)
			try:
				# check if the itme in the message is none http://effbot.org/zone/python-getattr.htm
				if getattr(message, item, None):
					command = "NewMessage['" + item + "'] = message." + item
					LogInput(command)
					exec command
			except Exception as error:
				LogInput("ERROR:  " + str(error))
				pass	

	return NewMessage


def NPRequestCancel(message):
	NewMessage = {
		'SenderID' : GetConfig('SENDER_ID'),
		'NPOrderID' : message.NPOrderID,
		'CancelReason' : "1",
	}

	# Get the optinal fileds
	OptionalFileds = ("CancelMessage")
	for item in OptionalFileds:
		try:
			if getattr(message, item, None):		
				command = "NewMessage['" + item + "'] = message." + item
				LogInput(command)
				exec command
		except:
			pass
	
	return NewMessage

def NPDonorReject(message):
	from web.models import DonorRejectList
	DonorRejectList = DonorRejectList.objects.filter(np_messages__pk = message.MessageLocalID)
	LogInput("def NPDonorReject(message): ")
	# This for is to get all the phone number ranges and place them inside the RoutingInfo List
	DonorReject = []	
	for Reject in DonorRejectList:
		DonorReject.append(
		{
			'DonorRejectReason' 	:Reject.DonorRejectReason,
			'DonorRejectMessage' 	:Reject.DonorRejectMessage,
			'RejectedPhoneNumberRange' : {
				'PhoneNumberStart'		:Reject.PhoneNumberStart,
				'PhoneNumberEnd'			:Reject.PhoneNumberEnd,
			}
		})

	LogInput("DonorReject ==" + str(DonorReject))

	NewMessage = {
		'SenderID' :GetConfig('SENDER_ID'),
		'NPOrderID' : message.NPOrderID,
		'DonorRejectList' : {'DonorReject' : DonorReject},
	}

	return NewMessage
      
def NPDonorAccept(message):
	NewMessage = {
		'SenderID'						:GetConfig('SENDER_ID'),
		'NPOrderID'						:message.NPOrderID,
	}

	return NewMessage


def NPActivated(message):
	NewMessage = {
		'SenderID'						:GetConfig('SENDER_ID'),
		'NPOrderID'						:message.NPOrderID,
	}

	return NewMessage


def NPDeactivated(message):
	NewMessage = {
		'SenderID'						:GetConfig('SENDER_ID'),
		'NPOrderID'						:message.NPOrderID,
	}

	return NewMessage


def NPReturn(message):
	PhoneNumberLists = RoutingInfoList.objects.filter(np_messages__pk = message.MessageLocalID)
	
	# This for is to get all the phone number ranges and place them inside the RoutingInfo List
	RoutingInfo = []	
	for PhoneNumber in PhoneNumberLists:
		RoutingInfo.append(
		{
			'PhoneNumberRange' : {
				'PhoneNumberStart' : PhoneNumber.PhoneNumberStart,
				'PhoneNumberEnd' : PhoneNumber.PhoneNumberEnd,
			},
		})

	LogInput("RoutingInfo ==" + str(RoutingInfo))

	NewMessage = {
		'SenderID'						:GetConfig('SENDER_ID'),
		'PhoneNumberList'	:RoutingInfo,
	}
	return NewMessage

def NPReturnCancel(message):
	NewMessage = {
		'SenderID'						:GetConfig('SENDER_ID'),
		'NPOrderID'						:message.NPOrderID,
	}

	return NewMessage


def NPRingFenceRequest(message):
	PhoneNumberLists = RoutingInfoList.objects.filter(np_messages__pk = message.MessageLocalID)
	
	# This for is to get all the phone number ranges and place them inside the RoutingInfo List
	RoutingInfo = []	
	for PhoneNumber in PhoneNumberLists:
		RoutingInfo.append(
		{
			'PhoneNumberRange' : {
				'PhoneNumberStart' : PhoneNumber.PhoneNumberStart,
				'PhoneNumberEnd' : PhoneNumber.PhoneNumberEnd,
			},
		})

	LogInput("RoutingInfo ==" + str(RoutingInfo))

	NewMessage = {
		'SenderID'					:GetConfig('SENDER_ID'),
		'PhoneNumberList'		:RoutingInfo,
	}
	return NewMessage


def NPRingFenceDeny(message):
	NewMessage = {
		'SenderID'						:GetConfig('SENDER_ID'),
		'NPOrderID'						:message.NPOrderID,
		'NpcdbRejectReason'		:message.NpcdbRejectReason,
		'DonorRejectMessage'	:message.DonorRejectMessage,
	}

	return NewMessage


def NPRingFenceApprove(message):
	NewMessage = {
		'SenderID'						:GetConfig('SENDER_ID'),
		'NPOrderID'						:message.NPOrderID,
		'AttachmentList' : {
			'Attachment' : []
		},
		'DonorRejectReason'		:message.DonorRejectReason,
		'DonorRejectMessage'	:message.DonorRejectMessage,
	}
	return NewMessage

def NPEmergencyRestore(message):
	NewMessage = {
		'SenderID' : GetConfig('SENDER_ID'),
		'OriginalNPOrderID' : message.OriginalNPOrderID,
	}
	return NewMessage

def NPFinalBillNotification(message):
	NewMessage = {
		'SenderID' : GetConfig('SENDER_ID'),
		'OriginalNPOrderID' : message.OriginalNPOrderID,
		'Balance' : message.Balance,
		'DueDate' : message.DueDate.strftime('%Y-%m-%d'),
	}

	return NewMessage

from web.models import NpMessages
from configuration import GetConfig, LogInput, LogMessageResutls, GetRequest
from int_osb.osb_proccess import OSB_int
from forker.tasks import stp_teller, InAcceptTask, InActivatedTask, InBroadcastTask, InRequestTask, IncomingRingFenceTask, RemoveRingFenceTask, InNPReturn
import datetime
from django.utils import timezone

import os
import traceback
import dateutil.parser


def NpProcess(NPMessage, NPResponse):
	'''
	This function will be called after receiving or sending any message to update the original order 
	information
	'''
	LogInput("NpProcess has been triggered")
	
	# if the message is ack
	if NPResponse.MessageTypeName == "MessageAck":
	
		# check if message has order id, it could only work on outgoin nprequest
		if NPMessage.NPOrderID:
			StatusUpdated = 0
			OrderID = NPMessage.NPOrderID
			LogInput("Processing the request with Order ID:  " + str(OrderID),str(NPMessage.NPOrderID),"process")
			NPOrder = NPMessage
			
			try:
				NPOrder = NpMessages.objects.get(NPOrderID=OrderID, MessageTypeName="NPRequest")
				LogInput("Found NPRequest  ")
	
				# update last message for the order
				NPOrder.last_message = GetRequest(NPMessage.MessageTypeID)
			# Np Request was not found, looking for NPEmergencyRestore
			except NpMessages.DoesNotExist:
				LogInput("Order was not found looking for NPEmergencyRestore  ",str(NPMessage.NPOrderID),"process")
				try:
					NPOrder = NpMessages.objects.get(NPOrderID=OrderID, MessageTypeName="NPEmergencyRestore")
					LogInput("Found NPEmergencyRestore  ",str(NPMessage.NPOrderID),"process")
		
					# update last message for the order
					NPOrder.last_message = GetRequest(NPMessage.MessageTypeID)
				# Np EmergencyRestore was not found, looking for NPRingFenceRequest
				except NpMessages.DoesNotExist:
					LogInput("Order was not found looking for NPRingFenceRequest  ",str(NPMessage.NPOrderID),"process")
					try:
						NPOrder = NpMessages.objects.get(NPOrderID=OrderID, MessageTypeName="NPRingFenceRequest")
						LogInput("Found NPRingFenceRequest  ",str(NPMessage.NPOrderID),"process")
			
						# update last message for the order
						NPOrder.last_message = GetRequest(NPMessage.MessageTypeID)
					# Np RingFenceRequest was not found, looking for NPRingFenceRequest
					except NpMessages.DoesNotExist:
						LogInput("Order was not found looking for FinalBillNotification  ",str(NPMessage.NPOrderID),"process")
						try:
							NPOrder = NpMessages.objects.get(NPOrderID=OrderID, MessageTypeName="FinalBillNotification")
							LogInput("Found FinalBillNotification  ",str(NPMessage.NPOrderID),"process")
				
							# update last message for the order
							NPOrder.last_message = GetRequest(NPMessage.MessageTypeID)
						except NpMessages.DoesNotExist:
							LogInput("Order was not found looking for NPExecuteBroadcast  ",str(NPMessage.NPOrderID),"process")
							try:
								NPOrder = NpMessages.objects.get(NPOrderID=OrderID, MessageTypeName="NPExecuteBroadcast")
								LogInput("Found NPExecuteBroadcast  ",str(NPMessage.NPOrderID),"process")
					
								# update last message for the order
								NPOrder.last_message = GetRequest(NPMessage.MessageTypeID)
							except NpMessages.DoesNotExist:
								LogInput("Order was not found looking for NPReturn  ",str(NPMessage.NPOrderID),"process")
								try:
									NPOrder = NpMessages.objects.get(NPOrderID=OrderID, MessageTypeName="NPReturn")
									LogInput("Found NPReturn  ",str(NPMessage.NPOrderID),"process")
						
									# update last message for the order
									NPOrder.last_message = GetRequest(NPMessage.MessageTypeID)
								except NpMessages.DoesNotExist:
									LogInput("Order was not found ... New Order  ",str(NPMessage.NPOrderID),"process")
								
			except NpMessages.MultipleObjectsReturned:
				LogInput("Request found multiple times.",str(NPMessage.NPOrderID),"process")
	
			except Exception as error:
				LogInput("ERROR: " + str(error),str(NPMessage.NPOrderID),"process")
		
			# if the message is getting sent from npg ################################# NPG ###########################
			if NPMessage.Originator == "NPG":
				if NPMessage.MessageTypeName == "NPRequest":
					NPGNPRequest(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPRequestCancel":
					NPGNPRequestCancel(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPDonorReject":
					NPGNPDonorReject(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPDonorAccept":
					NPGNPDonorAccept(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPReturn":
					NPGNPReturn(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPReturnCancel":
					NPGNPReturnCancel(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPActivated":
					NPGNPActivated(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPDeactivated":
					NPGNPDeactivated(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPRingFenceRequest":
					NPGNPRingFenceRequest(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPRingFenceDeny":
					NPGNPRingFenceDeny(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPRingFenceApprove":
					NPGNPRingFenceApprove(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPEmergencyRestore":
					NPGNPEmergencyRestore(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPEmergencyRestoreDeny":
					NPGNPEmergencyRestoreDeny(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPEmergencyRestoreApprove":
					NPGNPEmergencyRestoreApprove(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "FinalBillNotification":
					NPGFinalBillNotification(NPMessage,NPResponse,NPOrder)
			# if the message is received from NPCS #################################   NPCS   #
			elif NPMessage.Originator == "NPCS":
				if NPMessage.MessageTypeName == "NPRequest":
					CSNPRequest(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPRequestConfirmation":
					CSNPRequestConfirmation(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPRequestReject":
					CSNPRequestReject(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPRequestCancel":
					CSNPRequestCancel(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPDonorReject":
					CSNPDonorReject(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPDonorAccept":
					CSNPDonorAccept(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPExecuteCancel":
					CSNPExecuteCancel(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPExecuteBroadcast":
					CSNPExecuteBroadcast(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPReturnBroadcast":
					CSNPReturnBroadcast(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPActivated":
					CSNPActivated(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPRingFenceRequest":
					CSNPRingFenceRequest(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPRingFenceDeny":
					CSNPRingFenceDeny(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPRingFenceApprove":
					CSNPRingFenceApprove(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPEmergencyRestore":
					CSNPEmergencyRestore(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPEmergencyRestoreDeny":
					CSNPEmergencyRestoreDeny(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "NPEmergencyRestoreApprove":
					CSNPEmergencyRestoreApprove(NPMessage,NPResponse,NPOrder)
				elif NPMessage.MessageTypeName == "FinalBillNotification":
					CSFinalBillNotification(NPMessage,NPResponse,NPOrder)
			else:
				LogInput("ERROR: (np_process.py)  Message doesn't have Originator",str(NPMessage.NPOrderID),"process")
			
						
		# else message doen'st have order id
		else:
			LogInput("Message doesn't have NPOrderID",str(NPMessage.NPOrderID),"process")
	
	# message doesn't have a ack response
	else:
		NPMessage.ack = "error"
		
		if NPMessage.MessageTypeName == "NPReturn":
			NPMessage.status = 71
		elif NPMessage.MessageTypeName == "NPReturnCancel":
			NPMessage.status = 74
		elif NPMessage.MessageTypeName == "NPRingFenceRequest":
			NPMessage.status = 52
		elif NPMessage.MessageTypeName == "NPRequest":
			NPMessage.status = 2
		elif NPMessage.MessageTypeName == "NPEmergencyRestore":
			NPMessage.status = 43
			
		NPMessage.save(update_fields=['status','ack'])



def SaveOrder(StatusUpdated,NPOrder,NPMessage):
	# check if we should save or not 
	if StatusUpdated:
		NPMessage.ack = "ack"
		NPOrder.save(update_fields=['status','last_message','ack'])


def NPGNPRequest(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPRequest",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 0
	NPMessage.status = 1
	
	NPMessage.NPOrderID = NPResponse.NPOrderID
	NPMessage.last_message = GetRequest(NPMessage.MessageTypeID)
	
	# Update the operator information to the outgoing np request 
	NPMessage.RecipientID  =  NPResponse.RecipientID
	NPMessage.RangeHolderID = NPResponse.RangeHolderID
	NPMessage.DonorID =  NPResponse.DonorID
	
	NPMessage.save()

def NPGNPRequestCancel(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPRequestCancel",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 4
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPDonorReject(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPDonorReject",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 6
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPDonorAccept(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPDonorAccept",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 7
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPReturn(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPReturn",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 70
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPReturnCancel(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPReturnCancel",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 72
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPActivated(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPActivated",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 9
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPDeactivated(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPDeactivated",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 12
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPRingFenceRequest(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPRingFenceRequest",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 50
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPRingFenceDeny(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPRingFenceDeny",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 53
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPRingFenceApprove(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPRingFenceApprove",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 51
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPEmergencyRestore(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPEmergencyRestore",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 40
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPEmergencyRestoreDeny(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPEmergencyRestoreDeny",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 41
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGNPEmergencyRestoreApprove(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  NPEmergencyRestoreApprove",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 42
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def NPGFinalBillNotification(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPG  -----,  FinalBillNotification",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 13
	SaveOrder(StatusUpdated,NPOrder,NPMessage)

def CSFinalBillNotification(NPMessage,NPResponse,NPOrder):
	LogInput("INFO:   -----  From NPCS  -----,  FinalBillNotification",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 13
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPRequest(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPRequest",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	if NPMessage.EmergencyRestoreID:
		LogInput("INFO:   Incoming NPRequest with EmergencyRestoreID > " + str(NPMessage.EmergencyRestoreID),str(NPMessage.NPOrderID),"process")
		NPOrder.status = 40
	else:
		NPOrder.status = 5

		try:
			#ETA = timezone.make_aware(datetime.datetime.now(),timezone.get_default_timezone()) + datetime.timedelta(minutes = 0)
			ETA = timezone.make_aware(datetime.datetime.now(),timezone.get_default_timezone())
			#print ("the request will be proceed after: " + str(ETA))
			TaskResult = InRequestTask.apply_async(
				 (NPOrder, ), 
				eta=ETA, 
				retry=True, 
				retry_policy = {
					'max_retries': 3,
					'interval_start': 0,
			    'interval_step': 1,
			    #'interval_max': 1,
				}
			) 
			try:
				LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
			except:
				pass
		except:
			TaskResult = InRequestTask.delay(NPOrder)
			try:
				LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
			except:
				pass
			LogInput("ERROR:   NPRequest response 15 min timer didn't work:  " + str(traceback.format_exc()))
		
		
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPRequestConfirmation(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPRequestConfirmation",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 5
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPRequestReject(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPRequestReject",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 3
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPRequestCancel(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPRequestCancel",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 4
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPDonorReject(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPDonorReject",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 6
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPDonorAccept(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPDonorAccept",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 7
	if NPMessage.AutomaticAccept:
		LogInput("INFO  Auto Accept is set",str(NPMessage.NPOrderID),"process")
		NPOrder.status = 8
	
	# check if the incoming accept is coming for port in
	try:
		if NPOrder.RecipientName == GetConfig("COMPANY_NAME"):
			LogInput("INFO  Accepted port in request ",str(NPMessage.NPOrderID),"process")
			TaskResult = InAcceptTask.delay(NPOrder)
			try:
				LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
			except:
				pass
	except:
		LogInput("ERROR  Couldn't find the order ",str(NPMessage.NPOrderID),"process")
		
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPExecuteCancel(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPExecuteCancel",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 11
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPExecuteBroadcast(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPExecuteBroadcast",str(NPMessage.NPOrderID),"process")
	#LogInput("Starting NPExecuteBroadcast Process...",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	# this is for not related to our porting boradcasts, only between other operators
	try:
		NPOrder.status = 10
		# OSB Integration for Port IN NPExecuteBroadcast
	except Exception as error:
		# no need to update the orginal request as it doesn't exist
		StatusUpdated = 0
		LogInput("WARNING: NPExecuteBroadcast doesn't have a Request >>>> " + str(error),str(NPMessage.NPOrderID),"process")
	
	# BSCS Process - done below was repeted 
	#NewProcess = OSB_int(NPOrder)
	#NewProcess.PortInExecute
	#     STP  INTG     #
	'''
	LogInput("from STP_app.stp import Initializer")
	from STP_app.stp import Initializer
	LogInput("STP   Initializer")
	init = Initializer()
	
	LogInput("STP   Initializer DONE") 
	LogInput("STP   init.start")
	
	init.start(NPMessage)
	LogInput("STP   init.start DONE")
	'''
	# STP UPDATE PROCESS
	#stp_teller.delay(NPMessage)
	
	#Time = NPMessage.PortingTime 
	#T = datetime.datetime(Time)

	T = dateutil.parser.parse(NPMessage.PortingTime)

	#LogInput( str(T) ,str(NPMessage.NPOrderID),"process")

	ETA = datetime.datetime(
							year 	= T.year,
							month 	= T.month,
							day 	= T.day,
							hour 	= T.hour,	
							minute	= T.minute,
							tzinfo = dateutil.tz.tzlocal() )
	#LogInput('ETA: ' +str(ETA) ,str(NPMessage.NPOrderID),"process")
	LogInput("INFO:    STP Update...",str(NPMessage.NPOrderID),"process")
	try:
		#LogInput("STP   apply_async",str(NPMessage.NPOrderID),"process")

		TaskResult = stp_teller.apply_async(
			 (NPMessage, ), 
			eta=ETA, 
			retry=True, 
			retry_policy = {
				'max_retries': 3,
				'interval_start': 0,
		    'interval_step': 1,
		    #'interval_max': 1,
			}
		) 
		try:
			LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
		except:
			pass
		
		#LogInput("STP   apply_async DONE",str(NPMessage.NPOrderID),"process")
	
	except:

		LogInput("ERROR:    STP update with timer FAILED: " + str(traceback.format_exc()) ,str(NPMessage.NPOrderID),"process")
		LogInput("INFO:    STP force update", str(NPMessage.NPOrderID),"process")
		try:
			TaskResult = stp_teller.delay(NPMessage)
		except:
			LogInput("ERROR:    STP force update FAILED: " + str(traceback.format_exc()) ,str(NPMessage.NPOrderID),"process")
		#LogInput("STP   stp_teller.delay DONE",str(NPMessage.NPOrderID),"process")

		try:
			LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
		except:
			pass

	# Start OSB Process incoming NP activate process
	#LogInput("INFO:  Inside NPCS, NPExecuteBroadcast, before InBroadcastTask.delay",str(NPMessage.NPOrderID),"process")

	try:
		if NPOrder.MessageTypeName == "NPRequest" and NPOrder.Originator == "NPG":
			LogInput("INFO:    BSCS Update...",str(NPMessage.NPOrderID),"process")
			LogInput("INFO:    Porting IN request found, updating contract status",str(NPMessage.NPOrderID),"process")
			TaskResult = InBroadcastTask.apply_async(
				(NPMessage,), 
				eta=ETA, 
				retry=True, 
				retry_policy = {
					'max_retries': 3,
					'interval_start': 0,
					'interval_step': 1,
					#'interval_max': 1,
				}	
			)
			LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
			
			# This is a port in request, the next step is to update the report
			try:
				from reports.views import Reports
				x = Reports(PerRequest=True)
				x.PortInReport()
				x.MonthlyReports()
				LogInput("INFO:    Daily report updated successfully",str(NPMessage.NPOrderID),"process")
			except:
				LogInput("ERROR:    Unable to update daily report > " + str(traceback.format_exc()),str(NPMessage.NPOrderID),"process")
	except:
		LogInput("ERROR:    BSCS update FAILED: " + str(traceback.format_exc()) ,str(NPMessage.NPOrderID),"process")
	
	#NewProcess = OSB_int(NPOrder)
	#LogInput("INFO:  Inside NPCS, NPExecuteBroadcast, before activate",str(NPMessage.NPOrderID),"process")
	#NewProcess.PortInExecute()
	
	#LogInput("INFO:  Inside NPCS, NPExecuteBroadcast,  InBroadcastTask.delay over",str(NPMessage.NPOrderID),"process")
	#     OSB  INTG     #
	LogInput("INFO:    Updating NPG database" ,str(NPMessage.NPOrderID),"process")
	try:
		SaveOrder(StatusUpdated,NPOrder,NPMessage)
	except:
		LogInput("ERROR:    NPG database update FAILED: " + str(traceback.format_exc()) ,str(NPMessage.NPOrderID),"process")
	#LogInput("OVER NPExecuteBroadcast",str(NPMessage.NPOrderID),"process")
	
def CSNPReturnBroadcast(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPReturnBroadcast",str(NPMessage.NPOrderID),"process")
	try:
		StatusUpdated = 1
		if NPOrder.pk != NPMessage.pk:
			NPOrder.status = 73
	except:
		pass
	
	# Reimort the number to NMS, this function/task is for dealing with BSCS and NMS services
	try:
		TaskResult = InNPReturn.delay(NPMessage)
		try:
			LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
		except:
			pass
	except:
		pass
	
	'''
	LogInput("Starting NPReturnBroadcast Process...",str(NPMessage.NPOrderID),"process")
	LogInput("from STP_app.stp import Initializer",str(NPMessage.NPOrderID),"process")
	from STP_app.stp import Initializer
	LogInput("STP   Initializer",str(NPMessage.NPOrderID),"process")
	init = Initializer()
	
	LogInput("STP   Initializer DONE",str(NPMessage.NPOrderID),"process")
	LogInput("STP   init.start",str(NPMessage.NPOrderID),"process")
	
	init.start(NPMessage)
	LogInput("STP   init.start DONE",str(NPMessage.NPOrderID),"process")
	'''
	#stp_teller.delay(NPMessage)

	try:
		LogInput("STP   apply_async",str(NPMessage.NPOrderID),"process")
		#Time = NPMessage.PortingTime 
		#T = datetime.datetime(Time)

		T = dateutil.parser.parse(NPMessage.PortingTime)

		LogInput( str(T) ,str(NPMessage.NPOrderID),"process")

		ETA = datetime.datetime(
			year = T.year,
			month = T.month,
			day = T.day,
			hour 	= T.hour,	
			minute	= T.minute,
			tzinfo = dateutil.tz.tzlocal()
		)
		LogInput('ETA: ' +str(ETA) ,str(NPMessage.NPOrderID),"process")

		TaskResult = stp_teller.apply_async(
			(NPMessage), 
			eta = ETA, 
			retry = True, 
			retry_policy = {
				'max_retries': 3,
				'interval_start': 0,
				'interval_step': 1,
				#'interval_max': 1,
			}	
		)   
		try:
			LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
		except:
			pass
		LogInput("STP   apply_async DONE",str(NPMessage.NPOrderID),"process")
	
	except:
		LogInput("STP   apply_async FAILED",str(NPMessage.NPOrderID),"process")
		TaskResult = stp_teller.delay(NPMessage)
		try:
			LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
		except:
			pass
		LogInput("STP   stp_teller.delay DONE",str(NPMessage.NPOrderID),"process")

	LogInput("OVER NPReturnBroadcast",str(NPMessage.NPOrderID),"process")
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPActivated(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPActivated",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 9
	#     OSB  INTG     #
	# Start OSB Process incoming NP activate process
	LogInput("INFO:  Inside NPCS, NPActivated, before OSB_int",str(NPMessage.NPOrderID),"process")
	LogInput("INFO:  Inside NPCS, NPActivated, before activate",str(NPMessage.NPOrderID),"process")



	TaskResult = InActivatedTask.delay(NPOrder)
	try:
		LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
	except:
		pass

	#NewProcess = OSB_int(NPOrder)
	#NewProcess.IncomingNPActivate()
	LogInput("INFO:  Inside NPCS, NPActivated, activate over",str(NPMessage.NPOrderID),"process")
	#     OSB  INTG     #
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPRingFenceRequest(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPRingFenceRequest",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 50
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPRingFenceDeny(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPRingFenceDeny",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 53
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPRingFenceApprove(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPRingFenceApprove",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 51
	NPMessage.status = 51
	NPMessage.save()
	
	if NPOrder.MessageTypeName == "NPRingFenceApprove":
		try:
			TaskResult = IncomingRingFenceTask.delay(NPOrder)
			try:
				LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
			except:
				pass
			ETA = NPOrder.message_created + datetime.timedelta(minutes = 10)
			TaskResult = RemoveRingFenceTask.apply_async(
				(NPOrder, ), 
				eta=ETA, 
				retry=True, 
				retry_policy = {
					'max_retries': 3, 
					'interval_start': 0, 
					'interval_step': 1,
				}
			)
			try:
				LogInput("INFO:    %s task started with ID: %s" % (str(TaskResult.task_name),str(TaskResult.task_id)),str(NPMessage.NPOrderID),"process")
			except:
				pass
		except:
			#     OSB  INTG     #
			# Start OSB Process incoming NP activate process
			LogInput("ERROR:  RING Fence Task didn't work " + str(traceback.format_exc()),str(NPMessage.NPOrderID),"process")

	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPEmergencyRestore(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPEmergencyRestore",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 40
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPEmergencyRestoreDeny(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPEmergencyRestoreDeny",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 41
	SaveOrder(StatusUpdated,NPOrder,NPMessage)
	
def CSNPEmergencyRestoreApprove(NPMessage,NPResponse,NPOrder):
	LogInput("INFO ---- From NPCS ----,  NPEmergencyRestoreApprove",str(NPMessage.NPOrderID),"process")
	StatusUpdated = 1
	NPOrder.status = 42
	NPMessage.status = 42
	NPMessage.save()
	SaveOrder(StatusUpdated,NPOrder,NPMessage)

'''
	try:	
	except:

		now = datetime.datetime.now()

		year = "0" + str(now.year) if len(str(now.year)) == 1 else str(now.year)
		month = "0" + str(now.month) if len(str(now.month)) == 1 else str(now.month)
		day = "0" + str(now.day) if len(str(now.day)) == 1 else str(now.day)

		dir = GetConfig("ROOT_DIR") + GetConfig("DJANGO_DIR") + '/log/' + year + "/" + month + "/" + day + "/"
		
		if not os.path.exists(dir):
			os.makedirs(dir)

		FILE = open( dir + 'np_process_exception','a' )
		traceback.print_exc(file=FILE)		
'''		

#This was was generated by wsdl2py, try to not edit.
from soapbox import soap
from soapbox import xsd
from soapbox.xsd import UNBOUNDED




class NpcdbID(xsd.Long):
    pattern = r"[0-9]{14}"
    
class ServiceType(xsd.String):    
    enumeration = [ "mobile",  "fixed", ]
        
    
class SubscriptionType(xsd.String):    
    enumeration = [ "prepaid",  "postpaid", ]
        
    
class CustomerType(xsd.String):    
    enumeration = [ "consumer",  "business", ]
        
    
class CustomerIDType(xsd.Integer):
    minInclusive = r"1"
    maxInclusive = r"9"
    
class PhoneNumber(xsd.String):
    pattern = r"[0-9]{8}"
    
class RoutingNumber(xsd.String):
    pass
        
    
class BlockOrderID(xsd.String):
    pattern = r"[0-9]{14}"
    
class OperatorID(xsd.String):
    pattern = r"[0-9]{2}"
    
class Money(xsd.Decimal):
    pass
        
    
class AutomaticAccept(xsd.Integer):
    minInclusive = r"1"
    maxInclusive = r"2"
    




class Noop_Type(xsd.ComplexType):


class NoopResponse_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    Alive = xsd.Element(xsd.String)

    @classmethod
    def create(cls,Alive, ):
        instance = cls()
        instance.Alive = Alive
        return instance


class AccessFault_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    ErrorCode = xsd.Element(xsd.String, minOccurs=0)
    Description = xsd.Element(xsd.String, minOccurs=0)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class TechnicalFault_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    ErrorCode = xsd.Element(xsd.String, minOccurs=0)
    Description = xsd.Element(xsd.String, minOccurs=0)

    @classmethod
    def create(cls, ):
        instance = cls()
        return instance


class ErrorNotification_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    NpcdbRejectReason = xsd.Element(xsd.Integer)
    NpcdbRejectMessage = xsd.Element(xsd.String)
    NPOrderID = xsd.Element("NpcdbID", minOccurs=0)

    @classmethod
    def create(cls,NpcdbRejectReason,NpcdbRejectMessage, ):
        instance = cls()
        instance.NpcdbRejectReason = NpcdbRejectReason
        instance.NpcdbRejectMessage = NpcdbRejectMessage
        return instance


class PhoneNumberRange(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    PhoneNumberStart = xsd.Element("PhoneNumber")
    PhoneNumberEnd = xsd.Element("PhoneNumber")

    @classmethod
    def create(cls,PhoneNumberStart,PhoneNumberEnd, ):
        instance = cls()
        instance.PhoneNumberStart = PhoneNumberStart
        instance.PhoneNumberEnd = PhoneNumberEnd
        return instance


class RoutingInfoList(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    RoutingInfos = xsd.ListElement("RoutingInfo","RoutingInfo", maxOccurs=UNBOUNDED)

    @classmethod
    def create(cls,RoutingInfo, ):
        instance = cls()
        instance.RoutingInfo = RoutingInfo
        return instance


class RoutingInfo(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    PhoneNumberRange = xsd.Element("PhoneNumberRange")
    RoutingNumber = xsd.Element("RoutingNumber")

    @classmethod
    def create(cls,PhoneNumberRange,RoutingNumber, ):
        instance = cls()
        instance.PhoneNumberRange = PhoneNumberRange
        instance.RoutingNumber = RoutingNumber
        return instance


class PhoneNumberList(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    PhoneNumberRanges = xsd.ListElement("PhoneNumberRange","PhoneNumberRange", maxOccurs=UNBOUNDED)

    @classmethod
    def create(cls,PhoneNumberRange, ):
        instance = cls()
        instance.PhoneNumberRange = PhoneNumberRange
        return instance


class DonorReject(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    DonorRejectReason = xsd.Element(xsd.Integer)
    DonorRejectMessage = xsd.Element(xsd.String)
    RejectedPhoneNumberRange = xsd.Element("PhoneNumberRange")

    @classmethod
    def create(cls,DonorRejectReason,DonorRejectMessage,RejectedPhoneNumberRange, ):
        instance = cls()
        instance.DonorRejectReason = DonorRejectReason
        instance.DonorRejectMessage = DonorRejectMessage
        instance.RejectedPhoneNumberRange = RejectedPhoneNumberRange
        return instance


class DonorRejectList(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    DonorRejects = xsd.ListElement("DonorReject","DonorReject", maxOccurs=UNBOUNDED)

    @classmethod
    def create(cls,DonorReject, ):
        instance = cls()
        instance.DonorReject = DonorReject
        return instance



Schema8d8ea = xsd.Schema(
    name = "ns8d8ea",
    imports = [],
    targetNamespace = "http://np.systor.st/commontypes",
    location = "/4gtss/mount/site/npg_hazim/soap/wsdl/NPKW_WSDL_2013-02-15/nptypes.xsd",
    elementFormDefault = "qualified",
    simpleTypes = [ NpcdbID, ServiceType, SubscriptionType, CustomerType, CustomerIDType, PhoneNumber, RoutingNumber, BlockOrderID, OperatorID, Money, AutomaticAccept,],
    attributeGroups = [],
    groups = [],
    complexTypes = [ Noop_Type, NoopResponse_Type, AccessFault_Type, TechnicalFault_Type, ErrorNotification_Type, PhoneNumberRange, RoutingInfoList, RoutingInfo, PhoneNumberList, DonorReject, DonorRejectList,],
    elements = { })






class SpMessageAck_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    NPOrderID = xsd.Element("NpcdbID")

    @classmethod
    def create(cls,NPOrderID, ):
        instance = cls()
        instance.NPOrderID = NPOrderID
        return instance


class NPRequestToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element("NpcdbID")
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")
    EmergencyRestoreID = xsd.Element("NpcdbID", minOccurs=0)
    BlockOrderID = xsd.Element("BlockOrderID", minOccurs=0)
    BlockOrderCount = xsd.Element(xsd.Integer, minOccurs=0)
    ValidationMSISDN = xsd.Element("PhoneNumber", minOccurs=0)
    RecipientID = xsd.Element("OperatorID")
    DonorID = xsd.Element("OperatorID")
    CustomerName = xsd.Element(xsd.String)
    CustomerIDType = xsd.Element("CustomerIDType")
    CustomerID = xsd.Element(xsd.String)
    DateOfBirth = xsd.Element(xsd.Date, minOccurs=0)
    ServiceType = xsd.Element("ServiceType")
    CustomerType = xsd.Element("CustomerType")
    SubscriptionType = xsd.Element("SubscriptionType", minOccurs=0)
    RoutingInfoList = xsd.Element("RoutingInfoList")
    PortingTime = xsd.Element(xsd.DateTime, minOccurs=0)

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID,RecipientID,DonorID,CustomerName,CustomerIDType,CustomerID,ServiceType,CustomerType,RoutingInfoList, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        instance.RecipientID = RecipientID
        instance.DonorID = DonorID
        instance.CustomerName = CustomerName
        instance.CustomerIDType = CustomerIDType
        instance.CustomerID = CustomerID
        instance.ServiceType = ServiceType
        instance.CustomerType = CustomerType
        instance.RoutingInfoList = RoutingInfoList
        return instance


class NPRequestRejectToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element("NpcdbID")
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")
    NpcdbRejectReason = xsd.Element(xsd.Integer)
    NpcdbRejectMessage = xsd.Element(xsd.String, minOccurs=0)

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID,NpcdbRejectReason, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        instance.NpcdbRejectReason = NpcdbRejectReason
        return instance


class NPRequestConfirmationToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element("NpcdbID")
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        return instance


class NPRequestCancelToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element("NpcdbID")
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")
    CancelReason = xsd.Element(xsd.Integer)
    CancelMessage = xsd.Element(xsd.String, minOccurs=0)

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID,CancelReason, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        instance.CancelReason = CancelReason
        return instance


class NPDonorRejectToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element("NpcdbID")
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")
    DonorRejectList = xsd.Element("DonorRejectList")

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID,DonorRejectList, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        instance.DonorRejectList = DonorRejectList
        return instance


class NPDonorAcceptToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element("NpcdbID")
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")
    AutomaticAccept = xsd.Element("AutomaticAccept")

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID,AutomaticAccept, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        instance.AutomaticAccept = AutomaticAccept
        return instance


class NPActivatedToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element(xsd.Long)
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        return instance


class NPDeactivatedToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element(xsd.Long)
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        return instance


class NPExecuteBroadcastToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element(xsd.Long)
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")
    RecipientID = xsd.Element("OperatorID")
    DonorID = xsd.Element("OperatorID")
    RoutingInfoList = xsd.Element("RoutingInfoList")
    PortingTime = xsd.Element(xsd.DateTime)

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID,RecipientID,DonorID,RoutingInfoList,PortingTime, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        instance.RecipientID = RecipientID
        instance.DonorID = DonorID
        instance.RoutingInfoList = RoutingInfoList
        instance.PortingTime = PortingTime
        return instance


class NPExecuteCancelToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element(xsd.Long)
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")
    CancelReason = xsd.Element(xsd.Integer)
    CancelMessage = xsd.Element(xsd.String, minOccurs=0)

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID,CancelReason, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        instance.CancelReason = CancelReason
        return instance


class NPReturnBroadcastToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element(xsd.Long)
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")
    SubscriptionProviderID = xsd.Element("OperatorID")
    RangeHolderID = xsd.Element("OperatorID")
    PhoneNumberList = xsd.Element("PhoneNumberList")

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID,SubscriptionProviderID,RangeHolderID,PhoneNumberList, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        instance.SubscriptionProviderID = SubscriptionProviderID
        instance.RangeHolderID = RangeHolderID
        instance.PhoneNumberList = PhoneNumberList
        return instance


class NPRingFenceDenyToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element(xsd.Long)
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")
    NpcdbRejectReason = xsd.Element(xsd.Integer)
    NpcdbRejectMessage = xsd.Element(xsd.String)

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID,NpcdbRejectReason,NpcdbRejectMessage, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        instance.NpcdbRejectReason = NpcdbRejectReason
        instance.NpcdbRejectMessage = NpcdbRejectMessage
        return instance


class NPRingFenceApproveToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element(xsd.Long)
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        return instance


class NPEmergencyRestoreDenyToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element(xsd.Long)
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")
    NpcdbRejectReason = xsd.Element(xsd.Integer)
    NpcdbRejectMessage = xsd.Element(xsd.String)

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID,NpcdbRejectReason,NpcdbRejectMessage, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        instance.NpcdbRejectReason = NpcdbRejectReason
        instance.NpcdbRejectMessage = NpcdbRejectMessage
        return instance


class NPEmergencyRestoreApproveToSp_Type(xsd.ComplexType):
    INHERITANCE = None
    INDICATOR = xsd.Sequence
    MessageID = xsd.Element(xsd.Long)
    SenderID = xsd.Element("OperatorID")
    NPOrderID = xsd.Element("NpcdbID")
    EmergencyRestoreID = xsd.Element("NpcdbID")

    @classmethod
    def create(cls,MessageID,SenderID,NPOrderID,EmergencyRestoreID, ):
        instance = cls()
        instance.MessageID = MessageID
        instance.SenderID = SenderID
        instance.NPOrderID = NPOrderID
        instance.EmergencyRestoreID = EmergencyRestoreID
        return instance



Schemaa8fc8 = xsd.Schema(
    name = "nsa8fc8",
    imports = [Schema8d8ea,],
    targetNamespace = "http://np.systor.st/sp",
    elementFormDefault = "qualified",
    simpleTypes = [],
    attributeGroups = [],
    groups = [],
    complexTypes = [ SpMessageAck_Type, NPRequestToSp_Type, NPRequestRejectToSp_Type, NPRequestConfirmationToSp_Type, NPRequestCancelToSp_Type, NPDonorRejectToSp_Type, NPDonorAcceptToSp_Type, NPActivatedToSp_Type, NPDeactivatedToSp_Type, NPExecuteBroadcastToSp_Type, NPExecuteCancelToSp_Type, NPReturnBroadcastToSp_Type, NPRingFenceDenyToSp_Type, NPRingFenceApproveToSp_Type, NPEmergencyRestoreDenyToSp_Type, NPEmergencyRestoreApproveToSp_Type,],
    elements = {  "Noop":xsd.Element("Noop_Type"), "NoopResponse":xsd.Element("NoopResponse_Type"), "MessageAck":xsd.Element("SpMessageAck_Type"), "AccessFault":xsd.Element("AccessFault_Type"), "TechnicalFault":xsd.Element("TechnicalFault_Type"), "ErrorNotification":xsd.Element("ErrorNotification_Type"), "NPRequest":xsd.Element("NPRequestToSp_Type"), "NPRequestReject":xsd.Element("NPRequestRejectToSp_Type"), "NPRequestConfirmation":xsd.Element("NPRequestConfirmationToSp_Type"), "NPRequestCancel":xsd.Element("NPRequestCancelToSp_Type"), "NPDonorReject":xsd.Element("NPDonorRejectToSp_Type"), "NPDonorAccept":xsd.Element("NPDonorAcceptToSp_Type"), "NPActivated":xsd.Element("NPActivatedToSp_Type"), "NPDeactivated":xsd.Element("NPDeactivatedToSp_Type"), "NPExecuteBroadcast":xsd.Element("NPExecuteBroadcastToSp_Type"), "NPExecuteCancel":xsd.Element("NPExecuteCancelToSp_Type"), "NPReturnBroadcast":xsd.Element("NPReturnBroadcastToSp_Type"), "NPRingFenceDeny":xsd.Element("NPRingFenceDenyToSp_Type"), "NPRingFenceApprove":xsd.Element("NPRingFenceApproveToSp_Type"), "NPEmergencyRestoreDeny":xsd.Element("NPEmergencyRestoreDenyToSp_Type"), "NPEmergencyRestoreApprove":xsd.Element("NPEmergencyRestoreApproveToSp_Type"),})




Noop_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/Noop",  
    input = "Noop",#Pointer to Schema.elements
    inputPartName = "noop",  
    output = "NoopResponse",#Pointer to Schema.elements
    outputPartName = "noopResp",
    operationName = "Noop",
    faults = [
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleErrorNotification_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/ErrorNotification",  
    input = "ErrorNotification",#Pointer to Schema.elements
    inputPartName = "error",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleErrorNotification",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPRequest_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPRequest",  
    input = "NPRequest",#Pointer to Schema.elements
    inputPartName = "npReq",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPRequest",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPRequestReject_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPRequestReject",  
    input = "NPRequestReject",#Pointer to Schema.elements
    inputPartName = "npReqRej",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPRequestReject",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPRequestConfirmation_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPRequestConfirmation",  
    input = "NPRequestConfirmation",#Pointer to Schema.elements
    inputPartName = "npReqConf",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPRequestConfirmation",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPRequestCancel_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPRequestCancel",  
    input = "NPRequestCancel",#Pointer to Schema.elements
    inputPartName = "npCan",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPRequestCancel",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPDonorReject_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPDonorReject",  
    input = "NPDonorReject",#Pointer to Schema.elements
    inputPartName = "npRej",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPDonorReject",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPDonorAccept_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPDonorAccept",  
    input = "NPDonorAccept",#Pointer to Schema.elements
    inputPartName = "npAcc",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPDonorAccept",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPActivated_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPActivated",  
    input = "NPActivated",#Pointer to Schema.elements
    inputPartName = "npAct",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPActivated",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPDeactivated_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPDeactivated",  
    input = "NPDeactivated",#Pointer to Schema.elements
    inputPartName = "npDeact",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPDeactivated",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPExecuteBroadcast_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPExecuteBroadcast",  
    input = "NPExecuteBroadcast",#Pointer to Schema.elements
    inputPartName = "npExecBrcast",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPExecuteBroadcast",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPExecuteCancel_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPExecuteCancel",  
    input = "NPExecuteCancel",#Pointer to Schema.elements
    inputPartName = "npExecCan",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPExecuteCancel",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPReturnBroadcast_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPReturnBroadcast",  
    input = "NPReturnBroadcast",#Pointer to Schema.elements
    inputPartName = "npRetBrcast",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPReturnBroadcast",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPRingFenceDeny_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPRingFenceDeny",  
    input = "NPRingFenceDeny",#Pointer to Schema.elements
    inputPartName = "npRingFenceDeny",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPRingFenceDeny",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPRingFenceApprove_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPRingFenceApprove",  
    input = "NPRingFenceApprove",#Pointer to Schema.elements
    inputPartName = "npRingFenceApprove",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPRingFenceApprove",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPEmergencyRestoreDeny_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPEmergencyRestoreDeny",  
    input = "NPEmergencyRestoreDeny",#Pointer to Schema.elements
    inputPartName = "npEmergRestDeny",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPEmergencyRestoreDeny",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    




HandleNPEmergencyRestoreApprove_method = xsd.Method(
    soapAction = "http://np.systor.st/sp/SpPort/NPEmergencyRestoreApprove",  
    input = "NPEmergencyRestoreApprove",#Pointer to Schema.elements
    inputPartName = "npEmergRestApp",  
    output = "MessageAck",#Pointer to Schema.elements
    outputPartName = "ack",
    operationName = "HandleNPEmergencyRestoreApprove",
    faults = [
    xsd.Response(name="SpAccessFault",element="AccessFault",partName="accessFault"),
    xsd.Response(name="SpTechnicalFault",element="TechnicalFault",partName="technicalFault"),]
    )
    

    
SpSoap_SERVICE = soap.Service(
    name = "SpSoap",
    targetNamespace = "http://np.systor.st/sp",
    location = "http://m2m.nport.kw/services/SpService",
    schema = Schemaa8fc8,
    version = soap.SOAPVersion.SOAP11, 
    methods = [Noop_method,HandleErrorNotification_method,HandleNPRequest_method,HandleNPRequestReject_method,HandleNPRequestConfirmation_method,HandleNPRequestCancel_method,HandleNPDonorReject_method,HandleNPDonorAccept_method,HandleNPActivated_method,HandleNPDeactivated_method,HandleNPExecuteBroadcast_method,HandleNPExecuteCancel_method,HandleNPReturnBroadcast_method,HandleNPRingFenceDeny_method,HandleNPRingFenceApprove_method,HandleNPEmergencyRestoreDeny_method,HandleNPEmergencyRestoreApprove_method, ])
 

class SpSoapServiceStub(soap.Stub):
    SERVICE = SpSoap_SERVICE
    
    
    def Noop(self, Noop):
        return self.call("Noop", Noop)
    
    def HandleErrorNotification(self, ErrorNotification):
        return self.call("HandleErrorNotification", ErrorNotification)
    
    def HandleNPRequest(self, NPRequest):
        return self.call("HandleNPRequest", NPRequest)
    
    def HandleNPRequestReject(self, NPRequestReject):
        return self.call("HandleNPRequestReject", NPRequestReject)
    
    def HandleNPRequestConfirmation(self, NPRequestConfirmation):
        return self.call("HandleNPRequestConfirmation", NPRequestConfirmation)
    
    def HandleNPRequestCancel(self, NPRequestCancel):
        return self.call("HandleNPRequestCancel", NPRequestCancel)
    
    def HandleNPDonorReject(self, NPDonorReject):
        return self.call("HandleNPDonorReject", NPDonorReject)
    
    def HandleNPDonorAccept(self, NPDonorAccept):
        return self.call("HandleNPDonorAccept", NPDonorAccept)
    
    def HandleNPActivated(self, NPActivated):
        return self.call("HandleNPActivated", NPActivated)
    
    def HandleNPDeactivated(self, NPDeactivated):
        return self.call("HandleNPDeactivated", NPDeactivated)
    
    def HandleNPExecuteBroadcast(self, NPExecuteBroadcast):
        return self.call("HandleNPExecuteBroadcast", NPExecuteBroadcast)
    
    def HandleNPExecuteCancel(self, NPExecuteCancel):
        return self.call("HandleNPExecuteCancel", NPExecuteCancel)
    
    def HandleNPReturnBroadcast(self, NPReturnBroadcast):
        return self.call("HandleNPReturnBroadcast", NPReturnBroadcast)
    
    def HandleNPRingFenceDeny(self, NPRingFenceDeny):
        return self.call("HandleNPRingFenceDeny", NPRingFenceDeny)
    
    def HandleNPRingFenceApprove(self, NPRingFenceApprove):
        return self.call("HandleNPRingFenceApprove", NPRingFenceApprove)
    
    def HandleNPEmergencyRestoreDeny(self, NPEmergencyRestoreDeny):
        return self.call("HandleNPEmergencyRestoreDeny", NPEmergencyRestoreDeny)
    
    def HandleNPEmergencyRestoreApprove(self, NPEmergencyRestoreApprove):
        return self.call("HandleNPEmergencyRestoreApprove", NPEmergencyRestoreApprove)
     
 
 
 

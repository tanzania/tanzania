from django.db import models



class timer(models.Model):
	
	Type  	 = models.CharField(max_length=3, blank=False, null=False) 
	Days  	 = models.SmallIntegerField(max_length=3, blank=True, null=True)
	Hours    = models.SmallIntegerField(max_length=3, blank=True, null=True)
	Minutes  = models.SmallIntegerField(max_length=3, blank=True, null=True)
	Flag     = models.BooleanField(default=True)  ## True means consider holidays 

	class Meta:
		db_table = "timer"


class holidays(models.Model):

	Date 	= models.DateTimeField(editable=False)
	Length 	= models.SmallIntegerField(default=1, max_length=2, blank=False, null=False)

	class Meta:
		db_table = "holidays"
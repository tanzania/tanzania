import celery
from celery.task.schedules import crontab 
from celery.decorators import periodic_task , task
from STP_app.stp import Initializer, Failed_Retry

from time import sleep

import datetime

# @task( 
		# ignore_result = True, 
		# max_retries = None, 
		# default_retry_delay = 5 * 60
		# run_every = timedelta(seconds = 50) ## For periodic tasks
		# routing_key = 'foo.bars'
		# )

@celery.task(queue="smarttime")
def add(x, y):
	print 'Background task is running now. Time now is: ' + str( datetime.datetime.now() )
	a = x + y
	print a
	return a 


# ## time limit overrides default retry delay
# @task(default_retry_delay=5, max_retries = 2, queue="lineup", time_limit=10 )
# def stp_teller(inmessage):
# 	try:
# 		print 'Inside STP Teller'
# 		init = Initializer()
# 		init.start(inmessage)
# 	except Exception, exc:
# 		stp_teller.retry( exc=exc, countdown=5 )


# @periodic_task(run_every=crontab() , queue="lineup" , time_limit=10, )
# def retrier():
# 	try:
# 		print 'Retrier is working on failed numbers ...'
# 		FR = Failed_Retry() 
# 		FR.failed_retry()
# 	except Exception, exc:	
# 		retrier.retry(exc=exc, countdown=5,)  



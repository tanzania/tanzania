# Better way to handle addying holidays and considering time as well without taking soo much time

from datetime import timedelta
from datetime import datetime
#from datetime.datetime import date
from dateutil import rrule
from dateutil.rrule import MO, TU, WE, TH, FR, SA, SU
from dateutil.relativedelta import relativedelta
#####################################
from configuration import LogInput, GetRequest
######## Models ####################
from models import timer, holidays

workhour_start	= 8   ## 8 AM
workhour_end	= 17  ## 5 PM



def datty(intime= datetime.now(), timer_type=None, flag=None):

	LogInput("----------------New Datty Start -------------------","TIMER","timer")

	LogInput("Inside timer module ..datty : ","TIMER","timer")

	LogInput("In time: " + str(intime),"TIMER","timer")

	LogInput("Timer Type: " + str(timer_type),"TIMER","timer")

	#####################  Holidays  ############################
	from models import timer, holidays

	## Getting the holidays from the DB
	HOLIDAYS = holidays.objects.all()

	holidays_items = [ (holy.Date,holy.Length) for holy in HOLIDAYS ]

	LogInput("Holiday Items: " + str(holidays_items),"TIMER","timer")

	holidays=[]

	for holy in holidays_items:
		for single_day in range(holy[1]):
			holidays.append(holy[0] + timedelta(days=single_day))

	LogInput("HOlidays: " + str(holidays),"TIMER","timer")		

	############################################################
	
	############# Calculating weekends and holidays #############
	Working_Days = [MO, TU, WE, TH, SU,]

	weekends_margin = 7 - len(Working_Days)
	holidays_margin = len(holidays)
	#############################################################


	## Getting this field from the DB
	## time offset is the timer offset
	time_offset = timer.objects.get(Type=timer_type)


	day_offset = time_offset.Days

	LogInput("Days offset " + str(day_offset),"TIMER","timer")


	start_date = intime
	end_date   = intime + relativedelta(days=+( 	day_offset		 + 
                                                    weekends_margin  + 
                                                    holidays_margin
                                               ),
                                        ) 

	LogInput("Start Date: " + str(start_date),"TIMER","timer")
	LogInput("end_date Date: " + str(end_date),"TIMER","timer")

	r = rrule.rrule(rrule.DAILY,byweekday=Working_Days, dtstart=datetime.date(start_date), until=datetime.date(end_date) )


	set = rrule.rruleset()
	set.rrule(r)


	# Add holidays as exclusion days
	for exdate in holidays:
	    set.exdate(exdate)

	## Adding the now time +  time offset  
	
	LogInput("time before modification: " + str(set[-1]),"TIMER","timer")

	execution_time = set[-1] + ( timedelta(hours=intime.hour) + timedelta(hours=time_offset.Hours) ) + ( timedelta(minutes=intime.minute) + timedelta(minutes=time_offset.Minutes) ) + ( timedelta(seconds=intime.second) )
	
	LogInput("time after modification : " + str(execution_time),"TIMER","timer")

	## If outside working hours
	if execution_time.hour not in range( workhour_start , workhour_end+1 ):
		
		## if execution time is less than workhour start
		if execution_time.hour < workhour_start:
			execution_time = execution_time + timedelta( hours=(workhour_start-execution_time.hour) )

		elif execution_time.hour > workhour_start:
			execution_time = datetime( year=execution_time.year 	, 
									   month=execution_time.month	,
									   day=execution_time.day + 1	, ## We add 1 to make it next day
									   hour=workhour_start
									   )


	return execution_time
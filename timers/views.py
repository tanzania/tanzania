from django.http import HttpResponse
from django.shortcuts import render_to_response
from timers.tasks import add
from timers.timer import datty

from datetime import datetime

def index(request):

	## Defaults --> intime=now, timer_type=none, flag=none
	ex_time = datty(timer_type='T2' )


	result = add.apply_async(args=[2,3],
							 eta=ex_time
							 )

	ret = result.result 

	return HttpResponse('Return Value: '+str(ret) + ' ' + str(ex_time))


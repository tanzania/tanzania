from django.db import models

# Create your models here.
from django.contrib.auth.models import User

class UserCustomeProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    dealer_id = models.CharField(max_length=50)
    
    class Meta:
		db_table = "np_user_profile"

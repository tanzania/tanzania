from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib import admin
from django import forms
from django.contrib.auth.forms import UserCreationForm,UserChangeForm
from authenticate.models import UserProfile

admin.site.unregister(User)
class CustomUserCreationForm(UserCreationForm):
    userId = forms.CharField(label = "Dealer ID",required=False)

    class Meta:
        model = User
        fields = ( 'username', 'email', 'password1', 'password2', 'userId',)

    def save(self, commit=True):
	user = super(CustomUserCreationForm, self).save(commit=False)
	user.save()
	user_profile = UserProfile(user=user, admin=False, is_first_login=True,dealer_id=self.cleaned_data['userId'])
	user_profile.save()
		
        return user

class CustomUserChangeForm(UserChangeForm):
	userId = forms.CharField(label = "Dealer ID",required=False)
	def __init__( self, *args, **kwargs ):
	        super(CustomUserChangeForm, self ).__init__( *args, **kwargs )
        	if self.instance and self.instance.pk:
			# Since the pk is set this is not a new instance
            		self.fields['userId'].initial  =   UserProfile.objects.get_or_create(user = self.instance)[0].dealer_id
	def save(self, commit=True):
        	user = super(CustomUserChangeForm, self).save(commit)
		profile = user.get_profile()
		profile.dealer_id = self.cleaned_data['userId']
		profile.save()

		return user
		
	class Meta:
        	model = User
		fields =  ('username','first_name','last_name','email','is_active','is_staff','is_superuser','groups','user_permissions','last_login','date_joined', 'userId',)


class MyUserAdmin(UserAdmin):
        fieldsets = (
        	(None, {'fields': ('username','email','userId', 'password')}),
        	('Personal info', {'fields': ('first_name', 'last_name')}),
        	('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        	('Important dates', {'fields': ('last_login', 'date_joined')}),
    	) 
	add_fieldsets = (
                (None, {
                'classes': ('wide',),
                 'fields': ('username', 'email', 'password1', 'password2', 'userId',)}
                ),
        )
	add_form = CustomUserCreationForm
	form =CustomUserChangeForm
	#inlines = [ProfileInline,]
admin.site.register(User, MyUserAdmin)

from models import NpMessages, RoutingInfoList, DonorRejectList
from django import forms
from django.forms import ModelForm, Textarea
from django.forms.models import inlineformset_factory
from django import forms
from django.contrib.admin.widgets import AdminFileWidget
from django.forms import extras
from django.contrib.auth.models import User
from configuration import GetStatus


from datetime import datetime, date, timedelta
from npg import settings

class FilterForm(forms.Form):
	
	STATUS_CHOICES = [('', 'All Status')]
	
	for key, status in GetStatus("StatusDict").iteritems():
		if key in range(1,20):
			STATUS_CHOICES.append((str(key),status[1]))
	
	USER_CHOICES = [("","All Agent")]
	AllUsers = User.objects.all().order_by('first_name')
	for user in AllUsers:
		USER_CHOICES.append((user.username,'%s %s - %s' % (user.first_name,user.last_name,user.username)))
		
	Status = forms.ChoiceField(
		label = "Status", 
		choices = STATUS_CHOICES,
		initial = 'ALL', 
		widget = forms.Select(),
		required=False,
	)

	User = forms.ChoiceField(
		label = "Agent Name", 
		choices = USER_CHOICES,
		#initial = '0', 
		widget = forms.Select(),
		required=False,
	)
	
	PhoneNumber = forms.CharField(
		label='Phone Number', 
		#initial = "%s 03:00:00"%str(date.today()+timedelta(days=10)),
		#format = settings.DATETIME_INPUT_FORMATS,
		#widget = forms.TextInput(attrs={'readonly':'readonly'})
		required=False,
		max_length = 8,
		#widget=forms.TextInput(attrs={'maxlength':'8'}),
	)

	NPOrderID = forms.CharField(
		label='Order ID', 
		#initial = "%s 03:00:00"%str(date.today()+timedelta(days=10)),
		#format = settings.DATETIME_INPUT_FORMATS,
		#widget = forms.TextInput(attrs={'readonly':'readonly'})
		required=False,
	)

	
	class Meta:
		model = NpMessages
		fields = (
			'NPOrderID',
			'user',
		)


class SearchMessagesForm(ModelForm):
	OPERATOR_CHOICES = [
		("ALL","All"),
		("ooredoo","ooredoo"),
		("Zain","Zain"),
		("Viva","Viva"),
	]
	ORIGINATOR_CHOICES = [
		("ALL","All"),
		("NPG","NPG"),
		("NPCS","NPCS"),
	]
	ACK_CHOICES = [
		("ALL","All"),
		("ack","Acknowledged"),
		("error","Error"),
	]
	MESSAGE_TYPE_NAME_CHOICES = [
		("ALL","All"),
		("Porting Order", (
			("NPRequest","NPRequest"),
			("NPRequestConfirmation","NPRequestConfirmation"),
			("NPRequestReject","NPRequestReject"),
			("NPRequestCancel","NPRequestCancel"),
			("NPDonorReject","NPDonorReject"),
			("NPDonorAccept","NPDonorAccept"),
			("NPActivated","NPActivated"),
			("NPDeactivated","NPDeactivated"),
			("NPExecuteCancel","NPExecuteCancel"),
			("NPExecuteBroadcast","NPExecuteBroadcast"),
		)),
		("Return Numbers", (
			("NPReturn","NPReturn"),
			("NPReturnCancel","NPReturnCancel"),
			("NPReturnBroadcast","NPReturnBroadcast"),
		)),
		("Ring Fence", (
			("NPRingFenceRequest","NPRingFenceRequest"),
			("NPRingFenceDeny","NPRingFenceDeny"),
			("NPRingFenceApprove","NPRingFenceApprove"),
		)),
		("Emergency Restore", (
			("NPEmergencyRestore","NPEmergencyRestore"),
			("NPEmergencyRestoreDeny","NPEmergencyRestoreDeny"),
			("NPEmergencyRestoreApprove","NPEmergencyRestoreApprove"),
		)),
		("Others", (
			("NPFinalBillNotification","NPFinalBillNotification"),
			("MessageAck","MessageAck"),
			("ErrorNotification","ErrorNotification"),
		)),
	]

	STATUS_CHOICES = [
		('ALL', 'All Status'),
		("Porting Order", (
			("1","1 - Request Sent - Request sent waiting SMS confirmation"),
			("2","2 - Request Rejected - Request rejected invalid information"),
			("3","3 - Request Rejected - Request rejected SMS wasn't received"),
			("4","4 - Request Canceled - Request canceled"),
			("5","5 - Request Confirmed - Request confirmed by customer's SMS"),
			("6","6 - Request Rejected - Request rejected by donor"),
			("7","7 - Request Accepted - Request accepted by donor"),
			("8","8 - Request Accepted - Request auto accepted by CRDB"),
			("9","9 - Activation Request Received - Request has been activated"),
			("10","10 - Number Activated - Porting has been completed"),
			("11","11 - Request Execution Canceled - Request execution canceled"),
			("12","12 - Request Deactivated - Request has been deactivated"),
		)),
		("Return Numbers", (
			("70","70 - NPReturn Active - NPReturn Active"),
			("71","71 - NPReturn Failed - NPReturn validation failed"),
			("72","72 - NPReturn Canceled - NPReturn Canceled"),
			("73","73 - NPReturn Complete - NPReturn Complete"),
			("74","74 - NPReturn Cancel Failed - NPReturn cancel failed"),
		)),
		("Ring Fence", (
			("50","50 - Ring fence sent - New ring fence awaiting help desk approval"),
			("51","51 - Ring fence approved - Ring fence approved and active"),
			("52","52 - Ring fence failed - Ring fence validation failed"),
			("53","53 - Ring fence denied - Ring fence rejected by help desk"),
		)),
		("Emergency Restore", (
			("40","40 - Emergency Restore Active"),
			("41","41 - Emergency Restore Denied - Denied by the CRDB help desk"),
			("42","42 - Emergency Restore Approve - Approved by the CRDB help desk"),
			("43","43 - Emergency Restore failed - Emergency Restore validation failed"),
		)),
		("Others", (
			("150","150 - Waiting Internal - Waiting Internal System Update"),
			("151","151 - Agent Create Account - After account creating please activate"),
		)),
	]
	'''
	STATUS_CHOICES = [('ALL', 'All Status')]

	for key, status in GetStatus("StatusDict").iteritems():
		if key in range(1,200):
			STATUS_CHOICES.append((str(key),status[1]))
	'''
	Status = forms.ChoiceField(
		label = "Status", 
		choices = STATUS_CHOICES,
		initial = 'ALL', 
		widget = forms.Select(),
		required=False,
	)
	
	'''
	USER_CHOICES = [("","All Agent")]
	AllUsers = User.objects.all().order_by('first_name')
	for user in AllUsers:
		USER_CHOICES.append((user.username,'%s %s - %s' % (user.first_name,user.last_name,user.username)))

	User = forms.ChoiceField(
		label = "Agent Name", 
		choices = USER_CHOICES,
		widget = forms.Select(),
		required=False,
	)
	'''
	PhoneNumber = forms.CharField(
		label='Phone Number', 
		required=False,
		max_length = 8,
	)
	DateFrom = forms.DateField(
		label='From', 
		required=False,
	)
	DateTo = forms.DateField(
		label='To', 
		required=False,
	)

	ExecutionDateFrom = forms.DateField(
		label='From', 
		required=False,
	)
	ExecutionDateTo = forms.DateField(
		label='To', 
		required=False,
	)

	NPOrderID = forms.CharField(
		label='Order ID', 
		required=False,
	)
	RecipientName = forms.ChoiceField(
		label = "Recipient", 
		choices = OPERATOR_CHOICES,
		initial = 'ALL', 
		widget = forms.Select(),
		required=False,
	)
	DonorName = forms.ChoiceField(
		label = "Donor", 
		choices = OPERATOR_CHOICES,
		initial = 'ALL', 
		widget = forms.Select(),
		required=False,
	)
	Originator = forms.ChoiceField(
		label = "Originator", 
		choices = ORIGINATOR_CHOICES,
		initial = 'ALL', 
		widget = forms.Select(),
		required=False,
	)
	MessageTypeName = forms.ChoiceField(
		label = "Message", 
		choices = MESSAGE_TYPE_NAME_CHOICES,
		initial = 'ALL', 
		widget = forms.Select(),
		required=False,
	)
	ack = forms.ChoiceField(
		label = "Ack", 
		choices = ACK_CHOICES,
		initial = 'ack', 
		widget = forms.Select(),
		required=False,
	)
	class Meta:
		model = NpMessages
		fields = (
			'NPOrderID',
			'PhoneNumber',
			'MessageTypeName',
			'ack',
			'DonorName',
			'RecipientName',
			'Originator',
			'DateFrom',
			'DateTo',
			'ExecutionDateFrom',
			'ExecutionDateTo',
			'Status',
		)


class NpMessagesForm(ModelForm):
	"""
	Auto generated form to create Server models.
	"""
	class Meta:
		model = NpMessages
		## Added user and status to the fields, so we save the modelform object directly when assigning them to the instance
		#fields = ('NPOrderID','MessageTypeName', 'NumberType', 'CustomerType', 'SubscriptionType', 'CustomerName', 'CustomerID', 'UnbilledPayment', 'RecipientContactInfo', 'ExecutionTime', 'ReversalOrderID','user','status')

class EmergencyRestoreForm(ModelForm):

	class Meta:
		model = NpMessages
		fields = (
			'OriginalNPOrderID',
		)


class EmergencyRestoreForm(ModelForm):

	class Meta:
		model = NpMessages
		fields = (
			'OriginalNPOrderID',
		)

class NPRequestForm(ModelForm):
	#ExecutionTime = forms.SplitDateTimeField(label="Execution Time", input_date_formats='%d-%m-%Y', input_time_formats='%H:%M')
	'''
	ExecutionTime = forms.CharField(
		label='Execution Time', 
		initial = "%s 03:00:00"%str(date.today()+timedelta(days=10)),
		#format = settings.DATETIME_INPUT_FORMATS,
		#widget = forms.TextInput(attrs={'readonly':'readonly'})
	)
	RequestType = forms.ChoiceField(
		label = "Request Type", 
		choices = [
			('NewRequest', 'New Port IN'),
			('NewRequestBlock', 'New Block Order'),
			('EmergencyRestore', 'Emergency Restore'),
		], 
		initial = 'NewRequest', 
		widget = forms.Select()
	)
	'''
	ImmediatePorting = forms.ChoiceField(
		label = "Delayed Porting", 
		choices = [
			('now', 'Immediate Porting'),
			('delay', 'Delayed Porting'),
		], 
		initial = 'now', 
		widget = forms.Select()
	)
	SubscriptionType = forms.ChoiceField(
		label = "Subscription Type", 
		choices = [
			('prepaid', 'Prepaid'),
			('postpaid', 'Postpaid'),
		], 
		initial = 'prepaid', 
		widget = forms.Select()
	)
	CustomerType = forms.ChoiceField(
		label = "Customer Type", 
		choices = [
			('consumer', 'Consumer'),
			('business', 'Business'),
		], 
		initial = 'consumer', 
		widget = forms.Select()
	)

	CustomerIDType = forms.ChoiceField(
		label = "Customer ID Type", 
		choices = [
			(1, 'Passport'),
			(2, 'Civil ID'),
			(3, 'Company Registration Number'),
			(4, 'National ID'),
		], 
		initial = 1, 
		widget = forms.Select()
	)

	PaymentCode = forms.CharField(
		label = "Payment Code", 
		max_length = 8,
		required=False,
	)

	ValidationMSISDN = forms.CharField(
		label = "Mobile Number", 
		max_length = 8,
	)

	# Currently is set on auto Mobile
	'''
	ServiceType = forms.ChoiceField(
		label = "Service Type", 
		choices = [
			('mobile', 'Mobile'),
			('fixed', 'Fixed'),
		], 
		initial = 'mobile', 
		widget = forms.Select()
	)
	'''
	
	class Meta:
		model = NpMessages
		fields = (
			#'RequestType',
			'CustomerName',
			'CustomerType',
			'CustomerIDType',
			'CustomerID',
			#'ServiceType',
			'SubscriptionType',
			'ImmediatePorting',
			'PortingTime',
			'DateOfBirth',
			'ValidationMSISDN',
			'PaymentCode',
			#'BlockOrderID',
			#'BlockOrderCount',
		)
		exclude = ['EmergencyRestoreID']

class NPRequestEmergencyRestoreForm(ModelForm):
	CustomerType = forms.ChoiceField(
		label = "Customer Type", 
		choices = [
			('consumer', 'Consumer'),
			('business', 'Business'),
		], 
		initial = 'consumer', 
		widget = forms.Select()
	)
	SubscriptionType = forms.ChoiceField(
		label = "Subscription Type", 
		choices = [
			('prepaid', 'Prepaid'),
			('postpaid', 'Postpaid'),
		], 
		initial = 'prepaid', 
		widget = forms.Select()
	)

	CustomerIDType = forms.ChoiceField(
		label = "Customer ID Type", 
		choices = [
			(1, 'Passport'),
			(2, 'Civil ID'),
			(3, 'Company Registration Number'),
			(4, 'National ID'),
		], 
		initial = 1, 
		widget = forms.Select()
	)
	ValidationMSISDN = forms.CharField(
		label = "Mobile Number", 
		max_length = 8,
	)	
	class Meta:
		model = NpMessages
		fields = (
			'CustomerName',
			'CustomerType',
			'CustomerIDType',
			'CustomerID',
			'SubscriptionType',
			'ValidationMSISDN',
			'EmergencyRestoreID',
		)

class RoutingInfoListForm(ModelForm):
	PhoneNumberStart = forms.IntegerField(
		label="Phone Number Start", 
		required=True, 
		help_text="The start number of the range",
		#widget=forms.TextInput(attrs={'class' : 'textbox'}),
	)
	PhoneNumberEnd = forms.IntegerField(
		label="Phone Number End", 
		required=True, 
		help_text="The end number of the range",
		#widget=forms.TextInput(attrs={'class' : 'textbox'}),
	)
	
	def clean(self):
		#cleaned_data = self.cleaned_data # individual field's clean methods have already been called
		PhoneNumberStart = self.cleaned_data.get("PhoneNumberStart")
		PhoneNumberEnd = self.cleaned_data.get("PhoneNumberEnd")

		if PhoneNumberEnd < PhoneNumberStart:
			raise forms.ValidationError("The End Range should be bigger then the Start Range.") 
			#print 'error here'

		#print self.cleaned_data
		#raise forms.ValidationError("Range is not correct.") 
		return self.cleaned_data
	
	class Meta:
		model = RoutingInfoList
		fields = (
			'PhoneNumberStart',
			'PhoneNumberEnd',
		)
		exclude = ['RoutingNumber']

RoutingInfoListFormSet = inlineformset_factory(NpMessages, RoutingInfoList, form=RoutingInfoListForm, can_delete=False, extra=1)

class NPDonorRejectForm(ModelForm):

	REJECT_CHOICES = [
		("1", "The MSISDN is not a valid number on the Donor's network"),
		("2", "The MSISDN and the identifier2 do not match (for subscriber)"),
		("3", "The MSISDN and the Corporate Registration Number do not match (for Corporate)"),
		("4", "The MSISDN is already subject to suspension for outgoing and incoming calls, i.e. not active"),
		("5", "Any other reason agreed to by NTEC and notified to the operators in writing"),
	]

	DonorRejectReason = forms.ChoiceField(label = "Reject Reason", choices = REJECT_CHOICES, widget = forms.Select(),)

	class Meta:
		model = DonorRejectList
		fields = (
			'DonorRejectReason',
			'DonorRejectMessage',
			'PhoneNumberStart',
			'PhoneNumberEnd',
		)


class RingFencForm(ModelForm):
	class Meta:
		model = NpMessages
		fields = (
		)

class NPRequestCancelForm(ModelForm):
	CancelReason_CHOICES = [
		("1", "The subscriber has requested to cancel the porting"),
	]
	DonorRejectReason = forms.CharField(widget=forms.Textarea(attrs={'cols': 80, 'rows': 20}))

	class Meta:
		model = NpMessages
		fields = (
			'CancelReason',
			'CancelMessage',
		)


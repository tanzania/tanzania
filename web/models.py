from django.db import models
from django.db.models import FileField
from configuration import GetConfig, LogInput, GetRequest, GetOperator, LogMessageResutls
from django.core.files.storage import FileSystemStorage
from django.core.exceptions import ValidationError
from npg import settings

#For Regular Expression validation/Field validation
from django.core.validators import RegexValidator
import re
SID = re.compile('^[A-Z]{4}[\-][0-9]{4}[\-][0-9]{8}$')
NUM = re.compile('^[0-9]{8}$')
SIM = re.compile('^[0-9]{18,19}$')
CPR = re.compile('^[0-9]{9}$')
CRN = re.compile('^[0-9]{1,7}[\/][0-9]{1,2}$')
PASN = re.compile('^([0-9A-Za-z]{5}|[0-9A-Za-z]{12})$')
CMNT = re.compile('^[a-zA-Z0-9\-\_\+\.\s]{1,100}$')

## Making the DB collation utf8_general_ci
## All regular arabic characters, indian numbers, arabic numbers, and all english characters (small and capital). 
ar_en_char_num_all = re.compile(u'[\u0620-\u06A4\u0661-\u0669_1-9a-zA-Z]',re.UNICODE)

ar_en_char = re.compile(u'^[\u0620-\u06A4_a-zA-Z ]+$',re.UNICODE)  

#ar_char =  re.compile(u'[\u0620-\u06A4 ]',re.UNICODE)
#ar_num = re.compile(u'[\u0661-\u0669]',re.UNICODE)

#Example:
#passport_number = models.CharField("Passport Number", max_length=18, blank=True, validators=[RegexValidator(regex=PASN, message="Invalid input!")])
##########End of patterns############################
CUSOTMERNAME =  re.compile('^[a-zA-Z ]{50}$')
CUSTOMERID =  re.compile('^[a-zA-Z ]$')
CUSTOMERTYPE =  re.compile('^$')
#PLACEOFBIRTH =  re.compile('^[a-zA-Z ]{50}$')
RECIPIENTCONTACTINFO =  re.compile('^[a-zA-Z ]{100}$')
PHONENUMBER = re.compile('^[0-9]{8}$')
PHONENUMBER_message = "The number is incorrect, must be 8 digits."
NUMBERTYPE =  re.compile('^[0-2]$')
SUBSCRIPTIONTYPE =  re.compile('^[0-1]$')
EXECUTIONTIME =  re.compile('^$')
REVERSALORDERID =  re.compile('^[0-9]{14}$')

MESSAGE_CODE_CHOICES = (
	('NPRequest', 'NPRequest'),
	('NPRequestReject', 'NPRequestReject'),
	('NPRequestConfirmation', 'NPRequestConfirmation'),
	('NPRequestCancel', 'NPRequestCancel'),
	('NPDonorReject', 'NPDonorReject'),
	('NPDonorAccept', 'NPDonorAccept'),
	('NPActivated', 'NPActivated'),
	('NPDeactivated', 'NPDeactivated'),
	('NPExecuteBroadcast', 'NPExecuteBroadcast'),
	('NPExecuteCancel', 'NPExecuteCancel'),
	('NPReturn', 'NPReturn'),
	('NPReturnCancel', 'NPReturnCancel'),
	('NPReturnBroadcast', 'NPReturnBroadcast'),
	('NPRingFenceRequest', 'NPRingFenceRequest'),
	('NPRingFenceDeny', 'NPRingFenceDeny'),
	('NPRingFenceApprove', 'NPRingFenceApprove'),
	('NPEmergencyRestore', 'NPEmergencyRestore'),
	('NPEmergencyRestoreDeny', 'NPEmergencyRestoreDeny'),
	('NPEmergencyRestoreApprove', 'NPEmergencyRestoreApprove'),
	('FinalBillNotification', 'FinalBillNotification'),
)

SERVICE_TYPE_CHOICES = (
	('M', 'Mobile'),
	('F', 'Fixed'),
	('S', 'Special Services'),
)

YES_NO_CHOICES = (
	('None', 'Blank'),
	('Y', 'Yes'),
	('N', 'No'),
)	

DIRECTION_CHOICES = (
	("ALL","All"),
	("NPG","NPG"),
	("NPCS","NPCS"),
)

DONOR_REJECT_REASON_CODES = (
	("1", "The MSISDN is not a valid number on the Donor's network"),
	("2", "The MSISDN and the identifier2 do not match (for subscriber)"),
	("3", "The MSISDN and the Corporate Registration Number do not match (for Corporate)"),
	("4", "The MSISDN is already subject to suspension for outgoing and incoming calls, i.e. not active"),
	("5", "Any other reason agreed to by NTEC and notified to the operators in writing"),
)

NPCDB_REJECT_REASON_CODES = (
	(0, ''),
)

MESSAGE_TYPES = (#ID  Message name						Description
	('1', 'NP Request'),								#(Recipient) Request for portation
	('4', 'NP Request Cancel'),						#(Recipient) Cancel the request for portation
	('7', 'NP Donor Reject'),							#(Donor) Reject porting request
	('8', 'NP Donor Accept'),							#(Donor) Accept porting request
	('9', 'NP Execution Cancel'),						#(Recipient) Cancel porting
	('12', 'NP Execution'),							#(NPCDB) Confirm porting execution
	('14', 'NP Return'),								#(Holder) Return ported number
	('16', 'NP Return Execution'),					#(NPCDB) Confirm return of ported number
	('25', 'NP Billing Resolution'),					#(Donor) Start Billing Resolution process
	('26', 'NP Billing Resolution End'),				#(Donor or NPCDB) End Billing Resolution process
	('27', 'NP Billing Resolution Received'),			#(Recipient) Confirm Billing Resolution process
	('28', 'NP Billing Resolution Alert'),			#(Donor) Request service degradation
	('29', 'NP Billing Resolution Alert Received'),	#(Recipient) Confirm service degradation
	('30', 'NP Billing Resolution Error'),			#(NPCDB) Error notification
)

STATUS_CHOICES = (
				('1','Message has been delivered successfully'),
				('2','Failure to send the message'),
				('3','blah blah'),
				)

SERVICE_TYPE = (
	('mobile', 'Mobile'),
	('fixed', 'Fixed'),
)

CUSTOMER_TYPE = (
	('consumer', 'Consumer'),
	('business', 'Business'),
)

SUBSCRIPTION_TYPE = (
	('prepaid', 'Prepaid'),
	('postpaid', 'Postpaid'),
)

CUSTOMER_ID_TYPE = (
	(1, 'Passport'),
	(2, 'Civil ID'),
	(3, 'Company Registration Number'),
)

class NpMessages(models.Model):
	MessageLocalID = models.AutoField(primary_key=True)
	message_created = models.DateTimeField(editable=False)
	message_modified = models.DateTimeField(editable=False)
	Originator = models.CharField("Originator", max_length=6, choices=DIRECTION_CHOICES, blank=True, null=True, db_index=True)
	new_message = models.NullBooleanField(choices=YES_NO_CHOICES, db_index=True)
	ack = models.CharField(max_length=6, blank=True, null=True)
	related = models.CharField(max_length=16, blank=True, null=True)
	user = models.CharField(max_length=16, blank=True, null=True)
	status = models.CharField(max_length=30, blank=True, null=True)
	ErrorCode = models.CharField("Error Code", max_length=200, blank=True, null=True) 
	Description = models.CharField("Description", max_length=200, blank=True, null=True) 
	MessageTypeID = models.SmallIntegerField(max_length=5, blank=True, null=True) 
	MessageTypeName = models.CharField("Message Name", max_length=100, blank=True, null=True) 
	last_message = models.CharField("Message Name", max_length=100, blank=True, null=True)

	# Globally unique message identifier
	MessageID = models.BigIntegerField(max_length=15, blank=True, null=True) 
	SenderID = models.SmallIntegerField(max_length=3, blank=True, null=True) # provider ID of message sender
	SenderName = models.CharField("Sender Name", max_length=20, blank=True, null=True) # provider ID of message sender
	NPOrderID = models.BigIntegerField("Order ID", max_length=20, blank=True, null=True) # Unique porting order ID generated by the NPCDB
	EmergencyRestoreID = models.BigIntegerField("Emergency Restore ID", max_length=15, blank=True, null=True)
	BlockOrderID = models.CharField("Block Order ID", max_length=15, blank=True, null=True) 
	BlockOrderCount = models.BigIntegerField("Block Order Count", max_length=30, blank=True, null=True)
	ValidationMSISDN = models.CharField("Validation MSISDN", max_length=12, blank=True, null=True) 
	RecipientID = models.SmallIntegerField(max_length=3, blank=True, null=True) # Provider ID for recipient
	RecipientName = models.CharField("Recipient Name", max_length=20, blank=True, null=True) # Provider ID for recipient
	SubscriptionProviderID = models.SmallIntegerField(max_length=3, blank=True, null=True) # Provider ID for recipient
	SubscriptionProviderName = models.CharField("Recipient Name", max_length=20, blank=True, null=True) # Provider ID for recipient
	RangeHolderID = models.SmallIntegerField(max_length=3, blank=True, null=True) # Provider ID for current holder
	RangeHolderName = models.CharField("Holder Name", max_length=20, blank=True, null=True) # Provider ID for current holder
	DonorID = models.SmallIntegerField(max_length=3, blank=True, null=True)
	DonorName = models.CharField("Donor Name", max_length=20, blank=True, null=True) 
	DateOfBirth = models.DateField("Date Of Birth", blank=True, null=True) 
	#DateOfBirth = models.DateTimeField ("Date Of Birth", blank=True, null=True) 
	CustomerName = models.CharField("Customer Name", max_length=50, blank=False, null=True, 
		validators=[RegexValidator(regex=ar_en_char, message="Inappropriate Name!")],
	) 
	CustomerID = models.CharField("Customer ID", max_length=100, blank=False, null=True) # Provider ID for current holder
	CustomerIDType = models.SmallIntegerField('Customer ID Type', choices = CUSTOMER_ID_TYPE, max_length=1, blank=False, null=True) 
	ServiceType = models.CharField("Service Type", max_length=20, choices = SERVICE_TYPE, blank=False, null=True)
	CustomerType = models.CharField("Customer Type", max_length=20, choices = CUSTOMER_TYPE, blank=False, null=True)
	SubscriptionType = models.CharField("Subscription Type", max_length=20, choices=SUBSCRIPTION_TYPE, blank=False, null=True)
	NpcdbRejectReason = models.SmallIntegerField("NPCDB Reject Reason", max_length=6, choices = NPCDB_REJECT_REASON_CODES, blank=True, null=True)
	NpcdbRejectMessage = models.CharField("NPCDB Reject Message", max_length=254, blank=True, null=True)
	CancelReason = models.SmallIntegerField("NPCDB Reject Reason", max_length=4, choices = NPCDB_REJECT_REASON_CODES, blank=True, null=True)
	CancelMessage = models.CharField("Cancel Message", max_length=254, blank=False, null=True)
	PortingTime = models.DateTimeField("Porting Date", blank=True, null=True) 
	AutomaticAccept = models.SmallIntegerField("Automatic Accept", max_length=1, blank=True, null=True)
	Balance = models.DecimalField("Unbilled Payment", max_digits=12, decimal_places=3, blank=False, null=True)
	OriginalNPOrderID = models.BigIntegerField("Order ID", max_length=20, blank=True, null=True) # Unique porting order ID generated by the NPCDB
	PaymentCode = models.IntegerField(max_length=9, blank=True, null=True) # Reference to NP Order ID
	DueDate = models.DateTimeField("Due Date", blank=True, null=True) 
	HandleManually = models.SmallIntegerField("Handle Manually", max_length=1, blank=True, null=True)

	# the save function will set the time message created and modified, will also fill up auto fileds
	def save(self, **kwargs):
		from datetime import datetime
		
		# Fix the time created and time modified
		if not self.MessageLocalID:
			self.message_created = datetime.now() # Edit created timestamp only if it's new entry
		self.message_modified = datetime.now()
		# auto ganerated values
		if self.MessageTypeID:
			self.MessageTypeName = GetRequest(self.MessageTypeID)
		elif self.MessageTypeName:
			self.MessageTypeID = GetRequest(self.MessageTypeName)

		if self.RecipientID:
			self.RecipientName = GetOperator(int(self.RecipientID))
		if self.SubscriptionProviderID:
			self.SubscriptionProviderName = GetOperator(int(self.SubscriptionProviderID))
		if self.RangeHolderID:
			self.RangeHolderName = GetOperator(int(self.RangeHolderID))
		if self.DonorID:
			self.DonorName = GetOperator(int(self.DonorID))
		if self.SenderID:
			self.SenderName = GetOperator(int(self.SenderID))

		super(NpMessages,self).save()
        
	class Meta:
		db_table = "np_messages"

class RoutingInfoList(models.Model):
	np_messages = models.ForeignKey(NpMessages, related_name='RoutingInfoList')
	PhoneNumberStart = models.CharField(max_length=12, blank=False, null=False, validators=[RegexValidator(regex = PHONENUMBER, message = PHONENUMBER_message)])
	PhoneNumberEnd = models.CharField(max_length=12, blank=False, null=False, validators=[RegexValidator(regex = PHONENUMBER, message = PHONENUMBER_message)])
	RoutingNumber = models.CharField(max_length=7, blank=True, null=True)
	TotalNumber = models.BigIntegerField(max_length=20, blank=True, null=True)
	
	def save(self, **kwargs):
		self.TotalNumber = int(self.PhoneNumberEnd) - int(self.PhoneNumberStart) + 1
		#LogInput(str(self.TotalNumber) +"="+ str(int(self.PhoneNumberEnd)) + "-" + str(int(self.PhoneNumberStart)) +"+ 1")
		
		
		super(RoutingInfoList,self).save()

	class Meta:
		db_table = "np_routing_info"

class DonorRejectList(models.Model):
	np_messages					= models.ForeignKey(NpMessages, related_name='DonorRejectList')
	DonorRejectReason			= models.SmallIntegerField("Donor Reject Reason", max_length=4, blank=False, null=True)
	DonorRejectMessage		= models.CharField("Donor Reject Message", max_length=254, blank=False, null=True)
	PhoneNumberStart 			= models.CharField(max_length=12, blank=False, null=False, validators=[RegexValidator(regex = PHONENUMBER, message = PHONENUMBER_message)])
	PhoneNumberEnd				= models.CharField(max_length=12, blank=False, null=False, validators=[RegexValidator(regex = PHONENUMBER, message = PHONENUMBER_message)])	

	class Meta:
		db_table = "np_donor_reject_list"


class FinalBillNotification(models.Model):
	np_messages = models.ForeignKey(NpMessages, related_name='FinalBillNotification')
	date_created = models.DateTimeField(editable=False)
	date_modified = models.DateTimeField(editable=False)
	PortingTime = models.DateTimeField(blank=True, null=True) 
	NPOrderID = models.BigIntegerField(max_length=20, blank=True, null=True) # Unique porting order ID generated by the NPCDB
	Balance = models.DecimalField(max_digits=12, decimal_places=3, blank=False, null=True)
	DueDate = models.DateTimeField(blank=True, null=True) 
	status = models.CharField(max_length=30, blank=True, null=True)

	def save(self, **kwargs):
		import datetime
		
		# Fix the time created and time modified
		if not self.pk:
			self.date_created = datetime.datetime.now() # Edit created timestamp only if it's new entry
		self.date_modified = datetime.datetime.now()
		super(FinalBillNotification,self).save()

	class Meta:
		db_table = "np_final_bill_notification"

'''

AttachmentListStorage = FileSystemStorage(location=settings.STORAGE_ROOT)

class AttachmentList(models.Model):	
	
	# Customer validation for file extension
	def validate_file_extension(value):
		AllowedFiles = GetConfig("FILE_FORMAT")
		if not value.name.endswith(AllowedFiles):
			raise ValidationError(u'The allowed file types: %s' % str(AllowedFiles))


	# two documents are requried: 1- Personal identification document. 2- Signed porting request form

	np_messages = models.ForeignKey(NpMessages, related_name='AttachmentList')

	FileName = models.CharField(max_length=100, blank=True, null=True)

	FileType = models.CharField(max_length=5, blank=True, null=True)

	OrginalFileName = models.CharField(max_length=100, blank=True, null=True)

	#AttachFile = models.ImageField(upload_to='attach',storage=AttachmentListStorage)
	#AttachImage = models.ImageField(storage=AttachmentListStorage)

	AttachFile = FileField(upload_to='attach', storage=AttachmentListStorage, blank=False, validators=[validate_file_extension])

	class Meta:
		db_table = "np_attachment_list"




## lightspeed voice gateway database table's structure
class Number_Portability(models.Model):
	i_number = models.AutoField(primary_key=True, blank=True) 
	destination =  models.CharField("Destination", max_length=32, blank=True, null=True)
	origin =  models.CharField("Route", max_length=32, blank=True, null=True)

	class Meta:
		db_table = GetConfig("VGW_TABLE_NAME")

## Rawabi voice gateway database table's structure
class Number_Portability(models.Model):
	portid 			= models.CharField(max_length=100, blank=True) 
	b 				= models.CharField(max_length=100, blank=True, null=True)
	number 			= models.CharField(primary_key=True, max_length=100)
	recipient 		= models.CharField(max_length=100, blank=True, null=True)
	doner 			= models.CharField(max_length=100, blank=True, null=True)
	rn 				= models.CharField(max_length=100, blank=True, null=True)
	date_ported 	= models.CharField(max_length=100, blank=True, null=True)
	date_updated 	= models.DateTimeField(auto_now=True, auto_now_add=True, blank=True, null=True)

	class Meta:
		db_table = "np_voice_gateway"
'''	

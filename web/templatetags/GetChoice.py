from django import template
from django.template.defaultfilters import stringfilter
from configuration import GetStatus
import datetime
from configuration import LogInput

register = template.Library()

@register.filter
@stringfilter
def GetStatusName(value,value2):
	try:
		return str(GetStatus(int(value2))[0])
	except:
		return str(value)

@register.filter
@stringfilter
def GetStatusDetails(value,value2):
	try:
		return str(GetStatus(int(value2))[1])
	except:
		return str(value)
		
@register.filter
@stringfilter
def GetStatusAction(value,value2):
	
	if value2 in ("7","8","151"):
		# Activate
		ActionButton = 'Activate'
		
	elif value2 in ("1","5","70"):
		# Cancel
		ActionButton = 'Cancel'
		
	elif value2 in ("9","10"):
		# Copy Request
		#ActionButton = 'Copy Request'
		ActionButton = 'N/A'
		
	elif value2 == "5":
		# N/A
		ActionButton = 'N/A'
		
	elif value2 in ("2","3","4","6","11","40","52","53","71","72","73","74",):
		# Send Again
		#ActionButton = 'Send Again'
		ActionButton = 'N/A'

	elif value2 == "42":
		# New Request for Emergency Restore
		ActionButton = 'New Request'
	
	try:
		return str(ActionButton)
	except:
		return str(value)


@register.filter
@stringfilter
def GetStatusActionButton(value,value2):
	
	if value2 in ("7","8"):
		# Activate
		ActionButton = 'btn btn-success'

	elif value2 == 1:
		# N/A
		ActionButton = 'N/A'
	elif  value2  in("1", "5", "70"):
		ActionButton = 'btn btn-danger cancel_rq'
		
	elif value2 in ("2","3","4","6","9","10","11","51","52","53","71","72","73","74",):
		# Send Again
		ActionButton = 'btn btn-primary'

	elif value2 == "42":
		# New Request for Emergency Restore
		ActionButton = 'btn btn-success'
	
	try:
		return str(ActionButton)
	except:
		return str(value)

@register.filter
def RingFenceEndDate(value,value2):
	LogInput("RingFence    value" + str(value),"dev")
	LogInput("RingFence    value2 " + str(value2),"dev")
	LogInput("RingFence    value2 type " + str(type(value2)),"dev")
	try:
		#value2 = str(value2)[:-6]
		result = value2 + datetime.timedelta(30)
		LogInput("RingFence    result " + str(result),"dev")
		return result
	except:
		return str(value)
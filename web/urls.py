from django.conf.urls.defaults import patterns, include, url
from django.views.generic import DetailView, ListView
from configuration import GetConfig, LogInput
from django.contrib.auth.decorators import login_required
from web import views

from models import NpMessages

urlpatterns = patterns('',
	url(r'^/', login_required(views.SendNpRequest)),
	url(r'^new/$', login_required(views.SendNpRequest)),
	url(r'^alld/$', login_required(views.AllMessages)),
	url(r'^search/$', login_required(views.SearchMessages)),
	url(r'^search/(?P<pk>\d+)$',login_required(views.DetailMessages)),
	url(r'^outgoing/$', login_required(views.SentRequest)),
	url(r'^incoming/$', login_required(views.ReceivedRequest)),
	url(r'^erestore/$', login_required(views.EmergencyRestore)),
	url(r'^newerestore/$', login_required(views.SendNpRequestEmergencyRestore)),
	url(r'^ringfence/$', login_required(views.RingFence)),
	url(r'^npreturn/$', login_required(views.NPReturn)),
)

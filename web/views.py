# Create your views here.
import os, cgi
import datetime
import base64

import traceback
from django.utils import timezone
from configuration import GetConfig, LogInput, GetRequest, LogMessageResutls, GetTimeStamp
from models import NpMessages, RoutingInfoList
from STP_app.models import STP_db
from int_ras.models import IntegrationRAS
from forms import NPRequestForm, RoutingInfoListFormSet, NpMessagesForm, FilterForm, NPDonorRejectForm, EmergencyRestoreForm
from forms import RingFencForm, NPRequestCancelForm, SearchMessagesForm, NPRequestEmergencyRestoreForm
from int_ras.forms import RASFormSet
from soap.handle_out_message import HandleOutgoingMessage
#Importing the sender fucntion that will take data from from and pass it to CS
from reports.tzinfo import UTC
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render
from django.template import RequestContext
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import loader, Context
from django.db.models import Q
from django.forms.models import model_to_dict
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required,permission_required
from django.utils.timezone import utc

def index(request):
	return render_to_response('web/index.html')

def DetailMessages(request,pk):
	PageName = "Message Detail"
	LogInput('Inside DetailMessages')
	
	message_list = NpMessages.objects.select_related().get(MessageLocalID=pk)

	# This is if the message doesn't have order id, so we get the related message to this message, the first if stetment is for bill notification
	if message_list.OriginalNPOrderID:
		related_message_list = NpMessages.objects.filter(NPOrderID=message_list.NPOrderID) | NpMessages.objects.filter(NPOrderID=message_list.OriginalNPOrderID).exclude(ack="error") | NpMessages.objects.filter(OriginalNPOrderID=message_list.OriginalNPOrderID).exclude(ack="error")
		related_message_list = related_message_list.order_by('-MessageLocalID')
	elif message_list.NPOrderID:
		related_message_list = NpMessages.objects.filter(NPOrderID=message_list.NPOrderID) | NpMessages.objects.filter(OriginalNPOrderID=message_list.NPOrderID)
		related_message_list = related_message_list.order_by('-MessageLocalID')
	else:
		related_message_list = NpMessages.objects.filter(related=message_list.pk).order_by('-MessageLocalID') or NpMessages.objects.filter(pk=message_list.related)

	try:
		order_info = related_message_list.filter(MessageTypeName="NPRequest")
	except:
		pass
	
	# This is to get the STP information when the message is NPExecuteBroadcast
	try:
		if message_list.MessageTypeName in ("NPExecuteBroadcast", "NPReturnBroadcast"):
			stp_message_list = STP_db.objects.filter(NPOrderID = message_list.NPOrderID)
			
	except:
		pass
		
	return render_to_response('web/detail.html', locals(), context_instance = RequestContext(request))


@permission_required('authenticate.sent_requests',  raise_exception=True)	
@csrf_protect
def NPReturn(request):
	'''
	This loads the Sent Request page
	'''
	PageName = "NP Return"
	

	if request.POST: # If the form has been submitted
		instance = NpMessages()
		form = FilterForm(request.POST, prefix='main')

		LogInput('There is submission')
		
		if 'Cancel' in request.POST['SubmitType']:
			MessageLocalID = request.POST['MessageLocalID']
			NPRequestEntry = NpMessages.objects.get(pk = MessageLocalID)
	
			LogInput('Found Message: ' + LogMessageResutls(NPRequestEntry))

			NewMessage = NpMessages(
				MessageTypeID = GetRequest('NPReturnCancel'),
				NPOrderID = str(NPRequestEntry.NPOrderID) if NPRequestEntry.NPOrderID is not None else str(''),
				user = str(request.user),
			)
			
			NewMessage.save()
			
			# Log the user that submitted the form
			LogInput(str(request.user))
			#NPRequestEntry.user = str(request.user)		
			
			LogInput("MessageTypeID: " + str(NewMessage.MessageTypeID))
			LogInput('Inside if Cancel')

			# Send the new cancel message, and Order and the user
			#Respond = HandleOutgoingMessage(NewMessage, NPRequestEntry, str(request.user, str(request.user)))
			
			Respond = HandleOutgoingMessage(NewMessage, str(request.user))
			
			LogInput('Cancel Respond: ' + str(Respond))

		elif 'Filter' in request.POST['SubmitType']:
			LogInput("SubmitType is Filter")

			if form.is_valid():
				phone_number = form.cleaned_data['PhoneNumber']
				
				if phone_number:
					FindPhoneNumber(phone_number)

				# building a dic for all filters
				FilterDict = dict(
					RoutingInfoList__PhoneNumberEnd__gte = int(phone_number) if phone_number else '',
					RoutingInfoList__PhoneNumberStart__lte = int(phone_number) if phone_number else '',
					Originator="NPG",
					MessageTypeName="NPReturn",
				)
				#will remove all empty in dic
				FilterDict = dict((k, v) for k, v in FilterDict.iteritems() if v)
				
				LogInput("Form is valid for filter, filter is: " + str(FilterDict))
				message_list = NpMessages.objects.select_related().filter(**FilterDict).order_by('-message_created')[:100]
				
				return render_to_response('web/np_return.html', locals(), context_instance = RequestContext(request))
		
		
		else:
			LogInput('Inside else')
			LogInput('ERROR: There is no submission')
		
	else:	
		form = FilterForm(prefix="main")
		nform = NPRequestCancelForm(prefix="Reject")

	message_list = NpMessages.objects.filter(MessageTypeName="NPReturn", status__in = [70]).order_by('-message_modified')

	return render_to_response('web/np_return.html', locals(), context_instance = RequestContext(request)) 



@permission_required('authenticate.sent_requests',  raise_exception=True)	
@csrf_protect
def RingFence(request):
	PageName = "Ring Fence"
	if 'Submit' in request.POST:
		instance = NpMessages()
		BulkPhoneNumber = request.POST['BulkPhoneNumber']
		nform = RingFencForm(request.POST, instance=instance, prefix='main')
		if nform.is_valid(): # All validation rules pass
			NewRequest = nform.save(commit=False)
			#LogInput(BulkPhoneNumber,"Development","4gtss")
			if BulkPhoneNumber == "":
				ListOfNumbers = [False,"Missing phone numbers"]
			else:
				ListOfNumbers = ProcessBulkNumbers(BulkPhoneNumber)

			if ListOfNumbers[0]:
				NewRequest.MessageTypeID = GetRequest("NPRingFenceRequest")
				NewRequest.save()
				SaveBulkNumbers(ListOfNumbers[1],NewRequest)
				Respond = HandleOutgoingMessage(NewRequest, str(request.user))
			else:
				BulkPhoneError = ListOfNumbers[1]
	else:
		nform = RingFencForm(prefix="main")

	ThirtyDaysAgo = timezone.now() - datetime.timedelta(days=35)
	FiveDaysAgo = timezone.now() - datetime.timedelta(days=5)
	
	message_list = NpMessages.objects.filter(MessageTypeName="NPRingFenceRequest", ack = 'ack', message_created__gte=ThirtyDaysAgo, status__in = [50,51]) | NpMessages.objects.filter(MessageTypeName="NPRingFenceRequest", ack = 'ack', message_created__gte=FiveDaysAgo, status__in = [53])
	message_list = message_list.order_by('-message_modified')

	return render_to_response('web/ring_fence.html', locals(), context_instance = RequestContext(request))



@permission_required('authenticate.sent_requests',  raise_exception=True)	
@csrf_protect
def EmergencyRestore(request):
	PageName = "Emergency Restore"

	if request.POST: # If the form has been submitted
		if request.POST['SubmitType'] in ('New Request'):
			if 'Submit' in request.POST:
				instance = NpMessages()
		
				nnform = NPRequestEmergencyRestoreForm(request.POST, instance=instance, prefix='ERmain')
				rasform = RASFormSet(request.POST, instance=instance, prefix='RAS')
				BulkPhoneNumber = request.POST['BulkPhoneNumber']

				if nnform.is_valid(): # All validation rules pass
					NewRequest = nnform.save(commit=False)
					#LogInput(BulkPhoneNumber,"Development","4gtss")
					if BulkPhoneNumber == "":
						ListOfNumbers = [False,"Missing phone numbers"]
					else:
						ListOfNumbers = ProcessBulkNumbers(BulkPhoneNumber)

					if ListOfNumbers[0]:
						if rasform.is_valid():
							# NewRequest is the message that is saved to DB
							# set the message id as nprequst, also the message that is saved to db
							NewRequest.MessageTypeID = GetRequest("NPRequest")
		
							NewRequest.save()
							rasform.save()
							RASMessage = IntegrationRAS.objects.get(np_messages=NewRequest)
							RASMessage.PhoneNumber = "RANGE"

							SaveBulkNumbers(ListOfNumbers[1],NewRequest)
	
							# Changes for dealer ID add on 14 of Aug
							try:
								RASMessage.dealer_id = request.user.get_profile().dealer_id
							except:
								if RASMessage.CustomerType == "business":
									RASMessage.dealer_id = "77000"
								elif RASMessage.CustomerType == "consumer":
									RASMessage.dealer_id = "999999"
	
							RASMessage.save()
							
							Respond = HandleOutgoingMessage(NewRequest, str(request.user))
							LogInput('Accept Respond: ' + str(Respond))
							
							LogInput('Results in the new request page .. ')
							LogInput(str(Respond))
					else:
						BulkPhoneError = ListOfNumbers[1]
			else:
				ERNPOrderID = NpMessages.objects.get(NPOrderID = request.POST['NPOrderID'], MessageTypeName = "NPEmergencyRestoreApprove").EmergencyRestoreID
				NPRequestEntry = NpMessages.objects.get(NPOrderID = request.POST['OriginalNPOrderID'], MessageTypeName = "NPRequest")
				NPRequestEntry.EmergencyRestoreID = ERNPOrderID
				#SendNpRequestEmergencyRestore(request, NPRequestEntry)

				PageName = "Emergency Restore"
				today = datetime.date.today()
				today30 = today - datetime.timedelta(days=30)
	
				nnform = NPRequestEmergencyRestoreForm(instance=NPRequestEntry, prefix='ERmain')
				rasform = RASFormSet(prefix='RAS')
			return render_to_response('web/new_er_request.html', locals(), context_instance = RequestContext(request))

		elif 'Submit' in request.POST:
			'''
			nform is the main form which contains all the customer info, verify all form contains
			'''
			instance = NpMessages()
			nform = EmergencyRestoreForm(request.POST, instance=instance, prefix='main')
			if nform.is_valid(): # All validation rules pass
				NewRequest = nform.save(commit=False)

				NewRequest.MessageTypeID = GetRequest("NPEmergencyRestore")
				NewRequest.NPOrderID = NewRequest.OriginalNPOrderID

				NewRequest.save()

				Respond = HandleOutgoingMessage(NewRequest, str(request.user))
				LogInput('Accept Respond: ' + str(Respond))
				
				LogInput('Results in the new request page .. ')
				LogInput(str(Respond))
					
			else:
				LogInput('nform.is_valid is not valid')
					
	else:
		nform = EmergencyRestoreForm(prefix="main")
	
	today = timezone.now()
	today90 = timezone.now() - datetime.timedelta(days=90)
	today10 = timezone.now() - datetime.timedelta(days=10)


	message_list = NpMessages.objects.filter(message_modified__gte = today90, MessageTypeName="NPEmergencyRestoreApprove") | NpMessages.objects.filter(message_modified__gte = today90, status__in = [40]) | NpMessages.objects.filter(message_modified__gte = today10, status__in = [41])
	
	message_list = message_list.order_by('-message_modified')
	'''
	ERfound = NpMessages.objects.filter(message_modified__gte = today90, status__in = [40,41,42]).order_by('-message_modified')

	message_list = NpMessages.objects.filter(status__in = [1099090])
	
	for each in ERfound:
		NPOrderExecute = NpMessages.objects.get(NPOrderID = each.OriginalNPOrderID, MessageTypeName="NPExecuteBroadcast")
		
		if NPOrderExecute.PortingTime > today90:
			try:
				message_list = message_list | NpMessages.objects.filter(status__in = [40,41,42], OriginalNPOrderID= NPOrderExecute.NPOrderID)
			except:
				message_list = message_list | NpMessages.objects.filter(status__in = [40,41,42], OriginalNPOrderID= NPOrderExecute.NPOrderID)
	'''
	'''
	# testing the differnce between time and timezone
	from django.utils import timezone
	x = datetime.datetime.now() + datetime.timedelta(minutes = 15)
	y = timezone.now() + datetime.timedelta(minutes = 15)
	z = timezone.make_aware(datetime.datetime.now(),timezone.get_default_timezone()) + datetime.timedelta(minutes = 15)
	'''
	
	#end of the function
	return render_to_response('web/emergency_restore.html', locals(), context_instance = RequestContext(request))

def GetBulkNumbers(Order):
	RangeNumber = Order.RoutingInfoList.all()
	NumberList = []
	for each in RangeNumber:
		if each.PhoneNumberStart == each.PhoneNumberEnd:
			NumberList.append(str(each.PhoneNumberStart))
		else:
			NumberList.append(str(each.PhoneNumberStart) + "-" + str(each.PhoneNumberEnd))
	return str(NumberList).replace(' ','').replace('\'','')


def SaveBulkNumbers(NumberList, Order):
	for number in NumberList:
		if number.find('-') != -1:
			eachRange = number.split('-')
			Order.RoutingInfoList.create(PhoneNumberStart = eachRange[0],PhoneNumberEnd = eachRange[1])
		else:
			Order.RoutingInfoList.create(PhoneNumberStart = number,PhoneNumberEnd = number)

def ProcessBulkNumbers(x):
	#x = "900000,90000003,90000100-90000200\n900000 \n9000100-9000110 \n9000200-9000190,90001234,i3774667 i3774667-i3774665\n\n\ni3774667-i3774669"
	if x == "":
		return (False, "A minimum of one phone number is mandatory ")
	#LogInput("Input: " + str(x),"Development","4gtss")
	x = x.replace('\n',',').replace('\r', ',').replace(' ',',')
	x = x.split(',')
	x = filter(None, x)
	#LogInput("Input After: " + str(x),"Development","4gtss")
	try:
		for each in x:
			if each.find('-') != -1:
				y = each.split('-')
				if len(y[0]) != 8 and len(y[1]) != 8:
					#LogInput("The number should be 8 digit long " + str(y),"Development","4gtss")
					return (False, "The number should be 8 digit long " + str(y))
				else:
					if y[0] > y[1]:
						#LogInput("The number should be in range " + str(y),"Development","4gtss")
						return (False, "The number should be in range " + str(y))
					else:
						if y[0].isdigit() != True and y[1].isdigit() != True:
							#LogInput("Number shouldn't contain characters " + str(y),"Development","4gtss")
							return (False, "Number shouldn't contain characters " + str(y))
						else:
							LogInput(each +" This worked")
			else:
				if len(each) == 8:
					if each.isdigit() != True:
						#LogInput("Number shouldn't contain characters " + each,"Development","4gtss")
						return (False, "Number shouldn't contain characters " + each)
					else:
						#return (True, x)
						pass
				else:
					#LogInput("The number should be 8 digit long " + each,"Development","4gtss")
					return (False, "The number should be 8 digit long " + each)
		return (True, x)
	except:
		return (False, "Failed to process numbers")


@permission_required('authenticate.add_request', raise_exception=True)
@csrf_protect
def SendNpRequestEmergencyRestore(request, ):
	PageName = "Emergency Restore"
	today = datetime.date.today()
	today30 = today + datetime.timedelta(+30)
	'''
	if NPRequestEntry:
		nform = NPRequestEmergencyRestoreForm(instance=NPRequestEntry, prefix="main")
		rform = RoutingInfoListFormSet(prefix='RoutingInfoList')
		rasform = RASFormSet(prefix='RAS')
	'''
	if request.POST: # If the form has been submitted
		instance = NpMessages()

		nform = NPRequestEmergencyRestoreForm(request.POST, instance=instance, prefix='main')
		rform = RoutingInfoListFormSet(request.POST, instance=instance, prefix='RoutingInfoList')
		rasform = RASFormSet(request.POST, instance=instance, prefix='RAS')
		
		if 'addPhone' in request.POST:
			cp = request.POST.copy()
			cp['RoutingInfoList-TOTAL_FORMS'] = int(cp['RoutingInfoList-TOTAL_FORMS']) + 1
			rform = RoutingInfoListFormSet(cp, prefix='RoutingInfoList')
			
		elif 'delPhone' in request.POST:
			cp = request.POST.copy()
			cp['RoutingInfoList-TOTAL_FORMS'] = int(cp['RoutingInfoList-TOTAL_FORMS']) - 1
			rform = RoutingInfoListFormSet(cp, prefix='RoutingInfoList')
			
		elif 'Submit' in request.POST:
			if nform.is_valid(): # All validation rules pass
				NewRequest = nform.save(commit=False)
				routing_formset = RoutingInfoListFormSet(request.POST, instance=instance, prefix='RoutingInfoList')
				
				if rform.is_valid():
					if rasform.is_valid():
						# NewRequest is the message that is saved to DB
						# set the message id as nprequst, also the message that is saved to db
						NewRequest.MessageTypeID = GetRequest("NPRequest")
	
						NewRequest.save()
						rform.save()
						rasform.save()
						RASMessage = IntegrationRAS.objects.get(np_messages=NewRequest)
						RASMessage.PhoneNumber = "RANGE"

						# Changes for dealer ID add on 14 of Aug
						try:
							RASMessage.dealer_id = request.user.get_profile().dealer_id
						except:
							if RASMessage.CustomerType == "business":
								RASMessage.dealer_id = "77000"
							elif RASMessage.CustomerType == "consumer":
								RASMessage.dealer_id = "999999"

						RASMessage.save()
						
						Respond = HandleOutgoingMessage(NewRequest, str(request.user))
						LogInput('Accept Respond: ' + str(Respond))
						
						LogInput('Results in the new request page .. ')
						LogInput(str(Respond))
						
				else:
					LogInput('routing_formset.is_valid is not valid')
			else:
				LogInput('nform.is_valid is not valid')
	
	else:
		nform = NPRequestEmergencyRestoreForm(prefix="main")
		rform = RoutingInfoListFormSet(prefix='RoutingInfoList')
		rasform = RASFormSet(prefix='RAS')
	
	return render_to_response('web/new_er_request.html', locals(), context_instance = RequestContext(request))

@permission_required('authenticate.sent_requests',  raise_exception=True)	
@csrf_protect
def SentRequest(request):
	'''
	This loads the Sent Request page
	'''
	PageName = "Sent Requests"
	

	if request.POST: # If the form has been submitted
		instance = NpMessages()
		form = FilterForm(request.POST, prefix='main')

		LogInput('There is submission')
		
		# this for is only to print to logs all the contents of the POST submission 
		for key in request.POST:
			LogInput(key + ' : ' + request.POST[key])

		if 'Activate' in request.POST['SubmitType']:
			MessageLocalID = request.POST['MessageLocalID']
			NPRequestEntry = NpMessages.objects.get(pk = MessageLocalID)
	
			#LogInput('Found Message: ' + LogMessageResutls(NPRequestEntry))
			NewMessage = NpMessages(
				MessageTypeID = GetRequest('NPActivated'),
				NPOrderID = str(NPRequestEntry.NPOrderID) if NPRequestEntry.NPOrderID is not None else str(''),
				user = str(request.user),
			)
			NewMessage.save()

			Respond = HandleOutgoingMessage(NewMessage, str(request.user))
			
			LogInput('Activate Respond: ' + str(Respond))

		# Send Again Logic --- COMPLETED and IT WORKS
		elif request.POST['SubmitType'] in ('Copy Request', 'Send Again'):
			PageName = "New Request"
			MessageLocalID = request.POST['MessageLocalID']
			
			NPRequestEntry = NpMessages.objects.get(pk = MessageLocalID)
			#RoutingInfoLists = NPRequestEntry.RoutingInfoList.all()
			
			LogInput('Found Message: ' + LogMessageResutls(NPRequestEntry))

			nform = NPRequestForm(instance=NPRequestEntry, prefix='main')
			rform = RoutingInfoListFormSet(instance=NpMessages(), prefix='RoutingInfoList')
			rasform = RASFormSet(instance=NpMessages(), prefix='RAS')

			#the below code should bring the routing info to be edited but it is not working
			#rform = []
			#for RoutingInfoList in RoutingInfoLists.all():
			#	rform.append(RoutingInfoListFormSet(instance=RoutingInfoList, prefix='RoutingInfoList'))
			
			#end of the function
			return render_to_response('web/new_request.html', locals(), context_instance = RequestContext(request))


		elif 'Cancel' in request.POST['SubmitType']:
			MessageLocalID = request.POST['MessageLocalID']
			NPRequestEntry = NpMessages.objects.get(pk = MessageLocalID)
	
			LogInput('Found Message: ' + LogMessageResutls(NPRequestEntry))

			NewMessage = NpMessages(
				MessageTypeID = GetRequest('NPRequestCancel'),
				NPOrderID = str(NPRequestEntry.NPOrderID) if NPRequestEntry.NPOrderID is not None else str(''),
				CancelReason = 1,
				user = str(request.user),
			)
			
			NewMessage.save()
			
			# Log the user that submitted the form
			LogInput(str(request.user))
			#NPRequestEntry.user = str(request.user)		
			
			LogInput("MessageTypeID: " + str(NewMessage.MessageTypeID))
			LogInput('Inside if Cancel')

			# Send the new cancel message, and Order and the user
			#Respond = HandleOutgoingMessage(NewMessage, NPRequestEntry, str(request.user, str(request.user)))
			
			Respond = HandleOutgoingMessage(NewMessage, str(request.user))
			
			LogInput('Cancel Respond: ' + str(Respond))

		elif 'Filter' in request.POST['SubmitType']:
			LogInput("SubmitType is Filter")

			if form.is_valid():
				phone_number = form.cleaned_data['PhoneNumber']
				np_order_id = form.cleaned_data['NPOrderID']
				selected_status = form.cleaned_data['Status']
				selected_user = form.cleaned_data['User']
				
				if phone_number:
					FindPhoneNumber(phone_number)

				# building a dic for all filters
				FilterDict = dict(
					NPOrderID = np_order_id if np_order_id is not None else '' ,
					status = selected_status if selected_status is not None else '' ,
					user = selected_user if selected_user is not None else '' ,
					RoutingInfoList__PhoneNumberEnd__gte = int(phone_number) if phone_number else '',
					RoutingInfoList__PhoneNumberStart__lte = int(phone_number) if phone_number else '',
					Originator="NPG",
					MessageTypeName="NPRequest",
					message_created__gte=datetime.datetime(year=2013,month=6,day=15)
				)
				#will remove all empty in dic
				FilterDict = dict((k, v) for k, v in FilterDict.iteritems() if v)
				
				LogInput("Form is valid for filter, filter is: " + str(FilterDict))
				message_list = NpMessages.objects.select_related().filter(**FilterDict).order_by('-message_created')[:100]
				#message_list = NpMessages.objects.select_related().filter(**FilterDict) & NpMessages.objects.filter(Originator="NPG",MessageTypeName="NPRequest").order_by('-message_created')[:10]
				#message_list = message_list.order_by('-message_created')[:10]
				
				return render_to_response('web/sent_requests.html', locals(), context_instance = RequestContext(request))
		
		
		else:
			LogInput('ERROR: There is no Accept or Reject submission')
			LogInput('Inside else')
		
	else:	
		form = FilterForm(prefix="main")
		nform = NPRequestCancelForm(prefix="Reject")
	
	now = datetime.datetime.now()
	now_from = datetime.datetime(year=now.year,month=now.month,day=now.day)
	today = datetime.date.today()

	#message_list = NpMessages.objects.filter( Q(Originator="NPG") & Q(MessageTypeName="NPRequest") &  Q(user = request.user)  & Q(message_modified__gte=now_from) & (Q(status=1) | Q(status=4) | Q(status=5) | Q(status=6) | Q(status=7) | Q(status=8) | Q(status=9) | Q(status=10) ) )
	#message_list = message_list.order_by('-message_modified')
	
	message_list = NpMessages.objects.filter(Originator="NPG", MessageTypeName="NPRequest", user = request.user, message_modified__gte=now_from, status__in = [1,4,5,6,7,8,9,10]).order_by('-message_modified')

	
	return render_to_response('web/sent_requests.html', locals(), context_instance = RequestContext(request)) 

@permission_required('authenticate.add_request', raise_exception=True)
@csrf_protect
def SendNpRequest(request):
	PageName = "New Request"
	today = datetime.date.today()

	if request.POST: # If the form has been submitted
		instance = NpMessages()

		nform = NPRequestForm(request.POST, instance=instance, prefix='main')
		rasform = RASFormSet(request.POST, instance=instance, prefix='RAS')
		BulkPhoneNumber = request.POST['BulkPhoneNumber']

		if 'Submit' in request.POST:
			'''
			nform is the main form which contains all the customer info, verify all form contains
			'''
			if nform.is_valid(): # All validation rules pass
				NewRequest = nform.save(commit=False)

				#LogInput(BulkPhoneNumber,"Development","4gtss")
				if BulkPhoneNumber == "":
					ListOfNumbers = [False,"Missing phone numbers"]
				else:
					ListOfNumbers = ProcessBulkNumbers(BulkPhoneNumber)

				if ListOfNumbers[0]:
					if rasform.is_valid():
						# NewRequest is the message that is saved to DB
						# set the message id as nprequst, also the message that is saved to db
						NewRequest.MessageTypeID = GetRequest("NPRequest")
	
						NewRequest.save()
						SaveBulkNumbers(ListOfNumbers[1],NewRequest)

						rasform.save()
						RASMessage = IntegrationRAS.objects.get(np_messages=NewRequest)
						
						RASMessage.SIM = RASMessage.SIM.upper()
						RASMessage.PhoneNumber = "RANGE"
						#print(rasform)

						# Changes for dealer ID add on 14 of Aug
						try:
							if request.user.get_profile().dealer_id and request.user.get_profile().dealer_id != "":
								RASMessage.dealer_id = request.user.get_profile().dealer_id
								#LogInput("INFO: NewRequest id : " + str(NewRequest.MessageLocalID),"Development","4gtss")
								#LogInput("INFO: Dealer ID found and added: " + str(RASMessage.dealer_id),"Development","4gtss")
							else:
								if RASMessage.CustomerType == "business":
									RASMessage.dealer_id = "77000"
									#LogInput("INFO: Dealer ID not found, default for business","Development","4gtss")
								elif RASMessage.CustomerType == "consumer":
									RASMessage.dealer_id = "999999"
									#LogInput("INFO: Dealer ID not found, default for consumer","Development","4gtss")
						except:
							#LogInput("ERROR: Dealer ID not found : " + str(traceback.format_exc()),"Development","4gtss")
							if RASMessage.CustomerType == "business":
								RASMessage.dealer_id = "77000"
								#LogInput("INFO: Dealer ID not found, default for business","Development","4gtss")
							elif RASMessage.CustomerType == "consumer":
								RASMessage.dealer_id = "999999"
								#LogInput("INFO: Dealer ID not found, default for consumer","Development","4gtss")

						RASMessage.save()
						
						Respond = HandleOutgoingMessage(NewRequest, str(request.user))
						LogInput('Accept Respond: ' + str(Respond))
						
						LogInput('Results in the new request page .. ')
						LogInput(str(Respond))
						
				else:
					BulkPhoneError = ListOfNumbers[1]

					
			else:
				LogInput('nform.is_valid is not valid')
					
	else:
		nform = NPRequestForm(prefix="main")
		rasform = RASFormSet(prefix='RAS')
	
	#end of the function
	return render_to_response('web/new_request.html', locals(), context_instance = RequestContext(request))
	#return render(request, 'web/new_request.html', {'form':NPRequestForm()})

@permission_required('authenticate.search',raise_exception=True)	
@csrf_protect
def SearchMessages(request):
	PageName = "Search Messages"
	#ip_address = request.META.get('HTTP_X_FORWARDED_FOR', '') or request.META.get('REMOTE_ADDR')
	#LogInput('Accessed by: %s' % ip_address)
	'''
	try:
		user = request.user
		#print user
		#LogInput("INFO: " + str(user.get_profile().dealer_id),"Development","4gtss")
	except Exception as e :
		LogInput(str(e),"Development","4gtss")
	'''
	if request.POST: # If the form has been submitted
		if 'Reset' in request.POST:
			form = SearchMessagesForm()
			submitted = "no"
			#LogInput('New Form is created, ready to be filled and submitted')
			return render_to_response('web/search_messages.html', locals(), context_instance = RequestContext(request))
			
		form = SearchMessagesForm(request.POST)
		#LogInput("In Search 2 the request.POST  =  " + str(request.POST),"Development","Hazim")
		
		#if 'addPhone' in request.POST:

		if form.is_valid():
			
			phone_number = form.cleaned_data['PhoneNumber']
			NPOrderID = form.cleaned_data['NPOrderID']
			DonorName = form.cleaned_data['DonorName']
			RecipientName = form.cleaned_data['RecipientName']
			Originator = form.cleaned_data['Originator']
			MessageTypeName = form.cleaned_data['MessageTypeName']
			Status = form.cleaned_data['Status']
			Ack = form.cleaned_data['ack']
			
			DateFrom = form.cleaned_data['DateFrom']
			DateTo = form.cleaned_data['DateTo']

			ExecutionDateFrom = form.cleaned_data['ExecutionDateFrom']
			ExecutionDateTo = form.cleaned_data['ExecutionDateTo']

			if phone_number:
				FindPhoneNumber(phone_number)
			
			#empty dict
			ExcludeFilterDict = {}
			FilterDict = {
				'NPOrderID' : NPOrderID if NPOrderID else None,
				'RoutingInfoList__PhoneNumberEnd__gte' : int(phone_number) if phone_number else None,
				'RoutingInfoList__PhoneNumberStart__lte' : int(phone_number) if phone_number else None,
				'MessageTypeName' : MessageTypeName if MessageTypeName else None,
				'DonorName' : DonorName if DonorName else None,
				'RecipientName' : RecipientName if RecipientName else None,
				'Originator' : Originator if Originator else None,
				'status' : Status if Status else None,
				'ack' : Ack if Ack else None,
				#'message_created__range' : [DateFrom, DateTo]
				#'DateFrom' : request.POST['DateFrom'] if request.POST['DateFrom'] is not None else '',
				#'DateTo' : request.POST['DateTo'] if request.POST['DateTo'] is not None else '',
			}
			
			# Set the date from to 
			if DateFrom:
				if not DateTo:
					DateTo = datetime.date.today()
						
				DateFrom = datetime.datetime.combine(DateFrom, datetime.time(0, 0, 0)).replace(tzinfo=utc)
				DateTo = datetime.datetime.combine(DateTo, datetime.time(23, 59, 59)).replace(tzinfo=utc)
				DateFrom -= datetime.timedelta(hours=3)
				DateTo -= datetime.timedelta(hours=3)
				FilterDict["message_created__range"] = [DateFrom, DateTo]
				
			# Set the Execution date from to 
			if ExecutionDateFrom:
				if not ExecutionDateTo:
					ExecutionDateTo = datetime.date.today()
				
				ExecutionDateFrom = datetime.datetime.combine(ExecutionDateFrom, datetime.time(0, 0, 0)).replace(tzinfo=utc)
				ExecutionDateTo = datetime.datetime.combine(ExecutionDateTo, datetime.time(23, 59, 59)).replace(tzinfo=utc)
				ExecutionDateFrom -= datetime.timedelta(hours=3)
				ExecutionDateTo -= datetime.timedelta(hours=3)
			#	ExecutionDateTo = datetime.combine(ExecutionDateTo, datetime.min.time())	
				FilterDict["PortingTime__range"] = [ExecutionDateFrom, ExecutionDateTo]
			
			LogInput("In Search 2 the FilterDict  =  " + str(FilterDict),"Development","4gtss")

			#LogInput("In Search 2 the FilterDict  =  " + str(FilterDict),"Development","Hazim")
			
			#will remove all empty in dic
			FilterDict = dict((k, v) for k, v in FilterDict.iteritems() if v and v != "ALL")

			#LogInput("The ExecutionDateFrom is:   " + str(ExecutionDateFrom),"Development","4gtss")
			#LogInput("The ExecutionDateTo is:   " + str(ExecutionDateTo),"Development","4gtss")
			#LogInput("The Filter is:   " + str(FilterDict),"Development","4gtss")

			# remove ack messages
			try:
				#showAck = request.POST['showAck']
				ExcludeFilterDict["MessageTypeName"] = "MessageAck"
				#FilterDict["MessageTypeName__contains"] = "Ack"
			except:
				showAck = "NO"
				ExcludeFilterDict["MessageTypeName"] = "MessageAck"
		#	ExecutionDateTo.replace(tzinfo=UTC())
			#LogInput("In Search 2 the FilterDict  =  " + str(FilterDict),"Development","Hazim")
			SearchResults = NpMessages.objects.filter(**FilterDict).exclude(**ExcludeFilterDict).order_by('-MessageLocalID')
			#LogInput("In Search 2 the SearchResults  =  " + str(SearchResults),"Development","Hazim")
			if 'ExportReportCSV' in request.POST:
				return ExportCSV(SearchResults)
	
			if 'ExportReportExcel' in request.POST:
				return ExportExcel(SearchResults)

			try:
				ShowTotalResults = request.POST['ShowTotalResults']
			except:
				ShowTotalResults =  20
	
			# This is for Next and Previous 
			paginator = Paginator(SearchResults, ShowTotalResults)
			
			# Count total pages
			TotalPages = paginator.num_pages
			TotalSearchResults = paginator.count
			PagesList = []
	
			for x in range(TotalPages):
				PagesList.append(x+1)
			
			#LogInput('page list: %s' % PagesList)
			
			if 'PageChangeN' in request.POST:
				page = request.POST['pageN']
			elif 'PageChangeP' in request.POST:
				page = request.POST['pageP']
			else:
				page = ""
			
			try:
				SearchResults = paginator.page(page)
			except PageNotAnInteger:
				# If page is not an integer, deliver first page.
				SearchResults = paginator.page(1)
			except EmptyPage:
				# If page is out of range (e.g. 9999), deliver last page of results.
				SearchResults = paginator.page(paginator.num_pages)
				
			LogInput('ShowTotalResults: %s' % ShowTotalResults)
			#LogInput('Originator: %s' % Originator)
	
			submitted = "yes"
			#paginate_by = ''
							
	else:
		form = SearchMessagesForm()
		submitted = "no"
		#LogInput('New Form is created, ready to be filled and submitted')

	return render_to_response('web/search_messages.html', locals(), context_instance = RequestContext(request))

@permission_required('authenticate.search',raise_exception=True)	
@csrf_protect
def SearchMessages3(request):
	PageName = "Search Messages"
	#ip_address = request.META.get('HTTP_X_FORWARDED_FOR', '') or request.META.get('REMOTE_ADDR')
	#LogInput('Accessed by: %s' % ip_address)

	if request.POST: # If the form has been submitted
		form = NpMessagesForm(request.POST) # A form bound to the POST data
		'''
		#LogInput('Form has been submitted')
		instance = NpMessages()
		#form = NpMessagesForm(request.POST,instance=instance) # A form bound to the POST data
		#form=form_construct.save(commit=False)
		#if form.is_valid(): # All validation rules pass
		#LogInput('Checking submission')
		'''
		#Process the data in form.cleaned_data
		#MessageTypeName = request.POST['MessageTypeName']
		PhoneNumber = str(request.POST['PhoneNumber'])
		NPOrderID = request.POST['NPOrderID']
		#Originator = request.POST['Originator']
		
		try:
			ShowTotalResults = request.POST['ShowTotalResults']
		except:
			ShowTotalResults = request.POST['ShowTotalResults'] = 40
		
		
		ShowPortIDList = None

		try:
			related = request.POST['related']
		except:
			related = request.POST['related'] = "NO"
		#comments = form.cleaned_data['comments']
		
		# remove ack messages
		try:
			showAck = request.POST['showAck']
		except:
			showAck = request.POST['showAck'] = "NO"
		
		#SearchResults = NpMessages.objects.all()
		
		SearchResultsCommand = "NpMessages.objects.all()"
		#PreFetch = ".prefetch_related('RoutingInfoList','AttachmentList'"
		PreFetch = ""
		TheFilter = ""
		TheOrder = ".order_by("
		Orders = []
		#Limit = "[:%s]" % ShowTotalResults
		Limit = ""
		
		if showAck == "NO":
			TheFilter += (".filter(~Q(MessageTypeName__contains = 'Ack'))")
			#SearchResults = SearchResults.filter(~Q(MessageTypeName__contains = 'Ack'))
		'''	
		# show Originator control
		if Originator != "ALL":
			TheFilter += (".filter(Originator = Originator)")
			#SearchResults = SearchResults.filter(Originator = Originator)
		
		if MessageTypeName != "ALL":
			TheFilter += (".filter(MessageTypeName = MessageTypeName)")
		'''

		if NPOrderID != "" and NPOrderID is not None:
			TheFilter += (".filter(NPOrderID__contains = NPOrderID)")
			#SearchResults = SearchResults.filter(NPOrderID__contains = NPOrderID)

		if PhoneNumber != "" and PhoneNumber is not None and related == "NO":
			TheFilter += (".filter(Q(RoutingInfoList__PhoneNumberStart__lte = PhoneNumber) & Q(RoutingInfoList__PhoneNumberEnd__gte = PhoneNumber))")

		# sort messages

		'''
		if related == "YES" and number != "" and number is not None and port_id == "":
			RelatedSearchResults = SearchResults.filter(number = number)

			RelatedList = []

			for relatedSR in RelatedSearchResults:
				RelatedList.append(relatedSR.port_id)
			
			# remove duplicate port id list
			RelatedList = list(set(RelatedList))
			
			# Remove blank from port id list
			[x for x in RelatedList if x]
			
			RelatedListCount = len(RelatedList)
			
			if RelatedListCount == 1:
				NPOrderID = request.POST['NPOrderID'] = RelatedList[0]
			else:
				ShowPortIDList = RelatedList
					
		if 'ExportReportCSV' in request.POST:
			return ExportCSV(SearchResults)

		if 'ExportReportExcel' in request.POST:
			return ExportExcel(SearchResults)
		'''

		Orders.append("'-MessageLocalID'")
		TheOrder += ",".join(Orders) + ")"
		
		if TheFilter:
			SearchResultsCommand = SearchResultsCommand.replace(".all()", TheFilter)
		else:
			pass
		
		SearchResultsCommand += TheOrder + PreFetch + Limit

		LogInput('SearchResultsCommand: %s' % SearchResultsCommand)

		SearchResults = eval(SearchResultsCommand)
						
		# This is for Next and Previous 
		paginator = Paginator(SearchResults, ShowTotalResults)
		
		# Count total pages
		TotalPages = paginator.num_pages
		TotalSearchResults = paginator.count
		PagesList = []

		for x in range(TotalPages):
			PagesList.append(x+1)
		
		#LogInput('page list: %s' % PagesList)
		
		if 'PageChangeN' in request.POST:
			page = request.POST['pageN']
		elif 'PageChangeP' in request.POST:
			page = request.POST['pageP']
		else:
			page = ""
		
		try:
			SearchResults = paginator.page(page)
		except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			SearchResults = paginator.page(1)
		except EmptyPage:
			# If page is out of range (e.g. 9999), deliver last page of results.
			SearchResults = paginator.page(paginator.num_pages)
			
		LogInput('ShowTotalResults: %s' % ShowTotalResults)
		#LogInput('Originator: %s' % Originator)

		submitted = "yes"
		#paginate_by = ''
		return render_to_response('web/search_messages3.html', locals(), context_instance = RequestContext(request))
		#else:
		#LogInput('Submission is not Valid')
		#form = NpMessagesForm(request.POST, instance=instance)
	else:
		form = NpMessagesForm()
		submitted = "no"
		#LogInput('New Form is created, ready to be filled and submitted')

	return render_to_response('web/search_messages3.html', locals(), context_instance = RequestContext(request))

def ExportExcel(request):
	response = HttpResponse(request, mimetype='application/vnd.ms-excel')
	for each in request:
		if each.MessageTypeName == "NPFinalBillNotification":
			each.NPOrderID = each.OriginalNPOrderID
			request = request | NpMessages.objects.filter(NPOrderID=each.OriginalNPOrderID, MessageTypeName="NPRequest")
	response = render_to_response("export/ExportSearchExcel.html", {'data': request})
	filename = "NPG_Filter_Results_" + str(datetime.datetime.now()) + ".xls"
	response['Content-Disposition'] = 'attachment; filename=' + filename
	response['Content-Type'] = 'application/vnd.ms-excel; charset=utf-16'
	return response

def ExportCSV(request):
	# Create the HttpResponse object with the appropriate CSV header.
	response = HttpResponse(mimetype='text/csv')
	filename = "NPG_Filter_Results_" + str(datetime.datetime.now()) + ".csv"
	response['Content-Disposition'] = 'attachment; filename=' + filename

	t = loader.get_template('export/ExportSearch.txt')
	c = Context({
		'data': request,
		})
	response.write(t.render(c))
	return response


@permission_required('authenticate.received_requests', raise_exception=True)	
@csrf_protect
def ReceivedRequest(request):
	'''
	This loads the Received Request page
	'''
	PageName = "Received Request"
	LogInput('Inside ReceivedRequest')

	if request.POST: # If the form has been submitted
		LogInput('There is submission')

		for key in request.POST:
			LogInput(key + ' : ' + request.POST[key])
		
		'''
		Check if accept buttons was clicked
		'''
		if request.POST['SubmitType'] in ('Accept','Reject'):
			MessageLocalID = request.POST['MessageLocalID']
			NPRequestEntry = NpMessages.objects.get(pk=MessageLocalID)
			LogInput('Found Message: ' + LogMessageResutls(NPRequestEntry))


		if 'Accept' in request.POST['SubmitType']:		
			NewMessage=dict(
				MessageTypeID = GetRequest('NPDonorAccept'),
				NPOrderID = str(NPRequestEntry.NPOrderID) if NPRequestEntry.NPOrderID is not None else str(''),
			)
			
			LogInput("MessageTypeID: " + str(NewMessage["MessageTypeID"]))
			LogInput('Inside if accept')
			Respond = HandleOutgoingMessage(NewMessage, NPRequestEntry, str(request.user))
			LogInput('Accept Respond: ' + str(Respond.values()))
			
			Results = str(Respond.values())
		
			'''
			Check if Reject buttons was clicked
			'''
		elif 'Reject' in request.POST['SubmitType']:
			DonorRejectReason = request.POST['DonorRejectReason']
			DonorRejectMessage = request.POST['DonorRejectMessage']
			AttachmentList = request.POST['AttachmentList']
			
			NewMessage=dict(
				MessageTypeID = GetRequest('NPDonorReject'),
				MessageID = str(NPRequestEntry.MessageID) if NPRequestEntry.MessageID is not None else str(''),
				MessageTime = str(NPRequestEntry.MessageTime) if NPRequestEntry.MessageTime is not None else str(''),
				SenderID = str(NPRequestEntry.SenderID) if NPRequestEntry.SenderID is not None else str(''),
				NPOrderID = str(NPRequestEntry.NPOrderID) if NPRequestEntry.NPOrderID is not None else str(''),
				DonorRejectReason = str(DonorRejectReason) if DonorRejectReason is not None else str(''),
				DonorRejectMessage = str(DonorRejectMessage) if DonorRejectMessage is not None else str(''),
				AttachmentList = AttachmentList,
			)
			LogInput('Inside elif reject')

			LogInput("MessageTypeID: " + str(NewMessage["MessageTypeID"]))
			LogInput('Inside if Reject')
			Respond = HandleOutgoingMessage(NewMessage, NPRequestEntry, str(request.user))
			LogInput('Accept Respond: ' + str(Respond.values()))

			Results = str(Respond.values())
			
		# The option to hide the NPMessage if N/A
		elif 'Filter' in request.POST['SubmitType']:
			LogInput("SubmitType is Filter")
			
			form = FilterForm(request.POST, prefix='main')

			if form.is_valid():
				phone_number = form.cleaned_data['PhoneNumber']
				np_order_id = form.cleaned_data['NPOrderID']
				#selected_status = form.cleaned_data['Status']
				#selected_user = form.cleaned_data['User']
				
				if phone_number:
					FindPhoneNumber(phone_number)

				# building a dic for all filters
				FilterDict = dict(
					NPOrderID = np_order_id if np_order_id is not None else '' ,
					#status = selected_status if selected_status is not None else '' ,
					#user = selected_user if selected_user is not None else '' ,
					RoutingInfoList__PhoneNumberEnd__gte = int(phone_number) if phone_number else '',
					RoutingInfoList__PhoneNumberStart__lte = int(phone_number) if phone_number else '',
				)
				#will remove all empty in dic
				FilterDict = dict((k, v) for k, v in FilterDict.iteritems() if v)
				
				LogInput("Form is valid for filter, filter is: " + str(FilterDict))
				message_list = NpMessages.objects.select_related().filter(**FilterDict) & NpMessages.objects.filter(Originator="NPCS",MessageTypeName="NPRequest",status=1)
				
				return render_to_response('web/received_requests.html', locals(), context_instance = RequestContext(request))
			
			
		else:
			LogInput('ERROR: There is no Accept or Reject submission')
			LogInput('Inside else')
			exit()
	else:	
		form = FilterForm(prefix="main")

	nform = NPDonorRejectForm(prefix="Reject")

	message_list = NpMessages.objects.filter(Originator="NPCS",MessageTypeName="NPRequest",status=1)
	message_list = message_list.order_by('-message_created')

	return render_to_response('web/received_requests.html', locals(), context_instance = RequestContext(request))

def FindPhoneNumber(PhoneNumber):
	PhoneNumber = int(PhoneNumber)

	return RoutingInfoList.objects.filter(PhoneNumberStart__gte=PhoneNumber) & RoutingInfoList.objects.filter(PhoneNumberEnd__lte=PhoneNumber)


def AllMessages(request):
	'''
	This is testing function
	'''
	results = ""
	
	return render_to_response('web/new_css.html', {
		'message_list': results,
		#'page_name':PageName,
		'server':GetConfig("SOAP_CONNECTION"),
		}
	)
